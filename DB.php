<?
namespace RobinTail\EngineAPI;


class DB 
{
	private static $instance;

    /**
     * @return DB\PDO_mysql
     */

    public static function get()
	{
		if (!self::$instance) {
            self::$instance = new DB\PDO_mysql();
		}
		return self::$instance;
	}


    /**
     * @return DB\PDO_test
     */

    public static function getTest()
    {
        if (!self::$instance) {
            self::$instance = new DB\PDO_test();
            self::$instance->loadFromXML(Path::resolve('test/dbFake.xml'));
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

}

