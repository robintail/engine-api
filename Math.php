<?

namespace RobinTail\EngineAPI;

class Math
{
    /**
     * Calculate the factorial of the given number
     * @param $number int
     * @return int
     */

    public static function gmpFact($number)
	{
        assert( is_int($number), "Argument should be an integer" );
        assert( $number>=0, "Argument should be greater or equals to zero");
		if ($number===0) return 1;
		$result = 1;
		for($i=1;$i<=$number;$i++)
		{
			$result *= $i;
		}
		return $result;
	}

}

?>