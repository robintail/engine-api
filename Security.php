<?

namespace RobinTail\EngineAPI;


/**
 * Class Security
 * @package RobinTail\EngineAPI
 */
class Security implements Security\methods
{
    /**
     * running in local area network
     * try to fetch ip address from HTTP-X-FORWARDED-FOR
     * @var bool
     */
    private static $use_xforward = false;
    /**
     * passwords secure encrypting key
     * changing this value make all stored passwords' hashes invalid
     * @var string
     */
    private static $salt = "TATTOO-FACTORY-MD5-SALT";
    /**
     * length of password required and generated
     * @var int
     */
    private static $passwordLength = 6;
    /**
     * is password may use upper case chars
     * @var bool
     */
    private static $isPasswordUseUpperCase = true;


    /**
     * Returns $passwordLength
     * @return int
     */
    public static function getPasswordLength()
    {
        return self::$passwordLength;
    }

    /**
     * Special key for recover password
     * Valid in current day
     * @param $cryptedPassword
     * @return string
     */
    public static function getRecoveryHash($cryptedPassword)
	{
		return md5(md5($cryptedPassword).strrev($cryptedPassword).strrev(md5($cryptedPassword)).date("d", time()));
	}


    /**
     * Encrypt password with salt
     * @param $pwd
     * @return string
     */
    public static function cryptPassword($pwd)
	{
		return md5(md5($pwd).self::$salt);
	}


    /**
     * Fetch current user IP
     * @return string
     */
    public static function getIP()
	{
		if (self::$use_xforward)
		{
			foreach( array_reverse( explode( ',', $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) as $x_f )
			{
				$x_f = trim($x_f);
				if ( preg_match( '/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $x_f ) )
				{
					$addrs[] = $x_f;
				}
			}			
		}
		$addrs[] = $_SERVER['HTTP_CLIENT_IP'];
		$addrs[] = $_SERVER['HTTP_PROXY_USER'];
		$addrs[] = $_SERVER['REMOTE_ADDR'];

		foreach ( $addrs as $ip )
		{
			if ( $ip )
			{
				preg_match( "/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/", $ip, $match );
				$ip = $match[1].'.'.$match[2].'.'.$match[3].'.'.$match[4];
				if ( $ip AND $ip != '...' )
				{
					break;
				}
			}
		}
		if ( ! $ip OR $ip == '...' )
		{
			if ($_SERVER['SERVER_NAME']!='localhost') { die ("Could not determine your IP address / Невозможно определить ваш IP адрес"); }
		}
		return $ip;
	}


    /**
     * Generates password
     * @return string
     */
    public static function generatePassword()
	{
		$result = "";
		for($i=1;$i<=self::$passwordLength;$i++)
        {
			If (mt_rand(0,100) <= 25) // 25% numbers
			{
				$result .= chr(mt_rand(48,57)); 
			} else { // 75% chars
				If ((self::$isPasswordUseUpperCase) And (mt_rand(0,100) <= 25))  // 25% of chars are in upper case
				{
					$result .= chr(mt_rand(65,90)); 
				} else { // 75% of chars are in lower case
					$result .= chr(mt_rand(97,122)); 
				}
			}
		}
		return $result;
	}



}