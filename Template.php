<?

namespace RobinTail\EngineAPI;

/**
 * Class Template
 * @package RobinTail\EngineAPI
 */
class Template
{
    /**
     * @var string
     */
    private static $defaultCode = 'page';
    /**
     * @var string
     */
    private static $adminCPCode = 'admin';


    /**
     * @var Template
     */
    private static $instance;

    /**
     * @return Template
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return Template\Item[]
     * @throws \Exception
     */
    public function readAll()
    {
        $result = array();
        DB::get()->prepare("SELECT *
                          FROM templates
                          ORDER BY template_code")
                ->execute();
        $templates = DB::get()->result();
        foreach($templates as $template)
        {
            DB::get()->prepare("SELECT styles.id_style
                            FROM styles
                            INNER JOIN template_styles ON styles.id_style = template_styles.style_id
                            WHERE template_id=?")
                ->bind(1, intval($template['id_template']))
                ->execute();
            $styles = DB::get()->result();
            $result[] = $this->createTemplateItemFromArray($template, $styles);
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getDefaultCode()
    {
        return self::$defaultCode;
    }

    /**
     * @return string
     */
    public function getAdminCPCode()
    {
        return self::$adminCPCode;
    }

    /**
     * @param string $code
     * @return Template\Item
     * @throws \Exception
     */
    public function readByCode($code)
    {
        DB::get()->prepare("SELECT *
                            FROM templates
                            WHERE template_code=?")
                ->bind(1, $code)
                ->execute();
        if ($template = DB::get()->fetchRow()) {
            DB::get()->prepare("SELECT styles.id_style
                            FROM styles
                            INNER JOIN template_styles ON styles.id_style = template_styles.style_id
                            INNER JOIN templates ON templates.id_template = template_styles.template_id
                            WHERE template_code=?")
                ->bind(1, $code)
                ->execute();
            $styles = DB::get()->result();
            return $this->createTemplateItemFromArray($template, $styles);
        } else {
            return Template\Item::create();
        }
    }


    /**
     * @param int $id
     * @return Template\Item
     * @throws \Exception
     */
    public function readById($id)
    {
        DB::get()->prepare("SELECT *
                            FROM templates
                            WHERE id_template=?")
                ->bind(1, intval($id))
                ->execute();
        if ($template = DB::get()->fetchRow()) {
            DB::get()->prepare("SELECT styles.id_style
                            FROM styles
                            INNER JOIN template_styles ON styles.id_style = template_styles.style_id
                            WHERE template_id=?")
                    ->bind(1, intval($id))
                    ->execute();
            $styles = DB::get()->result();
            return $this->createTemplateItemFromArray($template, $styles);
        } else {
            return Template\Item::create();
        }
    }

    /**
     * @param string $code
     * @param int $idToExclude
     * @return bool
     * @throws \Exception
     */
    public function isExists($code, $idToExclude)
    {
        DB::get()->prepare("SELECT id_template
                            FROM templates
                            WHERE template_code=?")
            ->bind(1, $code)
            ->execute();
        if ($row = DB::get()->fetchRow())
        {
            if ($row['id_template'] == $idToExclude)
            {
                return false; // existed code of currently testing template
            } else {
                return true; // existed code of another template
            }
        }
        return false; // not exists
    }



    /**
     * @param array $template
     * @param array $styles
     * @return Template\Item
     */
    private function createTemplateItemFromArray($template, $styles)
    {
        return Template\Item::create()
                ->writeId($template['id_template'])
                ->writeCode($template['template_code'])
                ->writeHtml($template['template_html'])
                ->writeDescription($template['template_description'])
                ->writeStyles(Regex::arrayIntval(Arr2dim::keys($styles, 'id_style', false), false));
    }


    /**
     * @param Template\Item $template
     * @throws \Exception
     * @return bool
     */
    public function write($template)
	{
        if (intval($template->readId()))
        {
            // update template
            DB::get()->prepare("UPDATE templates
                                SET template_code=:code,
                                    template_html=:html,
                                    template_description=:desc
                                WHERE id_template=:id")
                    ->bind(':code', $template->readCode())
                    ->bind(':html', $template->readHtml())
                    ->bind(':desc', $template->readDescription())
                    ->bind(':id', $template->readId())
                    ->execute();
            // remove styles
            DB::get()->prepare("DELETE FROM template_styles
                                WHERE template_id=?")
                    ->bind(1, $template->readId())
                    ->execute();
        } else {
            // insert template
            DB::get()->prepare("INSERT INTO templates (template_code, template_html, template_description)
                                SELECT :code, :html, :desc")
                ->bind(':code', $template->readCode())
                ->bind(':html', $template->readHtml())
                ->bind(':desc', $template->readDescription())
                ->execute();
            $template->writeId(DB::get()->getInsertId());
        }
        // insert styles
        foreach($template->readStyles() as $style_id) {
            DB::get()->prepare("INSERT INTO template_styles (style_id, template_id)
                                            SELECT :style, :template")
                    ->bind(':style', $style_id)
                    ->bind(':template', $template->readId())
                    ->execute();
        }
        return true;
	}

    /**
     * @param int $id
     * @throws \Exception
     * @return bool
     */
    public function remove($id)
    {
        DB::get()->prepare("DELETE FROM templates
                            WHERE id_template=?")
                ->bind(1, intval($id))
                ->execute();
        return true;
    }

}
