<?

namespace RobinTail\EngineAPI;

/**
 * Class Input
 * @package RobinTail\EngineAPI
 */
class Input
{
    /**
     * @var Input
     */
    private static $instance;
    /**
     * @var array
     */
    private $data = array();


    /**
     * @return Input
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    /**
     * Load GET and POST data into one array
     */
    private function __construct()
    {
        foreach($_GET as $k => $v)
        {
            $this->write($k, $v);
        }
        foreach($_POST as $k => $v)
        {
            $this->write($k, $v);
        }
    }


    /**
     * @param $k
     * @param $v
     */
    public function write($k, $v)
	{
		$this->data[$k] = $v;
	}

    /**
     * @param $k
     */
    public function kill($k)
	{
		unset($this->data[$k]);
	}

    /**
     * @param $k
     * @return mixed
     */
    public function read($k)
	{
		return $this->data[$k];
	}

    /**
     * @param $k
     * @return bool
     */
    public function exist($k)
	{
		return isset($this->data[$k]);
	}

    /**
     * @return array
     */
    public function readAll()
	{
		return $this->data;
	}

    /**
     * @param $k
     * @return int
     */
    public function readInt($k)
	{
		return intval($this->data[$k]);
	}

    /**
     * @param $k
     * @return int|null
     */
    public function readIntOrNull($k)
    {
        return Regex::intNull($this->data[$k]);
    }

    /**
     * @param $k
     * @return float
     */
    public function readFloat($k)
	{
		return Regex::float($this->data[$k]);
	}

    /**
     * @param $k
     * @return array
     */
    public function readIntArray($k)
	{
		return Regex::arrayIntval($this->data[$k]);
	}

    /**
     * @param $k
     * @return array
     */
    public function readFloatArray($k)
	{
		return Regex::arrayFloatval($this->data[$k]);
	}

    /**
     * @param $k
     * @return bool
     */
    public function readBool($k)
	{
		return $this->data[$k] ? true : false;
	}


}

