<?

namespace RobinTail\EngineAPI;

/**
 * Class Ajax
 * @package RobinTail\EngineAPI
 */
class Ajax
{
    /**
     * Return ajax error if not authorized
     */
    public static function checkAuth()
	{
		if (!User\Current::get()->readId()) self::error('not authorized');
	}

    /**
     * Return ajax error if not admin
     */
    public static function checkIsAdmin()
    {
        if (!User\Current::get()->readIsAdmin()) self::error("permission denied");
    }

    /**
     * Outputs a JSON object
     * @param $arr
     */
    public static function json($arr)
    {
        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
        exit();
    }

    /**
     * @param string $err
     */
    public static function error($err)
	{
        $result = array(
            'id'=>0, 'error'=>1, 'value'=> $err
        );
        self::json($result);
	}

    /**
     * @param string $msg
     */
    public static function message($msg)
	{
        $result = array(
            'id'=>0, 'error'=>0, 'value'=> $msg
        );
        self::json($result);
	}




}
