<?

namespace RobinTail\EngineAPI;

class GDlib
{
	public static function captcha($randArray)
	{
		$img = imagecreate(Security\Captcha::$width, Security\Captcha::$height);
		$bg = imagecolorallocate($img, mt_rand(200,255), mt_rand(200,255), mt_rand(200,255));
		$black = imagecolorallocate($img, 0,0,0);
		$gray = imagecolorallocate($img,128,128,128);
		$grid_arr = array(1 => $bg, 2 => $gray, 3 => $black);
		if (!Security\Captcha::$digits) Security\Captcha::loadDigits();

		Security\Captcha::$symbols = count($randArray);
        $total_width = 0;
		for($i=0;$i<Security\Captcha::$symbols;$i++)
		{			
			$total_width += imagesx(Security\Captcha::$digits[ $randArray[$i] ]);
		}

		$x = intval( Security\Captcha::$width / 2  - $total_width / 2 );
		$y = intval( Security\Captcha::$height / 2 - Security\Captcha::$digit_height / 2 ) - 5;

		for($i=0;$i<Security\Captcha::$symbols;$i++)
		{
			$digit = $randArray[$i];
			imagecopy($img, Security\Captcha::$digits[ $digit ], $x, $y+mt_rand(-1*Security\Captcha::$digit_height/10,Security\Captcha::$digit_height/10), 0,0, imagesx(Security\Captcha::$digits[ $digit ]), imagesy(Security\Captcha::$digits[ $digit ]) );
			$x+=imagesx(Security\Captcha::$digits[ $digit ]);
		}
		
		// distortion
		$img = GDlib::waveDistort($img);
		// ^^

		// noise
		for($i=0;$i<Security\Captcha::$width+Security\Captcha::$height;$i+=rand(5,15))
		{
			imageline($img, $i+rand(-5,5),0,0,$i+rand(-5,5),$grid_arr[rand(1,3)]);
		}
		for($i=0;$i<Security\Captcha::$width*Security\Captcha::$height/10;$i++)
		{
			imagesetpixel($img, mt_rand(0,Security\Captcha::$width-1), mt_rand(0,Security\Captcha::$height), $black);
		}
		for($i=0;$i<3;$i++) 
		{
			GDlib::curveBezier($img, array(mt_rand(0,imagesx($img)),mt_rand(0,imagesy($img)), mt_rand(0,imagesx($img)),mt_rand(0,imagesy($img)), mt_rand(0,imagesx($img)),mt_rand(0,imagesy($img))), $black);
		}
		// ^^

		$copyright = "(c) ".$_SERVER['SERVER_NAME'].", ".date("Y",time());
		imagefilledrectangle($img, 0, Security\Captcha::$height-imagefontheight(2),Security\Captcha::$width,Security\Captcha::$height,$bg);
		imagestring  ( $img, 2, Security\Captcha::$width/2-imagefontwidth(2)*strlen($copyright)/2, Security\Captcha::$height-imagefontheight(2), $copyright, $black);
		imagerectangle($img, 0,0,Security\Captcha::$width-1,Security\Captcha::$height-1,$gray);
		self::sendImage($img);
	}


	public static function curveBezier($im, $points, $color, $dots=false)
	{
		if ( count($points)<6 ) return false;
		if ( intval(count($points)/2) <> (count($points)/2) ) return false;
		$n = count($points)/2;	// Number of points
		$step = 0.001; // Stepping between 0 and 1

		if ($dots)
		{
			for($i=0;$i<=$n-1;$i++) // mark dots
			{
				imagefilledellipse ($im, $points[$i*2], $points[$i*2+1], 5, 5, imagecolorallocate($im, 0,0,0) );
				imagestring($im, 2, $points[$i*2] + 1, $points[$i*2+1] + 1, $i+1, imagecolorallocate($im, 127,127,127) );
			}
		}

		$prex = $points[0]; $prey = $points[1]; // starting previous points

		for($t=0;$t<=1;$t+=$step)
		{
			// B(t) = SUMi( Pi * t^i * (1-t)^(n-i) * ( n! / i!*(n-i)! ) )
			// for 3 points:
			// $bx = $p0x * (1-$t) * (1-$t) + 2 * $t * (1-$t) * $p1x + $p2x * $t * $t;
			// $by = $p0y * (1-$t) * (1-$t) + 2 * $t * (1-$t) * $p1y + $p2y * $t * $t;
			$bx = 0; $by = 0;
			for($i=0;$i<=$n-1;$i++)
			{
				// step 1 - get t^i
				$ti = pow($t, $i);
				// step 2 - get (1-t)^(n-i)
				$tni = pow(1-$t,$n-1-$i);
				// step 3 - get n!
				$nfact = Math::gmpFact($n-1);
				// step 4 - get i!*(n-i)!
				$inifact = Math::gmpFact($i) * Math::gmpFact($n-1-$i);
				// step 5 - GO!
				$bx += $points[$i*2] * $ti * $tni * ($nfact / $inifact);
				$by += $points[$i*2+1] * $ti * $tni * ($nfact / $inifact);
			}
			imageline($im, $prex, $prey, $bx, $by, $color);
			//echo $bx." ".$by."<br>";
			$prex = $bx; $prey = $by;
		}	
	}


	public static function waveDistort($img)
	{
		// случайные параметры (можно поэкспериментировать с коэффициентами):
		// частоты
		$rand1 = mt_rand(700000, 1000000) / 15000000;
		$rand2 = mt_rand(700000, 1000000) / 15000000;
		$rand3 = mt_rand(700000, 1000000) / 15000000;
		$rand4 = mt_rand(700000, 1000000) / 15000000;
		// фазы
		$rand5 = mt_rand(0, 3141592) / 1000000;
		$rand6 = mt_rand(0, 3141592) / 1000000;
		$rand7 = mt_rand(0, 3141592) / 1000000;
		$rand8 = mt_rand(0, 3141592) / 1000000;
		// амплитуды
		$rand9 = mt_rand(400, 600) / 100;
		$rand10 = mt_rand(400, 600) / 100;

		$width = imagesx($img);
		$height = imagesy($img);
		$img2 = imagecreate($width,$height);

		// copy image palette
		for($c=0;$c<imagecolorstotal($img);$c++)
		{
			$c_arr = imagecolorsforindex($img, $c);
			imagecolorallocate($img2, $c_arr['red'], $c_arr['green'], $c_arr['blue']);
		}

		// creating pixels
		for($x=0;$x<$width;$x++)
		{
			for($y=0;$y<$height;$y++)
			{
				$src_x = intval( $x + ( sin($x * $rand1 + $rand5) + sin($y * $rand3 + $rand6) ) * $rand9 );
				$src_y = intval( $y + ( sin($x * $rand2 + $rand7) + sin($y * $rand4 + $rand8) ) * $rand10 );

				if ( ($src_x<0) || ($src_y<0) || ($src_x>=$width) || ($src_y>=$height) )
				{
					$color = 0;
				} else {
					$color = imagecolorat($img, $src_x, $src_y);
				}
				$newcolor = $color;
				imagesetpixel($img2, $x, $y, $newcolor);
			}
		}
		return $img2;
	}


	public static function sendImage($img)
	{
		header("Content-type: image/jpeg");
		Imagejpeg($img);
		imagedestroy($img);
		exit();
	}

}

?>