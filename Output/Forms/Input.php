<?php

namespace RobinTail\EngineAPI\Output\Forms;


class Input {
    private $name='';
    private $id='';
    private $type='text';
    private $disabled=false;
    private $checked=false;
    private $onchange='';
    private $value='';
    private $class='';


    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @param boolean $disabled
     * @return $this
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
        return $this;
    }

    public function setChecked($checked)
    {
        $this->checked = $checked;
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $onchange
     * @return $this
     */
    public function setOnChange($onchange)
    {
        $this->onchange = $onchange;
        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Input
     */

    public static function create()
    {
        return new self();
    }

    /**
     * @return string
     */
    public function render()
    {
        return "<input class='".$this->class."' ".($this->disabled?"disabled":"")." ".($this->checked?"checked":"")." type='".$this->type."' name='".$this->name."' id='".($this->id ? $this->id : $this->name)."' value='".$this->value."' ".($this->onchange?("onchange=\"".$this->onchange."\""):"").">";
    }
}

