<?php

namespace RobinTail\EngineAPI\Output\Forms;


class Select {
    private $onlyOptions=false;
    private $name='';
    private $id='';
    private $submit=false;
    private $class='';
    private $multi=false;
    private $array=array();
    private $valueField='';
    private $captionField='';
    private $offsetField='';
    private $selected='';

    /**
     * @param array $array
     * @return $this
     */
    public function setArray($array)
    {
        $this->array = is_array($array) ? $array : array();
        return $this;
    }

    /**
     * @param string $captionField
     * @return $this
     */
    public function setCaptionField($captionField)
    {
        $this->captionField = $captionField;
        return $this;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param boolean $multi
     * @return $this
     */
    public function setMulti($multi)
    {
        $this->multi = $multi;
        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $offsetField
     * @return $this
     */
    public function setOffsetField($offsetField)
    {
        $this->offsetField = $offsetField;
        return $this;
    }

    /**
     * @param boolean $onlyOptions
     * @return $this
     */
    public function setOnlyOptions($onlyOptions)
    {
        $this->onlyOptions = $onlyOptions;
        return $this;
    }

    /**
     * @param string $selected
     * @return $this
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;
        return $this;
    }

    /**
     * @param boolean|string $submit
     * @return $this
     */
    public function setSubmit($submit)
    {
        $this->submit = $submit;
        return $this;
    }

    /**
     * @param string $valueField
     * @return $this
     */
    public function setValueField($valueField)
    {
        $this->valueField = $valueField;
        return $this;
    }





    /**
     * @return Select
     */
    public static function create()
    {
        return new self();
    }

    /**
     * @return string
     */
    public function render()
    {
        if (!$this->onlyOptions) $out = "<select name=\"".$this->name."\" id=\"".($this->id?$this->id:$this->name)."\" ".($this->submit?"onchange=\"".($this->submit===true?"this.form.submit();":$this->submit)."\"":"")." ".($this->class?"class='".$this->class."'":"")." ".($this->multi?"multiple='multiple'":"").">\n";
        foreach($this->array as $row)
        {
            if (is_array($row))
            {
                if (is_array($this->selected))
                {
                    $test = in_array($row[$this->valueField],$this->selected);
                } else {
                    $test = $this->selected==$row[$this->valueField];
                }
                $out .= "<option value=\"".$row[$this->valueField]."\" ".( $test ? "selected" : "").">".($this->offsetField?str_repeat("&nbsp;",$row[$this->offsetField]*2):"").$row[$this->captionField]."</option>\n";
            } elseif (is_object($row)) {
                $valueMethod = $this->valueField;
                $offsetMethod = $this->offsetField;
                $captionMethod = $this->captionField;
                if (is_array($this->selected))
                {
                    $test = in_array($row->$valueMethod(),$this->selected);
                } else {
                    $test = $this->selected==$row->$valueMethod();
                }
                $out .= "<option value=\"".$row->$valueMethod()."\" ".( $test ? "selected" : "").">".($this->offsetField?str_repeat("&nbsp;",$row->$offsetMethod()*2):"").$row->$captionMethod()."</option>\n";
            } else {
                if (is_array($this->selected))
                {
                    $test = in_array($row, $this->selected);
                } else {
                    $test = $this->selected==$row;
                }
                $out .= "<option ".( $test ? "selected" : "").">{$row}</option>\n";
            }
        }
        if (!$this->onlyOptions) $out .= "</select>\n";
        return $out;
    }
} 