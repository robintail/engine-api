<?php

namespace RobinTail\EngineAPI\Output\Forms;


use RobinTail\EngineAPI\Output;
use RobinTail\EngineAPI\Output\Scripts;

class Textarea {
    private $br2nl=false;
    private $value='';
    private $class='';
    private $name='';
    private $id='';
    private $tiny=false;

    /**
     * @param boolean $br2nl
     * @return $this
     */
    public function setBr2nl($br2nl)
    {
        $this->br2nl = $br2nl;
        return $this;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param boolean $tiny
     * @return $this
     */
    public function setTiny($tiny)
    {
        $this->tiny = $tiny;
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }


    /**
     * @return Textarea
     */

    public static function create()
    {
        return new self();
    }

    /**
     * @return string
     */
    public function render()
    {
        if ($this->br2nl)
        {
            $this->value = preg_replace("/<br\s?\/?>/","\n", $this->value);
        }
        $result = "<textarea class='".$this->class."' ".($this->class?"":"rows='10' cols='50'")." name='".$this->name."' id='".($this->id?$this->id:$this->name)."'>".$this->value."</textarea>";
        if ($this->tiny) Scripts::get()->addScript("<script>tiny_load('" . ($this->id ? $this->id : $this->name) . "');</script>");
        return $result;
    }
} 