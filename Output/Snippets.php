<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 23.08.15
 * Time: 15:28
 */

namespace RobinTail\EngineAPI\Output;


use RobinTail\EngineAPI\Lang;
use RobinTail\EngineAPI\Menu;
use RobinTail\EngineAPI\Output;
use RobinTail\EngineAPI\Page;
use RobinTail\EngineAPI\Settings;
use RobinTail\EngineAPI\User;

/**
 * Class Components
 * @package RobinTail\EngineAPI\Output
 */
class Snippets {
    /**
     * Admin Control Panel Menu
     * @var array
     */
    public static $adminMenu = array(
        array('url' => "/engine-api/admin/pages", 'title' => '<#pages#>'),
        array('url' => "/engine-api/admin/menu", 'title' => '<#menu#>'),
        array('url' => "/engine-api/admin/styles", 'title' => '<#styles#>'),
        array('url' => "/engine-api/admin/templates", 'title' => '<#templates#>'),
        array('url' => "/engine-api/admin/language", 'title' => '<#languages#>'),
        array('url' => "/engine-api/admin/language_const", 'title' => '<#lang-consts#>'),
        array('url' => "/engine-api/admin/settings", 'title' => '<#settings#>'),
        array('url' => "/engine-api/admin/uploads", "title" => '<#images#>'),
        array('url' => "/engine-api/admin/users", "title" => '<#users#>'),
        array('url' => "/engine-api/login/recoveryform", 'title' => '<#change-password#>'),
        array('url' => "/engine-api/login/logout", 'title' => '<#logout#>')
    );

    /**
     * Returns html control of language selector for navigation bar
     * @return string
     */
    public static function langSelector()
    {
        $code = Lang::get()->getUsing();
        $flag = "<img class='robin-icon' src='" . Lang::get()->readByCode($code)->readFlag() . "'>";
        $name = Lang::get()->readByCode($code)->readName();
        $out = <<<HTM
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$flag} <span>{$name} </span><b class="caret"></b></a>
	          <ul class="dropdown-menu">
HTM;
        foreach (Lang::get()->readAll() as $item) {
            if ($item->readIsEnabled()) {
                $out .= "<li " . ($item->readCode() == $code ? "class='active'" : "") . "><a href='/engine-api/setlang?lang=" . $item->readCode() . "'><img class='robin-icon' src='" . $item->readFlag() . "'> " . $item->readName() . "</a></li>";
            }
        }
        $out .= "</ul></li>";
        return $out;
    }

    /**
     * Returns html control of language selector as dropdown button
     * @return string
     */
    public static function langSelectorButton()
    {
        $code = Lang::get()->getUsing();
        $flag = "<img class='robin-icon' src='" . Lang::get()->readByCode($code)->readFlag() . "'>";
        $name = Lang::get()->readByCode($code)->readName();
        $out = <<<HTM
				<div class="btn-group">
  				<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{$flag} <span>{$name} </span><b class="caret"></b></button>
	          		<ul class="dropdown-menu dropdown-menu-right">
HTM;
        foreach (Lang::get()->readAll() as $item) {
            if ($item->readIsEnabled()) {
                $out .= "<li " . ($item->readCode() == $code ? "class='active'" : "") . "><a href='/engine-api/setlang?lang=" . $item->readCode() . "'><img class='robin-icon' src='" . $item->readFlag() . "'> " . $item->readName() . "</a></li>";
            }
        }
        $out .= "</ul></div>";
        return $out;
    }


    /**
     * Returns html control of language selector for navigation bar for admin CP
     * @return string
     */
    public static function langSelectorForAdminCP()
    {
        $code = Lang::get()->getUsingInAdminCP();
        $flag = "<img class='robin-icon' src='" . Lang::get()->readByCode($code)->readFlag() . "'>";
        $name = Lang::get()->readByCode($code)->readName();
        $out = <<<HTM
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$flag} <span>{$name} </span><b class="caret"></b></a>
	          <ul class="dropdown-menu">
HTM;
        foreach (Lang::get()->readAll() as $item) {
            if ($item->readIsAdminCPAvailable()) {
                $out .= "<li " . ($item->readCode() == $code ? "class='active'" : "") . "><a href='/engine-api/admin/setlang?lang=" . $item->readCode() . "'><img class='robin-icon' src='" . $item->readFlag() . "'> " . $item->readName() . "</a></li>";
            }
        }
        $out .= "</ul></li>";
        return $out;
    }


    /**
     * Returns html control of language selector as dropdown button for admin CP
     * @return string
     */
    public static function langSelectorButtonForAdminCP()
    {
        $code = Lang::get()->getUsingInAdminCP();
        $flag = "<img class='robin-icon' src='" . Lang::get()->readByCode($code)->readFlag() . "'>";
        $name = Lang::get()->readByCode($code)->readName();
        $out = <<<HTM
				<div class="btn-group">
  				<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{$flag} <span>{$name} </span><b class="caret"></b></button>
	          		<ul class="dropdown-menu">
HTM;
        foreach (Lang::get()->readAll() as $item) {
            if ($item->readIsAdminCPAvailable()) {
                $out .= "<li " . ($item->readCode() == $code ? "class='active'" : "") . "><a href='/engine-api/admin/setlang?lang=" . $item->readCode() . "'><img class='robin-icon' src='" . $item->readFlag() . "'> " . $item->readName() . "</a></li>";
            }
        }
        $out .= "</ul></div>";
        return $out;
    }



    /**
     * Returns html meta tags for alternative page language links
     * @return string
     */
    public static function alterLanguageMeta()
    {
        $return = "";
        $req = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $url = $req . (strstr($req, '?') ? '&' : '?') . 'lang='; // exclude args
        foreach (Lang::get()->readAll() as $item) {
            if ($item->readIsEnabled()) {
                $return .= "<link rel='alternate' href='" . $url . $item->readCode() . "' hreflang='" . $item->readCode() . "' />\r\n";
            }
        }
        return $return;
    }

    /**
     * Returns http request to usage report server
     * @return string
     */
    public static function usageReport()
    {
        if (intval(Settings::get()->readValue('last_usage_report')) < time() - 86400) {
            Settings::get()->write('last_usage_report', time());
            return "<img src='http://robintail.cz/engine_usage_report.php?host=" . $_SERVER['SERVER_NAME'] . "&version=" . Output::VERSION . "' style='display: none;'>";
        } else {
            return "";
        }
    }

    /**
     * Returns html code of modal windows
     * @return string
     */
    public static function modals()
    {
        return <<<HTM
			<div id='filemanager' class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><#images#></h4>
                    </div>
                    <div class="modal-body">
                        <div id="filelist"></div>
                        <div id="container">
                            <a id="pickfiles" href="javascript:;" class='btn btn-default'><#select-files#></a>
                            <a id="uploadfiles" href="javascript:;" class='btn btn-primary'><#upload#></a>
                        </div>
                        <br>
                        <pre id="console"></pre>
                        <div class='checkbox'><label><input type='checkbox' id='force_thumb' checked> <#insert-thumbnail#></label></div>
                        <div id="uploads"></div>
                    </div>
                </div>
              </div>
			</div>

			<div id='msgbox' class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">[title]</h4>
                    </div>
                    <div class="modal-body">
                        [message]
                    </div>
                </div>
              </div>
			</div>

HTM;
    }

    /**
     * Returns html code of main menu with selected item (page url)
     * Uses Output option menu_active, that can be id of menu link or url of the page
     * @return string
     */
    public static function mainMenu()
    {
        if (is_numeric(Output::get()->readOption('menu_active')))
        {
            $activeLink = Menu::get()->readById(Output::get()->readOption('menu_active'));
        } else {
            $activeLink = Menu::get()->readByPageUrl(Output::get()->readOption('menu_active'));
        }
        while ($activeLink->readId()) {
            $activeLink->writeIsActive(true);
            $activeLink = Menu::get()->readById($activeLink->readParent());
        };
        return self::mainMenuIteration(NULL, 0);
    }

    /**
     * Iteration function for mainMenu()
     * @param null|int $parent
     * @param int $depth
     * @return string
     */
    private static function mainMenuIteration($parent = NULL, $depth = 0)
    {
        $html = "";
        $children = Menu::get()->readAllByParentId($parent);
        foreach ($children as $link) {
            if ($link->readPageId()) {
                $page = Page::get()->readById($link->readPageId());
                $menuTitle = $page->readTitleShortInUsingLang();
                $menuLink = '/' . $page->readUrl();
            } else {
                $menuTitle = $link->readTitle();
                $menuLink = "";
            }
            $hasChildren = count(Menu::get()->readAllByParentId($link->readId())) > 0; // count menu item children
            if (!$depth && ( // if level 0 and
                    $hasChildren || // has children or
                    Settings::get()->readValue('use_buttons_in_menu') // show as buttons
                )
            ) {
                if (Settings::get()->readValue('use_buttons_in_menu')) // when show as buttons
                {
                    $html .= "    <li class='btn-group main-menu-button'>
                                    <button onclick='window.location=\"" . $menuLink . "\"' type='button' class='btn btn-default" . ($link->readIsActive() ? ' active' : '') . "'>" . $menuTitle . "</button>";
                    if ($hasChildren) {
                        $html .= "  <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                        <span class='caret'></span>
                                        <span class='sr-only'>Toggle Dropdown</span>
                                    </button>
                                    <ul class='dropdown-menu'>";
                        $html .= self::mainMenuIteration($link->readId(), $depth + 1);
                        $html .= "		</ul>";
                    }
                    $html .= "</li>";

                } else {
                    $html .= "  <li class='dropdown" . ($link->readIsActive() ? ' active' : '') . "'>
									<a href='" . $menuLink . "' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>
										" . $menuTitle . " <span class='caret'></span>
									</a>
									<ul class='dropdown-menu" . ($link->readIsDropdownRight() ? ' dropdown-menu-right' : '') . "' role='menu'>";
                    $html .= self::mainMenuIteration($link->readId(), $depth + 1);
                    $html .= "		</ul>
                          </li>";
                }
            } else { // if in depth or has no children
                $html .= "<li" . (!$depth || $link->readPageId() ? ($link->readIsActive() ? " class='active'" : "") : " class='disabled'") . "><a href='" . $menuLink . "'>" . ($depth > 1 ? str_repeat(Settings::get()->readValue('menu_depth'), $depth - 1) : "") . $menuTitle . "</a></li>";
                $html .= self::mainMenuIteration($link->readId(), $depth + 1);
            }
        }
        return $html;
    }

    /**
     * Returns html code of admin menu
     * @return string
     */
    public static function adminMenu()
    {
        $return = "";
        foreach (self::$adminMenu as $row) {
            $return .= "<li><a href='" . $row['url'] . "'>" . $row['title'] . "</a></li>";
        }
        return $return;
    }

    /**
     * Returns html code of admin menu for list group
     * @return string
     */
    public static function adminMenuList()
    {
        $return = "";
        foreach (self::$adminMenu as $row) {
            $return .= "<a class='list-group-item' href='" . $row['url'] . "'>" . $row['title'] . "</a>";
        }
        return $return;
    }

    /**
     * Returns html of user menu (with admin link if user is admin)
     * @return string
     */
    public static function userMenu()
    {
        if (User\Current::get()->readId())
        {
            return (User\Current::get()->readIsAdmin() ? "<li><a href='/engine-api/admin/cp'><#admin-cp#></a></li>" : "").
                "
						<li><a href='/engine-api/login/recoveryform'><#change-password#></a></li>
						<li><a href='/engine-api/login/logout'><#logout#></a></li>
						";
        } else {
            return "
					<li>
						<form method='post' action='/engine-api/login/login'>
						  <div class='form-group'>
						    <label><#email#></label>
						    <input type='email' class='form-control' name='log'>
						  </div>
						  <div class='form-group'>
						    <label><#password#></label>
						    <input type='password' class='form-control' name='pwd'>
						  </div>
						  <p><button type='submit' class='btn btn-primary'><#login#></button></p>
						  <p><a href='/engine-api/login/recoveryform'><#recover-password#></a></p>
						</form>
					</li>";
        }
    }

    /**
     * Returns search control for navigation bar
     * @return string
     */
    public static function search()
    {
        return '<form class="navbar-form navbar-right" role="search" action="/engine-api/searchresult" method="post">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="<#search#>" name="text">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </form>';
    }

    /**
     * Returns standalone search control
     * @return string
     */
    public static function searchStandalone()
    {
        return '<form role="search" action="/engine-api/searchresult" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="<#search#>" name="text">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </form>';
    }

    /**
     * Returns html code of captcha input fields
     * @param $rid
     * @return string
     */
    public static function captcha($rid)
    {
        $rid = intval($rid);
        return <<<HTM
			<input type='hidden' name='captcha_rid' value='{$rid}' />
			<div class='form-group'>
				<label><#captcha-enter#></label>
				<p class='help-block'><#captcha-help#></p>
				<p><img src='/engine-api/captcha?rid={$rid}' border=0></p>
				<input id='captcha' name='captcha' class='form-control'>
			</div>
HTM;
    }
}
