<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 12.11.15
 * Time: 00:50
 */

namespace RobinTail\EngineAPI\Output;


/**
 * Class File
 * @package RobinTail\EngineAPI\Output
 */
class File {

    /**
     * Mime types by extension
     * @var string[]
     */
    private static $mimeTypes = array(
        'txt'   => 'text/plain',
        'htm'   => 'text/html',
        'html'  => 'text/html',
        'php'   => 'text/html',
        'css'   => 'text/css',
        'js'    => 'application/javascript',
        'json'  => 'application/json',
        'xml'   => 'application/xml',
        'swf'   => 'application/x-shockwave-flash',
        'flv'   => 'video/x-flv',
        'png'   => 'image/png',
        'jpe'   => 'image/jpeg',
        'jpeg'  => 'image/jpeg',
        'jpg'   => 'image/jpeg',
        'gif'   => 'image/gif',
        'bmp'   => 'image/bmp',
        'ico'   => 'image/vnd.microsoft.icon',
        'tiff'  => 'image/tiff',
        'tif'   => 'image/tiff',
        'svg'   => 'image/svg+xml',
        'svgz'  => 'image/svg+xml',
        'zip'   => 'application/zip',
        'rar'   => 'application/x-rar-compressed',
        'exe'   => 'application/x-msdownload',
        'msi'   => 'application/x-msdownload',
        'cab'   => 'application/vnd.ms-cab-compressed',
        'mp3'   => 'audio/mpeg',
        'qt'    => 'video/quicktime',
        'mov'   => 'video/quicktime',
        'pdf'   => 'application/pdf',
        'psd'   => 'image/vnd.adobe.photoshop',
        'ai'    => 'application/postscript',
        'eps'   => 'application/postscript',
        'ps'    => 'application/postscript',
        'doc'   => 'application/msword',
        'rtf'   => 'application/rtf',
        'xls'   => 'application/vnd.ms-excel',
        'ppt'   => 'application/vnd.ms-powerpoint',
        'odt'   => 'application/vnd.oasis.opendocument.text',
        'ods'   => 'application/vnd.oasis.opendocument.spreadsheet',
        'ttf'   => 'font/ttf',
        'eot'   => 'font/eot',
        'otf'   => 'font/otf',
        'woff'  => 'font/woff',
        'woff2' => 'font/woff2'
    );

    /**
     * Default mime type
     * @var string
     */
    private static $defaultMimeType = "text/plain";

    /**
     * Returns mime type of the file
     * @param $filename
     * @return string
     */
    public static function getMimeType($filename)
    {
        $pre = explode('.', $filename);
        $ext = strtolower(array_pop($pre));
        if (array_key_exists($ext, self::$mimeTypes)) {
            return self::$mimeTypes[$ext];
        }
        if ($finfo = finfo_open(FILEINFO_MIME))
        {
            if ($mimetype = finfo_file($finfo, $filename)) {
                finfo_close($finfo);
                return $mimetype;
            }
        }
        return self::$defaultMimeType;
    }

    /**
     * Sends file or returns false
     * @param $filename
     * @return bool
     */
    public static function send($filename)
    {
        if (file_exists($filename)) {
            header('Content-Type: '.self::getMimeType($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            exit();
        }
        return false;
    }

} 