<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 23.08.15
 * Time: 10:46
 */

namespace RobinTail\EngineAPI\Output;


use RobinTail\EngineAPI\Output;

/**
 * Class Styles
 * @package RobinTail\EngineAPI\Output
 */
class Styles {

    /**
     * @var Styles
     */
    private static $instance;

    /**
     * Styles array
     * @var string[]
     */
    private $styles = array();

    /**
     * @return Styles
     */
    public static function get()
    {
        if (!self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Add style to output
     * @param string $htmlTagLinkOrStyle
     * @param bool $toTop
     */
    public function addStyle($htmlTagLinkOrStyle, $toTop = false)
    {
        if ($toTop) {
            $this->styles = array_merge(array($htmlTagLinkOrStyle), $this->styles);
        } else {
            $this->styles[] = $htmlTagLinkOrStyle;
        }
    }

    /**
     * Add style (as link) to output
     * @param string $url
     * @param bool $toTop
     */
    public function addStyleLink($url, $toTop = false)
    {
        $this->addStyle('<link href="' . $url . '" rel="stylesheet">', $toTop);
    }

    /**
     * Add style (as text) to output
     * @param string $text
     * @param bool $toTop
     */
    public function addStyleText($text, $toTop = false)
    {
        $this->addStyle('<style>' . $text . '</style>');
    }

    /**
     * Add common engine styles to output, vendor packages and other
     * In backward order of usage
     * With second argument TRUE
     */
    private function addCommonStyles()
    {
        $lytebox = '<link href="/vendor/markusfhay/lytebox/lytebox.css" rel="stylesheet">';
        $this->addStyle($lytebox, true);
        $bootstrap = '<link href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">';
        $this->addStyle($bootstrap, true);
    }

    /**
     * @return string
     */
    public function render()
    {
        $this->addCommonStyles();
        return implode("\r\n", $this->styles);
    }
}