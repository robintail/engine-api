<?

namespace RobinTail\EngineAPI\Output;

use RobinTail\EngineAPI\Arr2dim;
use RobinTail\EngineAPI\Input;

class Forms
{

	public static function block($options)
	{
		return "<table class='bordered' style='width: ".$options[width]."'><thead><tr><th>".$options[title]."</th></tr></thead><tbody><tr><td>".$options[body]."</td></tr></tbody></table>";
	}


	public static function actionCombo($options)
	{
		$click = "<a href='#' id=\"".$options[name]."_title\"".($options[help] ? " title=\"".$options[help]."\"":"")." class=\"action".($options['class']?" ".$options['class']:"")."\" onclick=\"showhide('".$options[name]."_list', 'inline', 'none', ".(isset($options[only_one_opened])?($options[only_one_opened]?$options[only_one_opened]:'false'):'true').", '".$options[name]."_corner'); return false;\">".$options[title]."</a><a href='#' class=\"small nolink".($options['class']?" ".$options['class']:"")."\" onclick=\"showhide('".$options[name]."_list', 'inline', 'none', ".(isset($options[only_one_opened])?($options[only_one_opened]?$options[only_one_opened]:'false'):'true').", '".$options[name]."_corner'); return false;\">&nbsp;&#9660;</a>";
		$columns = array(); $row = array(); 
		$out=""; $filter="";
		foreach($options[menu] as $entry)
		{
			if (is_array($entry))
			{ // массив
				if (count($entry)>1)
				{ // больше одной записи в массиве
					$columns[] = $entry; // новая колонка
				} else { // одна запись в массиве
					$row[] = $entry[0]; // кидаем в нижнюю строку
				}
			} else { // те, что не массивы - кинуть в первую колонку :)
				$columns[0][] = $entry;
			}
		}
		foreach($columns as $column)
		{
			$out .= "<td valign=top>".implode("<br>",$column)."</td>";
		}
		$corner = "<img id='".$options[name]."_corner' class='action_corner' src='/vendor/robintail/engine-api/images/design3/corner.png' style='display: ".($options[opened]?'inline':'none').";'>";
		$list = "<div id='".$options[name]."_list' class='action_list' style='display: ".($options[opened]?'inline':'none').";'><table><tr>".$out."</tr>".(count($row)?"<tr><td colspan=".count($columns)."><hr>".implode(", ", $row)."</td></tr>":"")."</table></div>";
		return "<div id='".$options[name]."_holder' class='inline relative'>".$list.$corner.$click."</div>";
	}

	public static function dateSelect($options)
	{
		return "<input id='".($options[id] ? $options[id] : $options[name])."' name='".$options[name]."' value='".$options[value]."'> <img class='icon hand' src='/vendor/robintail/engine-api/images/small/calendar.gif' onclick=\"displayCalendar(document.getElementById('".($options[id] ? $options[id] : $options[name])."'),'dd.mm.yyyy',this);\">";
	}

	public static function input($options)
	{
        return \RobinTail\EngineAPI\Output\Forms\Input::create()
                        ->setClass($options['class'])
                        ->setDisabled($options['disabled'])
                        ->setType(($options['hidden']?'hidden':($options['password']?'password':($options['email']?'email':'text'))))
                        ->setName($options['name'])
                        ->setId($options['id'] ? $options['id'] : $options['name'])
                        ->setValue($options['value'])
                        ->setOnChange($options['onchange'])
                        ->render();
	}

	public static function checkbox($options)
	{
        return \RobinTail\EngineAPI\Output\Forms\Input::create()
                        ->setType('checkbox')
                        ->setValue($options['value'] ? $options['value'] : 1)
                        ->setName($options['name'])
                        ->setId($options['id'] ? $options['id'] : $options['name'])
                        ->setChecked($options['checked'])
                        ->setDisabled($options['disabled'])
                        ->setOnChange($options['onchange'])
                        ->render();
	}

	public static function forwardInput()
	{
		$result = array();
		foreach(Input::get()->readAll() as $k => $v)
		{
			if (is_array($v))
			{
				foreach($v as $k2 => $v2)
				{
					$result[] = self::input(array('name'=>$k."[".$k2."]",'value'=>$v2,'hidden'=>true));
				}
			} else {
				$result[] = self::input(array('name'=>$k, 'value'=>$v, 'hidden'=>true));
			}
		}
		return implode("\n",$result);
	}

	public static function text($options)
	{
        return \RobinTail\EngineAPI\Output\Forms\Textarea::create()
                        ->setBr2nl($options['br2nl'])
                        ->setValue($options['value'])
                        ->setClass($options['class'])
                        ->setName($options['name'])
                        ->setId($options['id'] ? $options['id'] : $options['name'])
                        ->setTiny($options['tiny'])
                        ->render();
	}

	public static function suggest($options)
	{
		if (strpos($options[url],'?')===false) { $options[url] .= "?"; } else { $options[url] = $options[url].'&'; }
		$hidden = self::input(array('name'=>$options[name_key],'value'=>$options[default_key],'hidden'=>true,'class'=>'suggest_value'));
		$field = self::input(array('name'=>$options[name_text],'value'=>$options[default_text],'hidden'=>false,'class'=>$options['class']));
		$script = "<script language=\"javascript\" type=\"text/javascript\">
						var options_".$options[name_text]." = {
							script:\"".$options[url]."\",
							varname:\"".$options[argument]."\",
							".(count($options[arguments_other]) ? "other: [\"".implode("\",\"", $options[arguments_other])."\"]," : "")."
							json:false,
							timeout:10000,
							minchars:settings.search_min,
							delay:500,
							cache:false,
							shownoresults:true,
							noresults:\"Ничего не найдено\",
							".($options['onenter'] ? "onenter: function () { ".$options['onenter']." }," : "").
							"callback: function (obj) { document.getElementById('".$options[name_key]."').value = obj.id; ".$options['onchange']." }							
						};
						var as_".$options[name_text]." = new AutoSuggest('".$options[name_text]."', options_".$options[name_text].");
					</script>";
		return $hidden.$field.$script;
	}

	public static function select($options)
	{
        return \RobinTail\EngineAPI\Output\Forms\Select::create()
                    ->setArray($options['array'])
                    ->setOnlyOptions($options['only_options'])
                    ->setName($options['name'])
                    ->setId($options['id'] ? $options['id'] : $options['name'])
                    ->setSubmit($options['submit'])
                    ->setClass($options['class'])
                    ->setMulti($options['multi'])
                    ->setValueField($options['valuefield'])
                    ->setCaptionField($options['captionfield'])
                    ->setSelected($options['selected'])
                    ->setOffsetField($options['offset_field'])
                    ->render();
	}


	public static function quickFilter(&$options)
	{
		$arr = Arr2dim::distinct($options['array'], $options[valuefield], $options[captionfield]);
		$arr = Arr2dim::inc($arr,array($options[valuefield]=>0, $options[captionfield]=>''),true);
		$html = self::select(array(
				'array' => $arr, 'name'	=> $options['name'], 'valuefield' => $options['valuefield'], 'captionfield' => $options['captionfield'], 
				'selected' => intval(Input::get()->read($options['name'])), 'multi' => false, 'class' => $options['class'], 'submit' => $options['submit']
				));
		if (intval(Input::get()->read($options['name']))) $options['array'] = Arr2dim::filter($options['array'], $options['valuefield'], intval(Input::get()->read($options['name'])));
		return $html;
	}




}

?>