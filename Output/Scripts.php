<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 23.08.15
 * Time: 10:40
 */

namespace RobinTail\EngineAPI\Output;


use RobinTail\EngineAPI\Output;
use RobinTail\EngineAPI\Settings;

/**
 * Class Scripts
 * @package RobinTail\EngineAPI\Output
 */
class Scripts {

    /**
     * @var Scripts
     */
    private static $instance;

    /**
     * Scripts array
     * @var string[]
     */
    private $scripts = array();
    /**
     * JavaScript variables array
     * @var array
     */
    private $vars = array();

    /**
     * @return Scripts
     */
    public static function get()
    {
        if (!self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Add JavaScript variable
     * @param string $name
     * @param mixed $value
     */
    public function addVar($name, $value)
    {
        $this->vars[$name] = $value;
    }

    /**
     * Convert JavaScript variables to script with declared object: settings
     * Add this script to output
     */
    private function varsToScript()
    {
        if (!count($this->vars)) return false;
        $script = "<script>var settings = " . json_encode($this->vars, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . ";</script>";
        $this->addScript($script, true); // add to top
    }

    /**
     * Add script to output
     * @param string $htmlTagScript
     * @param bool $toTop
     */
    public function addScript($htmlTagScript, $toTop = false)
    {
        if ($toTop) {
            $this->scripts = array_merge(array($htmlTagScript), $this->scripts);
        } else {
            $this->scripts[] = $htmlTagScript;
        }
    }

    /**
     * Add common engine scripts to output, vendor packages and other
     * In backward order of usage
     * With second argument TRUE
     */
    private function addCommonScripts()
    {
        $commonjs = '<script src="/vendor/robintail/engine-api/js/common.js"></script>';
        $this->addScript($commonjs, true);
        $numeral_lang = '<script src="/vendor/adamwdraper/Numeral-js/min/languages/ru.min.js"></script>';
        $this->addScript($numeral_lang, true);
        $numeral = '<script src="/vendor/adamwdraper/Numeral-js/min/numeral.min.js"></script>';
        $this->addScript($numeral, true);
        $lytebox = '<script type="text/javascript" language="javascript" src="/vendor/markusfhay/lytebox/lytebox.js"></script>';
        $this->addScript($lytebox, true);
        $jsapi = '<script src="https://www.google.com/jsapi"></script>';
        $this->addScript($jsapi, true);
        $tinymce = '<script src="/vendor/tinymce/tinymce/tinymce.min.js"></script>';
        $this->addScript($tinymce, true);
        $ace = '<script src="/vendor/robintail/ace-builds/src-min/ace.js" type="text/javascript" charset="utf-8"></script>';
        $this->addScript($ace, true);
        $legacy = <<<HTM
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
HTM;
        $this->addScript($legacy, true);
        $bootstrap = '<script src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>';
        $this->addScript($bootstrap, true);
        $jquery = '<script src="/vendor/components/jquery/jquery.min.js"></script>';
        $this->addScript($jquery, true);
    }

    /**
     * @return string
     */
    public function render()
    {
        $this->varsToScript();
        $this->addCommonScripts();
        return implode("\r\n", $this->scripts);
    }
}
