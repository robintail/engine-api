<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 17.11.15
 * Time: 18:33
 */

namespace RobinTail\EngineAPI;


/**
 * Class Admin
 * @package RobinTail\EngineAPI
 */
class Admin {

    /**
     * Check admin access right
     * This function have to be executed in the beginning of each admin process
     */
    public static function init()
    {
        // check rights
        if (!User\Current::get()->readIsAdmin()) die("Access denied.");
        // set output template
        Output::get()->writeTemplate(Template::get()->getAdminCPCode());
    }


    /**
     * Admin Control Panel
     */
    public static function CP()
    {
        self::init();

        $git = new Git();
        $revisions = $git->getLastRevisions();
        $version = $git->getPackageVersion();
        $commit = $git->getCurrentCommit();
        $commit = $commit ? $commit : "FAILED";
        $phpVersion = phpversion();
        $gitVersion = $git->getVersion();
        $extensions = implode(", ",get_loaded_extensions());
        $ext_count = count(get_loaded_extensions());
        $changes = implode("<br>", $revisions);
        $root = Path::getRoot();

        $html = <<<HTM
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div id="panel-engine-api" class="panel panel-primary panel-admin">
                            <div class="panel-heading">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Engine.API
                            </div>
                            <div class="panel-body">
                                <p><#version#>: <strong>{$version}</strong></p>
                                <p><#commit#>: <strong><small>{$commit}</small></strong></p>
                                <p><#last-changes#>:</p>
                                <div class="well well-sm">
                                    {$changes}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary panel-admin">
                            <div class="panel-heading">
                                <span class="glyphicon glyphicon-globe" aria-hidden="true"></span> <#hosting#>
                            </div>
                            <div class="panel-body">
                                <p><#host#>: <strong>{$_SERVER['SERVER_NAME']}</strong></p>
                                <p><#root-path#>: <strong><small>{$root}</small></strong></p>
                                <p>PHP <#version#>: <strong>{$phpVersion}</strong></p>
                                <p>git <#version#>: <strong>{$gitVersion}</strong></p>
                                <p><#extensions#>: <a href="#" onclick="toggle_extensions();">({$ext_count})</a></p>
                                <div id='extensions' class="well well-sm small" style="display: none;">
                                    {$extensions}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
HTM;

        Output\Scripts::get()->addVar('version', $version);
        Output\Scripts::get()->addScript('<script src="/vendor/robintail/engine-api/js/admin/cp.js"></script>');
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Languages control centre
     */
    public static function language()
    {
        self::init();

        $html = <<<HTM
            <p><a href='/engine-api/admin/language_edit' class='pull-right btn btn-lg btn-primary'><#new-language#></a></p>
            <table class="table">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><#code#></th>
                        <th><#language#></th>
                        <th class='text-center'><#flag#></th>
                        <th class='text-center'><#enabled#></th>
                        <th class='text-center'><#domestic#></th>
                        <th class='text-center'><#international#></th>
                        <th class='text-center'><#protected#></th>
                    </tr>
                </thead>
                <tbody>
HTM;

        foreach(Lang::get()->readAll() as $item)
        {
            $html .= "<tr>
                            <td>
                                <a href='/engine-api/admin/language_edit?id_lang=".$item->readId()."'><span class='glyphicon glyphicon-edit'></span></a>
                                <a href='/engine-api/admin/language_remove?id_lang=".$item->readId()."' onclick=\"return confirm('<#confirm-language-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                            </td>
                            <td>".$item->readCode()."</td>
                            <td>".$item->readName()."</td>
                            <td class='text-center'><img src='".$item->readFlag()."'></td>
                            <td class='text-center'><a href='/engine-api/admin/language_enable?id_lang=".$item->readId()."'><span class='".($item->readIsEnabled() ? 'glyphicon glyphicon-ok-sign text-success' : 'glyphicon glyphicon-remove-sign text-danger')."'></span></a></td>
                            <td class='text-center'><a href='/engine-api/admin/language_domestic?id_lang=".$item->readId()."'><span class='".($item->readIsDomestic() ? 'glyphicon glyphicon-ok-sign text-success' : 'glyphicon glyphicon-ok-circle text-muted')."'></span></a></td>
                            <td class='text-center'><a href='/engine-api/admin/language_international?id_lang=".$item->readId()."'><span class='".($item->readIsInternational() ? 'glyphicon glyphicon-ok-sign text-success' : 'glyphicon glyphicon-ok-circle text-muted')."'></span></a></td>
                            <td class='text-center'><span class='".($item->readIsProtected() ? 'glyphicon glyphicon-lock text-danger' : '')."'></span></td>
                    </tr>";
        }

        $html .= <<<HTM
            </tbody>
        </table>
HTM;


        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Language edit
     */
    public static function languageEdit()
    {
        self::init();

        $lang = Lang::get()->readById(Input::get()->readInt('id_lang'));
        if ($lang->readIsProtected()) Output::get()->error('<#error-language-protected#>');

        if (Input::get()->readInt('update'))
        {
            if (Lang::get()->isExists(
                Input::get()->read('lang_code'),
                Input::get()->readInt('id_lang')
            )){
                Output::get()->error('<#error-lang-code-exists#>');
            }

            Lang::get()->write(
                Input::get()->readInt('id_lang'),
                Input::get()->read('lang_code'),
                Input::get()->read('lang_name'),
                Input::get()->read('lang_flag')
            );
            Output::get()->redirect('/engine-api/admin/language');
        }

        $id = $lang->readId();
        $code = $lang->readCode();
        $name = $lang->readName();
        $flag = $lang->readFlag();

        $html = <<<HTM
            <form action='' method='post'>
                <input type='hidden' name='id_lang' value='{$id}'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group has-feedback'>
                    <label class='control-label'><#code#></label>
                    <input class='form-control' name='lang_code' value='{$code}' onchange="check_lang_code();">
                    <span class="glyphicon form-control-feedback"></span>
                    <div class='help-block'></div>
                </div>
                <div class="form-group">
                    <label><#language#></label>
                    <input class="form-control" name="lang_name" value="{$name}">
                </div>
                <div class="form-group">
                    <label><#flag#></label>
                    <p class="help-block"><#flag-help#></p>
                    <input class="form-control" name="lang_flag" value="{$flag}">
                </div>
                <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
            </form>
HTM;

        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/language_edit.js'></script>");
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Language remove
     */
    public static function languageRemove()
    {
        self::init();

        $id = Input::get()->readInt('id_lang');
        if (!$id) Output::get()->error('id_lang - <#error-smth-not-specified#>');
        $result = Lang::get()->remove($id, $err);
        if ($result)
        {
            Output::get()->redirect('/engine-api/admin/language');
        } else {
            Output::get()->error($err);
        }
    }


    /**
     * Toggle language enabled state
     */
    public static function languageEnable()
    {
        self::init();

        $id = Input::get()->readInt('id_lang');
        if (!$id) Output::get()->error('id_lang - <#error-smth-not-specified#>');
        $result = Lang::get()->toggleEnabled($id, $err);
        if ($result)
        {
            Output::get()->redirect('/engine-api/admin/language');
        } else {
            Output::get()->error($err);
        }
    }


    /**
     * Toggle language domestic state
     */
    public static function languageDomestic()
    {
        self::init();

        $id = Input::get()->readInt('id_lang');
        if (!$id) Output::get()->error('id_lang - <#error-smth-not-specified#>');
        $result = Lang::get()->setDomesticDefault($id, $err);
        if ($result)
        {
            Output::get()->redirect('/engine-api/admin/language');
        } else {
            Output::get()->error($err);
        }
    }


    /**
     * Toggle language international state
     */
    public static function languageInternational()
    {
        self::init();

        $id = Input::get()->readInt('id_lang');
        if (!$id) Output::get()->error('id_lang - <#error-smth-not-specified#>');
        $result = Lang::get()->setInternationalDefault($id, $err);
        if ($result)
        {
            Output::get()->redirect('/engine-api/admin/language');
        } else {
            Output::get()->error($err);
        }
    }


    /**
     * Language constants control centre
     */
    public static function languageConst()
    {
        self::init();

        $lang_headers = "";
        foreach(Lang::get()->readAll() as $entry)
        {
            if ($entry->readIsEnabled()) {
                $lang_headers .= "<th>" . $entry->readName() . "</th>";
            }
        }
        $consts = Lang\Constant::get()->readAll();

        $html = "
		<p><a href='/engine-api/admin/language_const_edit' class='pull-right btn btn-lg btn-primary'><#new-lang-const#></a></p>
		<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><#code#></th>
					{$lang_headers}
				</tr>
			</thead><tbody>";
        foreach($consts as $const)
        {
            $html .= "<tr>
				<td class='text-nowrap'>
					<a href='/engine-api/admin/language_const_edit?id_const=".$const->readId()."'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/engine-api/admin/language_const_remove?id_const=".$const->readId()."' onclick=\"return confirm('<#confirm-lang-const-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
				</td>
				<td class='text-nowrap'>".$const->readCode()."</td>";
            foreach(Lang::get()->readAll() as $entry) {
                if ($entry->readIsEnabled()) {
                    $html .= "<td>" . $const->readTrans($entry->readCode()) . "</td>";
                }
            }
            $html .= "
    		</tr>";
        }
        $html .= "</tbody></table>";

        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Language constant edit
     */
    public static function languageConstEdit()
    {
        self::init();

        if (Input::get()->readInt('update'))
        {
            if (Lang\Constant::get()->isExists(
                Input::get()->read('const_code'),
                Input::get()->readInt('id_const')
            )){
                Output::get()->error("<#error-lang-const-code-exists#>");
            }

            Lang\Constant::get()->write(
                Input::get()->readInt('id_const'),
                Input::get()->read('const_code'),
                Input::get()->read('translations')
            );
            Output::get()->redirect('/engine-api/admin/language_const');
        }


        $const = Lang\Constant::get()->readById(Input::get()->readInt('id_const'));
        $id = $const->readId();
        $code = $const->readCode();

        $html = <<<HTM
            <form action='' method='post'>
                <input type='hidden' name='id_const' value='{$id}'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group has-feedback'>
                    <label class='control-label'><#code#></label>
                    <input class='form-control' name='const_code' value='{$code}' onchange="check_const_code();">
                    <span class="glyphicon form-control-feedback"></span>
                    <div class='help-block'></div>
                </div>
HTM;

        foreach(Lang::get()->readAll() as $entry)
        {
            if ($entry->readIsEnabled()) {
                $html .= "
                    <div class='form-group'>
                        <label>" . $entry->readName() . "</label>
                        <input class='form-control' name='translations[" . $entry->readCode() . "]' value='" . $const->readTrans($entry->readCode()) . "'>
                    </div>";
            }
        }

        $html .= <<<HTM
                <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
            </form>
HTM;
        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/language_const_edit.js'></script>");
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Language constant remove
     */
    public static function languageConstRemove()
    {
        self::init();

        Lang\Constant::get()->remove(Input::get()->readInt('id_const'));
        Output::get()->redirect('/engine-api/admin/language_const');
    }


    /**
     * Internal method for menu()
     * @param int|null $parent
     * @param int $depth
     * @return string
     */
    private static function __menu_iteration_output($parent=NULL, $depth=0)
    {
        $html = "";
        $plink = Menu::get()->readById($parent);
        $children = Menu::get()->readAllByParentId($parent);
        foreach($children as $link)
        {
            $ordering = Output\Forms::select(array('array'=>range(0,count($children)), 'name'=>'link_ordering', 'selected'=>$link->readOrdering(), 'submit'=>"ordering_change(".$link->readId().", this.value);", 'class' => 'form-control'));
            if ($plink->readPageId())
            {
                $parentTitle = Page::get()->readById($plink->readPageId())->readTitleShortInUsingLang();
            } else {
                $parentTitle = "<em>".$plink->readTitle()."</em>";
            }
            if ($link->readPageId())
            {
                $menuTitle = "<a href='/engine-api/admin/page_edit?id_page=".$link->readPageId()."'>".Page::get()->readById($link->readPageId())->readTitleShortInUsingLang()."</a>";
            } else {
                $menuTitle = "<input onchange='title_change(".$link->readId().", this.value);' class='form-control' value='".$link->readTitle()."'>";
            }
            if ($depth==0)
            {
                $dropdownRight = Output\Forms\Input::create()
                    ->setType('checkbox')
                    ->setValue(1)
                    ->setChecked($link->readIsDropdownRight())
                    ->setOnChange("dropdown_right_change(".$link->readId().", this.checked);")
                    ->render();
            } else {
                $dropdownRight = "&nbsp;";
            }
            $html .= "<tr>
					<td class='text-nowrap'>
						<a href='/engine-api/admin/menu_remove?id_link=".$link->readId()."' onclick=\"return confirm('<#confirm-menu-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
					</td>
					<td>".$parentTitle."</td>
					<td>".$menuTitle."</td>
					<td>".$ordering."</td>
					<td class='text-center'>".$dropdownRight."</td>
				</tr>";
            $html .= self::__menu_iteration_output($link->readId(), $depth+1);
        }
        return $html;
    }


    /**
     * Menu control centre
     */
    public static function menu()
    {
        self::init();

        $html = "<p><a href='/engine-api/admin/menu_edit' class='pull-right btn btn-lg btn-primary'><#new-element#></a></p>
		<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><#parent#></th>
					<th><#element#></th>
					<th><#ordering#></th>
					<th class='text-center'><#dropdown-right#></th>
				</tr>
			</thead><tbody>";
        $html .= self::__menu_iteration_output();
        $html .= "</tbody></table>";

        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/menu.js'></script>");
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Internal method for menuEdit()
     * @param int|null $parent
     * @param int $depth
     * @return array
     */
    private static function __menuEdit_iteration($parent=NULL,$depth=0)
    {
        $children = Menu::get()->readAllByParentId($parent);
        $arr=array();
        foreach($children as $link)
        {
            if ($link->readPageId()) {
                $menuTitle = Page::get()->readById($link->readPageId())->readTitleShortInUsingLang();
            } else {
                $menuTitle = $link->readTitle();
            }
            $arr[] = array('id_link'=>$link->readId(), 'title'=>str_repeat(Settings::get()->readValue('menu_depth'),$depth).$menuTitle);
            $arr = array_merge($arr, self::__menuEdit_iteration($link->readId(), $depth+1));
        }
        return $arr;
    }


    /**
     * Menu item edit
     */
    public static function menuEdit()
    {

        self::init();

        if (Input::get()->readInt('update'))
        { // insert
            if (Input::get()->read('element_type')=='page')
            {
                Menu::get()->createPageItem(
                    Input::get()->readInt('parent_link_id'),
                    Input::get()->readInt('link_page_id')
                );
            } elseif (Input::get()->read('element_type')=='text')
            {
                Menu::get()->createTextItem(
                    Input::get()->readInt('parent_link_id'),
                    Input::get()->read('link_title_short')
                );
            }
            Output::get()->redirect('/engine-api/admin/menu');
        }

        $parents = Output\Forms::select(array(
            'name'=>'parent_link_id',
            'array'=>array_merge(
                array(array('id_link'=>NULL,'title'=>'<#no-parent#>')),
                self::__menuEdit_iteration()
            ),
            'valuefield'=>'id_link',
            'captionfield'=>'title',
            'class'=>'form-control'
        ));

        $sortedPages = Page::get()->readAll();
        usort(
            $sortedPages,
            /**
             * @param Page\Item $a
             * @param Page\Item $b
             * @return int
             */
            function($a, $b) {
                /**
                 * @var Page\Item $a
                 * @var Page\Item $b
                 */
                return mb_strtolower($a->readTitleShortInUsingLang(), 'utf-8') <= mb_strtolower($b->readTitleShortInUsingLang(), 'utf-8') ? -1 : 1;
            }
        );

        $pages = Output\Forms::select(array(
            'name'=>'link_page_id',
            'array'=>$sortedPages,
            'valuefield'=>'readId',
            'captionfield'=>'readTitleShortInUsingLang',
            'class'=>'form-control'
        ));

        $html = <<<HTM
            <form action='' method='post'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group'>
                    <label><#parent#></label>
                    {$parents}
                </div>
                <div class='radio'>
                    <label>
                        <input type='radio' name='element_type' value='page' checked> <#page#>
                    </label>
                </div>
                <div class='form-group'>
                    <label><#page#></label>
                    {$pages}
                </div>
                <div class='radio'>
                    <label>
                        <input type='radio' name='element_type' value='text'> <#text#>
                    </label>
                </div>
                <div class='form-group'>
                    <label><#text#></label>
                    <input name='link_title_short' class='form-control'>
                </div>
                <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
            </form>
HTM;
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * Menu item remove
     */
    public static function menuRemove()
    {
        self::init();

        Menu::get()->remove(Input::get()->readInt('id_link'));
        Output::get()->redirect('/engine-api/admin/menu');
    }


    /**
     * pages control centre
     */
    public static function pages()
    {
        self::init();

        $pages = Page::get()->readAll();
        $html = "
            <p><a href='/engine-api/admin/page_edit' class='pull-right btn btn-lg btn-primary'><#new-page#></a></p>
            <table class='table'>
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>URL</th>
                        <th><#title#></th>
                        <th>SEO</th>
                        <th><#template#></th>
                    </tr>
                </thead><tbody>";
        foreach($pages as $item)
        {
            $idPage = $item->readId();
            $url = $item->readUrl();
            $titleShort = $item->readTitleShortInUsingLang();
            $templateCode = Template::get()->readById($item->readTemplateId())->readCode();
            $description = $item->readDescriptionInUsingLang();
            $keywords = $item->readKeywordsInUsingLang();
            $html .= "<tr>
				<td class='text-nowrap'>
					<a href='/engine-api/admin/page_edit?id_page={$idPage}'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/engine-api/admin/page_remove?id_page={$idPage}' onclick=\"return confirm('<#confirm-page-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
				</td>
				<td class='text-nowrap'><a href='/{$url}'>{$url}</a></td>
				<td>{$titleShort}</td>
				<td><#description#>: {$description}<br><#keywords#>: {$keywords}</td>
				<td>{$templateCode}</td>
			</tr>";
        }
        $html .= "</tbody></table>";
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * internal method for pageEdit()
     * @param int|null $parent
     * @param int $depth
     * @return array
     */
    private static function __pageEdit_iteration($parent=NULL,$depth=0)
    {
        $children = Menu::get()->readAllByParentId($parent);
        $arr=array();
        foreach($children as $link)
        {
            if ($link->readPageId()) {
                $menuTitle = Page::get()->readById($link->readPageId())->readTitleShortInUsingLang();
            } else {
                $menuTitle = $link->readTitle();
            }
            $arr[] = array('id_link'=>$link->readId(), 'title'=>str_repeat(Settings::get()->readValue('menu_depth'),$depth).$menuTitle);
            $arr = array_merge($arr, self::__pageEdit_iteration($link->readId(), $depth+1));
        }
        return $arr;
    }

    /**
     * page edit
     */
    public static function pageEdit()
    {
        self::init();

        if (Input::get()->readInt('update'))
        {
            if (Page::get()->isExists(
                Input::get()->read('page_url'),
                Input::get()->readInt('id_page')
            )){
                Output::get()->error("<#error-page-url-exists#>");
            }

            $page = Page\Item::create()
                ->writeId(Input::get()->readInt('id_page'))
                ->writeUrl(Input::get()->read('page_url'))
                ->writeOverrideActiveLinkId(Input::get()->readIntOrNull('override_active_link_id'))
                ->writeTemplateId(Input::get()->readInt('template_id'))
                ->writeIsUseAffix(Input::get()->readBool('is_use_affix'));

            foreach(Lang::get()->readAll() as $lang) {
                if ($lang->readIsEnabled()) {
                    $trans = Page\Item\Trans::create()
                        ->writeIsEnabled(Input::get()->read('is_trans_enabled')[$lang->readCode()])
                        ->writeLangId($lang->readId())
                        ->writeTitleShort(Input::get()->read('page_title_short')[$lang->readCode()])
                        ->writeTitleLong(Input::get()->read('page_title_long')[$lang->readCode()])
                        ->writeHtml(Input::get()->read('page_html')[$lang->readCode()])
                        ->writeKeywords(Input::get()->read('page_keywords')[$lang->readCode()])
                        ->writeDescription(Input::get()->read('page_description')[$lang->readCode()]);
                    if ($page->readIsUseAffix()) $trans->cacheAffixFromHtml();
                    $page->writeTrans($lang->readCode(), $trans);
                }
            }
            Page::get()->write($page);
            Output::get()->redirect('/engine-api/admin/pages');
        }

        $page = Page::get()->readById(Input::get()->readInt('id_page'));
        $override_active_link_html = Output\Forms::select(array(
            'name'=>'override_active_link_id',
            'array'=>array_merge(
                array(array('id_link'=>NULL,'title'=>'<#do-not-use#>')),
                self::__pageEdit_iteration()
            ),
            'selected'=>$page->readOverrideActiveLinkId(),
            'valuefield'=>'id_link',
            'captionfield'=>'title',
            'class'=>'form-control'
        ));
        $templates_html = Output\Forms::select(array(
            'name'=>'template_id',
            'array'=>Template::get()->readAll() ,
            'valuefield'=>'readId',
            'captionfield'=>'readCode',
            'selected' => $page->readTemplateId(),
            'class'=>'form-control'
        ));
        $is_use_affix = Output\Forms\Input::create()
            ->setName('is_use_affix')
            ->setType('checkbox')
            ->setValue(1)
            ->setChecked($page->readIsUseAffix())
            ->render();
        $lang_enabled_array = array();
        $html = <<<HTM
            <form action='' method='post' onsubmit='before_submit();'>
                <input type='hidden' name='id_page' value='{$page->readId()}'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group has-feedback'>
                    <label class='control-label'>URL</label>
                    <input class='form-control' name='page_url' value='{$page->readUrl()}' onchange="check_page_url();">
                    <span class="glyphicon form-control-feedback"></span>
                    <div class='help-block'></div>
                </div>
                <div class='form-group'>
                    <label><#override-active-link#></label>
                    <p class="help-block"><#override-active-link-help#></p>
                    {$override_active_link_html}
                </div>
                <div class='form-group'>
                    <label><#template#></label>
                    {$templates_html}
                </div>
                <div class='checkbox'>
                    <label>
                        {$is_use_affix} <#use-affix#>
                    </label>
                    <p class="help-block"><#use-affix-help#></p>
                </div>
HTM;

        foreach(Lang::get()->readAll() as $lang)
        {
            if ($lang->readIsEnabled())
            {
                $lang_enabled_array[] = $lang->readCode();
                $html .= "<h4 class='text-center'>&nbsp;<br><#language#>: ".$lang->readName()."</h4>";
                $transEnabledCheckbox = Output\Forms\Input::create()
                    ->setType('checkbox')
                    ->setName('is_trans_enabled['.$lang->readCode().']')
                    ->setValue(1)
                    ->setChecked($page->readTrans($lang->readCode())->readIsEnabled())
                    ->setOnChange("toggle_trans_enabled(this);")
                    ->render();
                $translationDisplay = $page->readTrans($lang->readCode())->readIsEnabled() ? 'block' : 'none';
                if (Settings::get()->readValue('use_raw_page_editor'))
                {
                    $editor = "<div id='page_html_ace_{$lang->readCode()}'>{$page->readTrans($lang->readCode())->readHtmlForEditor()}</div>
                        <textarea id='page_html_{$lang->readCode()}' name='page_html[{$lang->readCode()}]' style='display: none;'></textarea>";
                } else {
                    $editor = "<textarea class='form-control' style='height: 400px;' name='page_html[{$lang->readCode()}]'>{$page->readTrans($lang->readCode())->readHtmlForEditor()}</textarea>";
                }

                $html .= <<<HTM
                    <div class="checkbox">
                        <label>
                            {$transEnabledCheckbox}
                            <#enable-translation#>
                        </label>
                    </div>
                    <div id="translation_{$lang->readCode()}" style="display: {$translationDisplay}">
                        <div class='form-group'>
                            <label><#title-short#></label>
                            <input class='form-control' name='page_title_short[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readTitleShort()}'>
                        </div>
                        <div class='form-group'>
                            <label><#title-long#></label>
                            <input class='form-control' name='page_title_long[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readTitleLong()}'>
                        </div>
                        <div class='form-group'>
                            <label><#keywords#></label>
                            <input class='form-control' name='page_keywords[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readKeywords()}'>
                        </div>
                        <div class='form-group'>
                            <label><#description#></label>
                            <input class='form-control' name='page_description[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readDescription()}'>
                        </div>
                        <div class='form-group'>
                            <label><#contains#></label>
                            {$editor}
                        </div>
                    </div>
HTM;
            }
        }

        $html .= <<<HTM
            <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
        </form>
HTM;


        Output\Scripts::get()->addScript('<script type="text/javascript" src="/vendor/moxiecode/plupload/js/plupload.full.min.js"></script>', true);
        Output\Scripts::get()->addVar('languages', $lang_enabled_array);
        Output\Scripts::get()->addVar('use_raw_page_editor', Settings::get()->readValue('use_raw_page_editor'));
        Output\Scripts::get()->addVar('upload_dir', Settings::get()->readValue('upload_dir'));
        Output\Scripts::get()->addVar('thumb_size_filemanager', Settings::get()->readValue('thumb_size_filemanager'));
        Output\Scripts::get()->addVar('load_more', '<#load-more#>');
        Output\Scripts::get()->addVar('confirm_image_remove', '<#confirm-image-remove#>');
        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/upload.js'></script>");
        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/page_edit.js'></script>");
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * page remove
     */
    public static function pageRemove()
    {
        self::init();

        Page::get()->remove(Input::get()->readInt('id_page'));
        Output::get()->redirect('/engine-api/admin/pages');
    }


    /**
     * settings cotrol centre
     */
    public static function settings()
    {

        self::init();

        $html = "<table class='table'>
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><#code#></th>
                            <th><#value#></th>
                            <th><#description#></th>
                        </tr>
                    </thead><tbody>";
        foreach(Settings::get()->readAll() as $setting)
        {
            if (!$setting->readIsHidden()) {
                $html .= "<tr>
                            <td class='text-nowrap'>
                                <a href='/engine-api/admin/settings_edit?skey={$setting->readKey()}'><span class='glyphicon glyphicon-edit'></span></a>
                            </td>
                            <td class='text-nowrap'>{$setting->readKey()}</td>
                            <td>";
                switch($setting->readTypeCode())
                {
                    case 'bool':
                        $html .= "<span class='".($setting->readValue() ? "glyphicon glyphicon-ok-circle text-success" : "glyphicon glyphicon-remove-sign text-danger")."'></span>";
                        break;
                    default:
                        $html .= $setting->readValue();
                }
                $html .= "  </td>
                            <td>{$setting->readDescription()}</em></td>
                        </tr>";
            }
        }
        $html .= "</tbody></table>";
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * settings edit
     */
    public static function settingsEdit()
    {
        self::init();

        $skey = Input::get()->read('skey');
        $setting = Settings::get()->readByKey($skey);
        if ($setting->readIsHidden()) Output::get()->error("This setting is hidden. You can not change it.");

        if (Input::get()->readInt('update'))
        {
            if (Input::get()->read('skey'))
            {  // update
                Settings::get()->write(Input::get()->read('skey'), Input::get()->read('value'));
            }
            Output::get()->redirect('/engine-api/admin/settings');
        }

        $html = <<<HTM
            <form id='settings_edit' action='' method='post'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group'>
                    <label><#code#></label>
                    <input class='form-control' name='template_code' value='{$skey}' readonly disabled>
                </div>
HTM;

        switch($setting->readTypeCode())
        {
            case 'int':
            case 'float':
                $html .= "<div class='form-group'>
			        <label><#value#></label>
			        <p class='help-block'>".$setting->readDescription()."</p>".
                    Output\Forms\Input::create()
                        ->setName('value')
                        ->setValue($setting->readValue())
                        ->setClass('form-control')
                        ->render().
                    "</div>";
                break;
            case 'bool':
                $html .= "<div class='checkbox'><label>";
                $html .= Output\Forms\Input::create()
                    ->setType('checkbox')
                    ->setName('value')
                    ->setValue(1)
                    ->setChecked($setting->readValue())
                    ->render();
                $html .= $setting->readDescription()."</label></div>";
                break;
            case 'html':
                $html .= "<div class='form-group'>
			        <label><#value#></label>
			        <p class='help-block'>".$setting->readDescription()."</p>
                    <textarea class='form-control' style='display: none;' id='value' name='value'></textarea>
                    <div id='value_ace'>{$setting->readHtmlValueForEditor()}</div>
                </div>";
                Output\Scripts::get()->addScript("<script src='/admin/settings_edit.js'></script>");
                break;
            case 'string':
            default:
                $html .= "<div class='form-group'>
			        <label><#value#></label>
			        <p class='help-block'>".$setting->readDescription()."</p>
                    <textarea class='form-control' style='height: 400px;' id='value' name='value'>{$setting->readValue()}</textarea>
                </div>";
        }

        $html .= <<<HTM
            <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
        </form>
HTM;
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * styles control centre
     */
    public static function styles()
    {
        self::init();

        $styles = Style::get()->readAll();
        $html = "
        <p><a href='/engine-api/admin/style_edit' class='pull-right btn btn-lg btn-primary'><#new-style#></a></p>
        <table class='table'>
			<thead>
				<tr>
				    <th>&nbsp;</th>
				    <th><#code#></th>
				    <th><#description#></th>
				    <th><#type#></th>
                </tr>
            </thead><tbody>";

        foreach($styles as $style)
        {
            $html .= "<tr>
                <td class='text-nowrap'>
                    <a href='/engine-api/admin/style_edit?id_style={$style->readId()}'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/engine-api/admin/style_remove?id_style={$style->readId()}' onclick=\"return confirm('<#confirm-style-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                </td>
                <td>".$style->readCode()."</td>
                <td>".$style->readDescription()."</td>
                <td>".($style->readLink() ? '<#link#>' : '<#text#>')."</td>
              </tr>";
        }
        $html .= "</tbody></table>";

        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * style edit
     */
    public static function styleEdit()
    {
        self::init();

        if (Input::get()->readInt('update'))
        {
            if (Style::get()->isExists(
                Input::get()->read('style_code'),
                Input::get()->readInt('id_style')
            )) {
                Output::get()->error("<#error-style-code-exists#>");
            }

            Style::get()->write(
                Style\Item::create()
                    ->writeId(Input::get()->readInt('id_style'))
                    ->writeCode(Input::get()->read('style_code'))
                    ->writeLink(Input::get()->read('style_link'))
                    ->writeText(Input::get()->read('style_text'))
                    ->writeDescription(Input::get()->read('style_description'))
            );
            Output::get()->redirect('/engine-api/admin/styles');
        }
        $style = Style::get()->readById(Input::get()->readInt('id_style'));
        $html = <<<HTM
            <form action='' method='post' onsubmit="ace_fetch('style_text', 'style_text_ace');">
                <input type='hidden' name='id_style' value='{$style->readId()}'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group has-feedback'>
                    <label class='control-label'><#code#></label>
                    <input class='form-control' name='style_code' value='{$style->readCode()}' onchange="check_style_code();">
                    <span class="glyphicon form-control-feedback"></span>
                    <div class='help-block'></div>
                </div>
                <div class='form-group'>
                    <label><#description#></label>
                    <input class='form-control' name='style_description' value='{$style->readDescription()}'>
                </div>
                <div class='form-group'>
                    <label><#link-to-css#></label>
                    <p class='help-block'><#link-to-css-help#></p>
                    <input class='form-control' name='style_link' value='{$style->readLink()}'>
                </div>
                <div class='form-group'>
                    <label><#or#> <#style-text#></label>
                    <p class='help-block'><#style-text-help#></p>
                    <textarea style='display: none;' id='style_text' name='style_text'></textarea>
                    <div id='style_text_ace'>{$style->readTextForEditor()}</div>
                </div>
                <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
            </form>
HTM;

        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/style_edit.js'></script>");
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * style remove
     */
    public static function styleRemove()
    {
        self::init();

        Style::get()->remove(Input::get()->readInt('id_style'));
        Output::get()->redirect('/engine-api/admin/styles');
    }


    /**
     * templates control centre
     */
    public static function templates()
    {
        self::init();

        $templates = Template::get()->readAll();
        $html = "
            <p><a href='/engine-api/admin/template_edit' class='pull-right btn btn-lg btn-primary'><#new-template#></a></p>
            <table class='table'>
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><#code#></th>
                        <th><#description#></th>
                    </tr>
                </thead><tbody>";
        foreach($templates as $template)
        {
            $html .= "<tr>
                        <td class='text-nowrap'>
                            <a href='/engine-api/admin/template_edit?id_template={$template->readId()}'><span class='glyphicon glyphicon-edit'></span></a>
                            <a href='/engine-api/admin/template_remove?id_template={$template->readId()}' onclick=\"return confirm('<#confirm-template-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                        </td>
                        <td class='text-nowrap'>{$template->readCode()}</td>
                        <td>{$template->readDescription()}</td>
                    </tr>";
        }
        $html .= "</tbody></table>";
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * template edit
     */
    public static function templateEdit()
    {
        self::init();

        if (Input::get()->readInt('update'))
        {
            if (Template::get()->isExists(
                Input::get()->read('template_code'),
                Input::get()->readInt('id_template')
            )) {
                Output::get()->error("<#error-template-code-exists#>");
            }

            Template::get()->write(
                Template\Item::create()
                    ->writeId(Input::get()->readInt('id_template'))
                    ->writeCode(Input::get()->read('template_code'))
                    ->writeHtml(Input::get()->read('template_html'))
                    ->writeDescription(Input::get()->read('template_description'))
                    ->writeStyles(array_keys(Input::get()->readIntArray('styles')))
            );
            Output::get()->redirect('/engine-api/admin/templates');
        }
        $template = Template::get()->readById(Input::get()->readInt('id_template'));
        $styles = $template->readStyles();
        $all_styles = Style::get()->readAll();
        $all_styles_html = "";
        foreach($all_styles as $style)
        {
            $all_styles_html .= "<li><a href='#'><label>".Output\Forms\Input::create()
                    ->setType('checkbox')
                    ->setName('styles['.$style->readId().']')
                    ->setId('styles_'.$style->readId())
                    ->setValue(1)
                    ->setChecked(in_array($style->readId(), $styles) ? true : false)
                    ->render().
                "&nbsp;".$style->readCode()."&nbsp;&mdash;&nbsp;".$style->readDescription()."
                        </label></a></li>";
        }

        if (!count($styles))
        {
            $styles_selector_title = "<#choose#>";
        } elseif (count($styles)===1) {
            $styles_selector_title = Style::get()->readById($styles[0])->readCode();
        } else {
            $styles_selector_title = "<#several#> (".count($styles).")";
        }
        $html = <<<HTM
            <form action='' method='post' onsubmit="ace_fetch('template_html', 'template_html_ace');">
                <input type='hidden' name='id_template' value='{$template->readId()}'>
                <input type='hidden' name='update' value='1'>
                <div class='form-group has-feedback'>
                    <label class='control-label'><#code#></label>
                    <input class='form-control' name='template_code' value='{$template->readCode()}' onchange="check_template_code();">
                    <span class="glyphicon form-control-feedback"></span>
                    <div class='help-block'></div>
                </div>
                <div class='form-group'>
                    <label><#description#></label>
                    <input class='form-control' name='template_description' value='{$template->readDescription()}'>
                </div>
                <div class='form-group'>
                    <label><#styles#></label><br>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default">{$styles_selector_title}</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            {$all_styles_html}
                        </ul>
                    </div>
                </div>
                <div class='form-group'>
                    <label><#template#></label>
                    <p class='help-block'><a href='#' onclick="toggle_options_list();"><#template-constants-help#></a>
                        <span id='options_list' style='display: none;'><%options%></span>
                    </p>
                    <textarea style='display: none;' id='template_html' name='template_html'></textarea>
                    <div id='template_html_ace'>{$template->readHtmlForEditor()}</div>
                </div>
                <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
            </form>
HTM;
        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/template_edit.js'></script>");
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * template remove
     */
    public static function templateRemove()
    {
        self::init();

        Template::get()->remove(Input::get()->readInt('id_template'));
        Output::get()->redirect('/engine-api/admin/templates');
    }


    /**
     * uploading file handle
     */
    public static function upload()
    {
        self::init();

        /**
         * Copyright 2013, Moxiecode Systems AB
         * Released under GPL License.
         *
         * License: http://www.plupload.com/license
         * Contributing: http://www.plupload.com/contributing
         */

        // Make sure file is not cached (as it happens for example on iOS devices)
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Settings
        $targetDir = Path::resolve( Settings::get()->readValue('upload_dir') );
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // Get a file name
        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }
        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        // Remove old temp files
        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);

            // robin - create thumb
            if (in_array(substr(strtolower($fileName), 1+strrpos($fileName, '.')), array('jpg','jpeg','bmp','gif','png','jpe')))
            {
                if ($data = file_get_contents($filePath))
                {
                    $im = imagecreatefromstring($data);
                    list($w, $h) = getimagesize($filePath);
                    $min = min($w, $h);
                    $im2 = imagecreatetruecolor(Settings::get()->readValue('thumb_size'), Settings::get()->readValue('thumb_size'));
                    imagecopyresampled($im2, $im, 0,0, intval($w/2-$min/2),intval($h/2-$min/2), Settings::get()->readValue('thumb_size'), Settings::get()->readValue('thumb_size'), $min, $min);
                    $fileThumb = $fileName.".thumb.jpg";
                    imagejpeg($im2, $targetDir . DIRECTORY_SEPARATOR . $fileThumb);
                }
            }


            // robin - write to regestry
            Upload::get()->save(
                Upload\Item::create()
                    ->writeSrc($fileName)
                    ->writeThumb($fileThumb)
            );
        }

        // Return Success JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
    }


    /**
     * uploads control centre
     */
    public static function uploads()
    {
        self::init();

        Output\Scripts::get()->addScript('<script type="text/javascript" src="/vendor/moxiecode/plupload/js/plupload.full.min.js"></script>', true);
        Output\Scripts::get()->addVar('upload_dir', Settings::get()->readValue('upload_dir'));
        Output\Scripts::get()->addVar('thumb_size_filemanager', Settings::get()->readValue('thumb_size_filemanager'));
        Output\Scripts::get()->addVar('load_more', '<#load-more#>');
        Output\Scripts::get()->addVar('confirm_image_remove', '<#confirm-image-remove#>');
        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/upload.js'></script>");
        Output\Scripts::get()->addScript("<script src='/vendor/robintail/engine-api/js/admin/page_edit.js'></script>");
        Output\Scripts::get()->addScript("<script>file_manager();</script>");
        Output::get()->writeOption('contains', '')
            ->render();
    }


    /**
     * users control centre
     */
    public static function users()
    {
        self::init();

        $html = "<p><a href='/engine-api/admin/user_edit' class='pull-right btn btn-lg btn-primary'><#new-user#></a></p>
                <table class='table'>
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><#name#></th>
                            <th><#email#></th>
                            <th class='text-center'><#admin#></th>
                        </tr>
                    </thead><tbody>";
        foreach(User::get()->readAll() as $item)
        {
            $html .= "<tr>
                        <td>
                            <a href='/engine-api/admin/user_edit?id_user=".$item->readId()."'><span class='glyphicon glyphicon-edit'></span></a>
                            <a href='/admin/user_remove?id_user=".$item->readId()."' onclick=\"return confirm('<#confirm-user-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                        </td>
                        <td>".$item->readName()."</td>
                        <td>".$item->readEmail()."</td>
                        <td class='text-center'><a onclick='return confirm(\"<#confirm-change-permissions#>\");' href='/engine-api/admin/user_admin?id_user=".$item->readId()."'><span class='glyphicon ".($item->readIsAdmin() ? "glyphicon-ok-sign text-danger" : "glyphicon-remove-sign text-muted")."'></span></a></td>
                    </tr>";
        }
        $html .= "</tbody></table>";
        Output::get()->writeOption('contains', $html)
            ->render();
    }


    /**
     * toggle user admin state
     * @throws \Exception
     */
    public static function userAdmin()
    {
        self::init();

        if ($id = Input::get()->readInt('id_user'))
        {
            DB::get()->prepare("UPDATE users
                                SET is_admin = NOT is_admin
                                WHERE id_user=?")
                ->bind(1, $id)
                ->execute();
        }
        Output::get()->redirect("/engine-api/admin/users");
    }


    /**
     * user edit
     */
    public static function userEdit()
    {
        self::init();

        $item = User::get()->readById(Input::get()->readInt('id_user'));
        if (Input::get()->readInt('update'))
        {
            if (!User::get()->write(
                User\Item::create()
                    ->writeId(Input::get()->readInt('id_user'))
                    ->writeName(Input::get()->read('user_name'))
                    ->writePassword(Input::get()->read('user_password'))
                    ->writeEmail(Input::get()->read('user_email'))
                    ->writeIsAdmin(Input::get()->readBool('is_admin'))
            ))
            {
                Output::get()->error(User::get()->readErrorMessage());
            }
            Output::get()->redirect("/engine-api/admin/users");
        }

        $checkedIsAdmin = $item->readIsAdmin() ? "checked" : "";
        $out = <<<HTM
                <form action='' method='post'>
                    <input type='hidden' name='id_user' value='{$item->readId()}'>
                    <input type='hidden' name='update' value='1'>
                    <div class='form-group has-feedback'>
                        <label class='control-label'><#email#></label>
                        <input class='form-control' name='user_email' value='{$item->readEmail()}' onchange="check_user_email();">
                        <span class="glyphicon form-control-feedback"></span>
                        <div class='help-block'></div>
                    </div>
                    <div class="form-group">
                        <label><#name#></label>
                        <input class="form-control" name="user_name" value="{$item->readName()}">
                    </div>
                    <div class="form-group">
                        <label><#new-password#></label>
                        <input class="form-control" name="user_password" value="">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_admin" value="1" {$checkedIsAdmin}> <#admin-rights#>
                        </label>
                    </div>

                    <button type='submit' class='btn btn-lg btn-primary'><#save#></button>
                </form>
HTM;
        Output::get()->writeOption('contains', $out)
            ->render();
    }


    /**
     * user remove
     */
    public static function userRemove()
    {
        self::init();

        $id = Input::get()->readInt('id_user');
        User::get()->remove($id);
        Output::get()->redirect("/engine-api/admin/users");
    }


} 