<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 30.07.15
 * Time: 11:28
 */

namespace RobinTail\EngineAPI\Upload;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Upload
 */
class Item {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $src;
    /**
     * @var string
     */
    private $thumb;
    /**
     * @var int
     */
    private $userId;
    /**
     * @var int
     */
    private $userAt;

    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function readSrc()
    {
        return $this->src;
    }

    /**
     * @param string $src
     * @return $this
     */
    public function writeSrc($src)
    {
        $this->src = $src;
        return $this;
    }

    /**
     * @return string
     */
    public function readThumb()
    {
        return $this->thumb;
    }

    /**
     * @param string $thumb
     * @return $this
     */
    public function writeThumb($thumb)
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * @return int
     */
    public function readUserAt()
    {
        return $this->userAt;
    }

    /**
     * @param int $userAt
     * @return $this
     */
    public function writeUserAt($userAt)
    {
        $this->userAt = $userAt;
        return $this;
    }

    /**
     * @return int
     */
    public function readUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function writeUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }




} 