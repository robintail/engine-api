<?php

namespace RobinTail\EngineAPI\DB;

interface methods
{
    public function prepare($q);
    public function setOperatingStatement($result);
    public function getOperatingStatement();
    public function bind($param, $value);
    public function execute();
    public function result($result=false);
    public function fetchRow($result=false);
    public function getInsertId();
    public function getLastQuery();
}
