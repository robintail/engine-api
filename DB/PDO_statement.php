<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 21.06.15
 * Time: 23:51
 */

namespace RobinTail\EngineAPI\DB;


use RobinTail\EngineAPI\DB;

class PDO_statement {
    private $result;
    private $binded=array();

    /**
     * @param \PDO $obj
     * @param string $q
     * @throws \Exception
     * @return DB\PDO_statement
     */
    public function __construct($obj, $q)
    {
        try {
            $this->result = $obj->prepare($q);
        } catch (\PDOException $exception)
        {
            throw new \Exception("ERROR IN PREPARE: ".$exception->getMessage());
        }
    }

    /**
     * @param mixed $var
     * @throws \Exception
     * @return int
     */
    public function getPDOParamType( $var )
    {
        if( is_object($var) || is_array($var))
            throw new \Exception("Tried to bind object or array!");
        if( is_int( $var ) )
            return \PDO::PARAM_INT;
        if( is_bool( $var ) )
            return \PDO::PARAM_BOOL;
        if( is_null( $var ) )
            return \PDO::PARAM_NULL;
        if (strlen( $var ) > 8000 )
            return \PDO::PARAM_LOB;
        //Default
        return \PDO::PARAM_STR;
    }


    /**
     * @param int|string $param
     * @param mixed $value
     * @throws \Exception
     * @return $this
     */
    public function bind( $param, $value )
    {
        try {
            $this->result->bindValue($param, $value, $this->getPDOParamType($value));
            $this->binded[$param] = $value;
            DB::get()->setOperatingStatement($this);
            return $this;
        } catch (\PDOException $exception)
        {
            throw new \Exception("ERROR IN BIND: ".$exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     * @return $this
     */
    public function execute()
    {
        try{
            $this->result->execute();
            DB::get()->setOperatingStatement($this);
            return $this;
        } catch(\PDOException $exception)
        {
            throw new \Exception("ERROR IN EXECUTE: ".$this->getLastQuery()."\r\nMESSAGE: ".$exception->getMessage()."\r\nBINDED: ".var_export($this->binded,true));
        }
    }

    /**
     * @return array
     */
    public function fetchRow()
    {
        $data = $this->result->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }


    /**
     * @return string
     */
    public function getLastQuery()
    {
        return $this->result->queryString;
    }


} 