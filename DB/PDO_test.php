<?php

namespace RobinTail\EngineAPI\DB;


class PDO_test implements methods
{
    private $obj;
    private $result;

    public function __construct()
    {
        try {
            $this->obj = new \PDO('sqlite::memory:');
            $this->obj->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $exception) {
            throw new \Exception("CONNECTION ERROR: ".$exception->getMessage());
        }
        return true;
    }

    public function loadFromXML($filename)
    {
        $dump = new \DOMDocument('1.0', 'utf-8');
        if (!$dump->load($filename))
        {
            throw new \Exception("XML load failure.");
        }
        if (!($root = $dump->getElementsByTagName('engine')->item(0)))
        {
            throw new \Exception("Can not find XML root node engine");
        }
        for($tid=0; $tid<$root->childNodes->length; $tid++)
        {
            $table = $root->childNodes->item($tid);
            $tableName = $table->nodeName;
            $tableCreated = false;
            for($rid=0; $rid<$table->childNodes->length; $rid++)
            {
                $row = $table->childNodes->item($rid);
                if (($row->nodeName!='#text') && (!$tableCreated)) {
                    $keyCreated = false;
                    $realColumnCount = 0;
                    $str = "CREATE TABLE " . $tableName . " (";
                    for ($cid = 0; $cid < $row->childNodes->length; $cid++) {
                        $column = $row->childNodes->item($cid);
                        if ($column->nodeName != '#text') {
                            $str .= $column->nodeName . ($keyCreated ? " varchar(255)" : " integer CONSTRAINT rid_".$column->nodeName." PRIMARY KEY AUTOINCREMENT") . ($cid < $row->childNodes->length - 2 ? ", " : "");
                            $keyCreated = true;
                            $realColumnCount++;
                        }
                    }
                    $str .= ")";
                    //var_dump($str);
                    $this->prepare($str)->execute();
                    $tableCreated = true;
                }
                if ($row->nodeName!='#text') {
                    $str = "INSERT INTO " . $tableName . " SELECT ".implode(", ",array_fill(1,$realColumnCount,"?"));
                    $this->prepare($str);
                    $bindId = 0;
                    for ($cid = 0; $cid < $row->childNodes->length; $cid++) {
                        $column = $row->childNodes->item($cid);
                        if ($column->nodeName != '#text') {
                            $bindId++;
                            $this->bind($bindId, $column->nodeValue);
                        }
                    }
                    //var_dump($str);
                    $this->execute();
                }
            }
        }
    }


    /**
     * @param string $q
     * @throws \Exception
     * @return PDO_statement
     */
    public function prepare( $q )
    {
        $this->setOperatingStatement( new PDO_statement($this->obj, $q) );
        return $this->getOperatingStatement();
    }

    /**
     * @param PDO_statement $result
     */
    public function setOperatingStatement( $result )
    {
        $this->result = $result;
    }

    /**
     * @return PDO_statement
     */
    public function getOperatingStatement()
    {
        return $this->result;
    }


    /**
     * @param int|string $param
     * @param mixed $value
     * @return PDO_statement
     * @throws \Exception
     */
    public function bind($param, $value)
    {
        return $this->getOperatingStatement()->bind($param, $value);
    }

    /**
     * @return PDO_statement
     * @throws \Exception
     */
    public function execute()
    {
        return $this->getOperatingStatement()->execute();
    }

    /**
     * @param bool|PDO_statement $result
     * @return array
     * @throws \Exception
     */
    public function result($result=false)
    {
        $result = $result ? $result : $this->getOperatingStatement();
        $arr = array();
        while ($row=$this->fetchRow($result))
            $arr[] = $row;
        return $arr;
    }

    /**
     * @param bool|PDO_statement $result
     * @return mixed
     * @throws \Exception
     */
    public function fetchRow($result=false)
    {
        $result = $result ? $result : $this->getOperatingStatement();
        if ($result===null) throw new \Exception("ERROR IN FETCH: Query was not executed.");
        /*return $result->fetch(\PDO::FETCH_ASSOC);*/
        return $result->fetchRow();
    }

    /**
     * @return int
     */
    public function getInsertId()
    {
        return $this->obj->lastInsertId();
    }

    /**
     * @return string
     */
    public function getLastQuery()
    {
        return $this->getOperatingStatement()->getLastQuery();
    }
}
