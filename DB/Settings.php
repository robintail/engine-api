<?
namespace RobinTail\EngineAPI\DB;

/**
 * Class Settings
 * @package RobinTail\EngineAPI\DB
 */
class Settings
{
    /**
     * Server host (for local use - 127.0.0.1)
     * @var string
     */
    protected static $host = '127.0.0.1';

    /**
     * Server port. Default is 3306
     * @var string
     */
    protected static $port = '3306';

    /**
     * MySQL user name
     * @var string
     */
    protected static $user = 'root';

    /**
     * MySQL user password
     * @var string
     */
    protected static $pass = 'root';

    /**
     * MySQL database name
     * @var string
     */
    protected static $base = 'engine';

    /**
     * false - installer should create the database
     * @var bool
     */
    protected static $baseIsExists = true;




    // methods





    /**
     * @param string $base
     */
    public static function setBase($base)
    {
        self::$base = $base;
    }

    /**
     * @param boolean $baseIsExists
     */
    public static function setBaseIsExists($baseIsExists)
    {
        self::$baseIsExists = $baseIsExists;
    }

    /**
     * @param string $host
     */
    public static function setHost($host)
    {
        self::$host = $host;
    }

    /**
     * @param string $pass
     */
    public static function setPass($pass)
    {
        self::$pass = $pass;
    }

    /**
     * @param string $port
     */
    public static function setPort($port)
    {
        self::$port = $port;
    }

    /**
     * @param string $user
     */
    public static function setUser($user)
    {
        self::$user = $user;
    }



}