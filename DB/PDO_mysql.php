<?php

namespace RobinTail\EngineAPI\DB;


class PDO_mysql extends Settings implements methods
{
    private $obj;
    private $result;

    public function __construct()
    {
        try {
            $this->obj = new \PDO(
                            'mysql:host=' . parent::$host .
                            ';port=' . parent::$port .
                            ';dbname=' . parent::$base,
                            parent::$user, parent::$pass);
            $this->obj->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            //$this->prepare("SET NAMES 'utf8'")->execute();
            $this->obj->query("SET NAMES 'utf8'");
        } catch (\PDOException $exception) {
            throw new \Exception("CONNECTION ERROR: ".$exception->getMessage());
        }
        return true;
    }


    /**
     * @param string $q
     * @throws \Exception
     * @return PDO_statement
     */
    public function prepare( $q )
    {
        $this->setOperatingStatement( new PDO_statement($this->obj, $q) );
        return $this->getOperatingStatement();
    }

    /**
     * @param PDO_statement $result
     */
    public function setOperatingStatement( $result )
    {
        $this->result = $result;
    }

    /**
     * @return PDO_statement
     */
    public function getOperatingStatement()
    {
        return $this->result;
    }


    /**
     * @param int|string $param
     * @param mixed $value
     * @return PDO_statement
     * @throws \Exception
     */
    public function bind($param, $value)
    {
        return $this->getOperatingStatement()->bind($param, $value);
    }

    /**
     * @return PDO_statement
     * @throws \Exception
     */
    public function execute()
    {
        return $this->getOperatingStatement()->execute();
    }

    /**
     * @param bool|PDO_statement $result
     * @return array
     * @throws \Exception
     */
    public function result($result=false)
    {
        $result = $result ? $result : $this->getOperatingStatement();
        $arr = array();
        while ($row=$this->fetchRow($result))
            $arr[] = $row;
        return $arr;
    }

    /**
     * @param bool|PDO_statement $result
     * @return mixed
     * @throws \Exception
     */
    public function fetchRow($result=false)
    {
        $result = $result ? $result : $this->getOperatingStatement();
        if ($result===null) throw new \Exception("ERROR IN FETCH: Query was not executed.");
        /*return $result->fetch(\PDO::FETCH_ASSOC);*/
        return $result->fetchRow();
    }

    /**
     * @return int
     */
    public function getInsertId()
    {
        return $this->obj->lastInsertId();
    }

    /**
     * @return string
     */
    public function getLastQuery()
    {
        return $this->getOperatingStatement()->getLastQuery();
    }
}
