<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 16.07.15
 * Time: 19:42
 */

namespace RobinTail\EngineAPI\Lang;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Lang
 */
class Item {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $flag;
    /**
     * @var bool
     */
    private $isEnabled;
    /**
     * @var bool
     */
    private $isProtected;
    /**
     * @var bool
     */
    private $isDomestic;
    /**
     * @var bool
     */
    private $isInternational;
    /**
     * @var bool
     */
    private $isAdminCPAvailable;



    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return string
     */
    public function readCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function writeCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function readFlag()
    {
        return $this->flag;
    }

    /**
     * @param string $flag
     * @return $this
     */
    public function writeFlag($flag)
    {
        $this->flag = $flag;
        return $this;
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return string
     */
    public function readName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function writeName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param boolean $isEnabled
     * @return $this
     */
    public function writeIsEnabled($isEnabled)
    {
        $this->isEnabled = (bool) $isEnabled;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsProtected()
    {
        return $this->isProtected;
    }

    /**
     * @param boolean $isProtected
     * @return $this
     */
    public function writeIsProtected($isProtected)
    {
        $this->isProtected = (bool) $isProtected;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsDomestic()
    {
        return $this->isDomestic;
    }

    /**
     * @param boolean $isDomestic
     * @return $this
     */
    public function writeIsDomestic($isDomestic)
    {
        $this->isDomestic = (bool) $isDomestic;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsInternational()
    {
        return $this->isInternational;
    }

    /**
     * @param boolean $isInternational
     * @return $this
     */
    public function writeIsInternational($isInternational)
    {
        $this->isInternational = (bool) $isInternational;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsAdminCPAvailable()
    {
        return $this->isAdminCPAvailable;
    }

    /**
     * @param boolean $isAdminCPAvailable
     * @return $this
     */
    public function writeIsAdminCPAvailable($isAdminCPAvailable)
    {
        $this->isAdminCPAvailable = (bool) $isAdminCPAvailable;
        return $this;
    }



}