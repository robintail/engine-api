<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 16.07.15
 * Time: 18:25
 */

namespace RobinTail\EngineAPI\Lang\Constant;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Lang\Constant
 */
class Item {
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $code;
    /**
     * @var array()
     */
    private $trans=array();

    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return string
     */
    public function readCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function writeCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @param string $langCode
     * @return string
     */
    public function readTrans($langCode)
    {
        return $this->trans[$langCode];
    }

    /**
     * @param string $langCode
     * @param string $transValue
     * @return $this
     */
    public function writeTrans($langCode, $transValue)
    {
        $this->trans[$langCode] = $transValue;
        return $this;
    }


} 