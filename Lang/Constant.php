<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 16.07.15
 * Time: 17:55
 */

namespace RobinTail\EngineAPI\Lang;

use RobinTail\EngineAPI\DB;
use RobinTail\EngineAPI\Lang;

/**
 * Class Constant
 * @package RobinTail\EngineAPI\Lang
 */
class Constant {
    /**
     * @var Constant
     */
    private static $instance;
    /**
     * @var Constant\Item[]
     */
    private $constants=array();

    /**
     * @return Constant
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * caches language constants from DB to memory
     * @throws \Exception
     */
    private function __construct()
    {
        // caching constants
        DB::get()->prepare("SELECT id_const, const_code
                                    FROM language_const
                                    ORDER BY const_code")
                    ->execute();
        while($row = DB::get()->fetchRow())
        {
            $this->constants[$row['const_code']] = Constant\Item::create()
                                                    ->writeId($row['id_const'])
                                                    ->writeCode($row['const_code']);
        }
        // caching translations
        DB::get()->prepare("SELECT const_code, lang_code, trans_value
                                    FROM language_trans
                                    INNER JOIN language_const ON language_trans.const_id=language_const.id_const
                                    INNER JOIN languages ON language_trans.lang_id=languages.id_lang")
                    ->execute();
        while($row = DB::get()->fetchRow())
        {
            $this->constants[$row['const_code']]->writeTrans($row['lang_code'], $row['trans_value']);
        }
    }


    /**
     * Returns a language constant object by ID
     * Returns empty new object if not found
     * @param $id
     * @return Constant\Item
     */
    public function readById($id)
    {
        foreach($this->constants as $item)
        {
            if ($item->readId() == $id) return $item;
        }
        return Constant\Item::create();
    }

    /**
     * Return a language constant object by code
     * Returns empty new object if not found
     * @param $code
     * @return Constant\Item
     */
    public function readByCode($code)
    {
        return isset($this->constants[$code]) ? $this->constants[$code] : Constant\Item::create();
    }


    /**
     * @return Constant\Item[]
     */
    public function readAll()
    {
        return $this->constants;
    }

    /**
     * Test is the language constant exists
     * @param string $code
     * @param int $idToExclude
     * @return bool
     */
    public function isExists($code, $idToExclude)
    {
        if (isset($this->constants[$code]))
        {
            if ($this->constants[$code]->readId() == $idToExclude)
            {
                return false; // exists, but excluded
            } else {
                return true; // exists
            }
        }
        return false; // not exists
    }


    /**
     * Perform an insert/update action to DB
     * @param int $id
     * @param string $code
     * @param array $translations
     * @throws \Exception
     */
    public function write($id, $code, $translations)
    {
        foreach($translations as $k => $v)
        {
            if (!Lang::get()->isExists($k, 0)) unset($translations[$k]);
        }
        if ($id)
        {
            // update code
            DB::get()->prepare("UPDATE language_const
                                        SET const_code=:const_code
                                        WHERE id_const=:id_const")
                            ->bind(':const_code', $code)
                            ->bind(':id_const', $id)
                            ->execute();
        } else {
            // insert code
            DB::get()->prepare("INSERT INTO language_const (const_code)
                                        SELECT :const_code")
                        ->bind(':const_code', $code)
                        ->execute();
            $id = DB::get()->getInsertId();
        }
        foreach($translations as $langCode => $transValue) {
            // remove translations
            DB::get()->prepare("DELETE FROM language_trans
                                    WHERE const_id=:const_id AND lang_id=:lang_id")
                ->bind(':const_id', $id)
                ->bind(':lang_id', Lang::get()->readByCode($langCode)->readId())
                ->execute();
            // insert new translations
            DB::get()->prepare("INSERT INTO language_trans (lang_id, const_id, trans_value)
                                        SELECT id_lang, :id_const, :trans_value
                                        FROM languages
                                        WHERE lang_code=:lang_code")
                        ->bind(':id_const', $id)
                        ->bind(':trans_value', $transValue)
                        ->bind(':lang_code', $langCode)
                        ->execute();
        }
    }


    /**
     * Removes language constant from DB
     * @param int $id
     * @throws \Exception
     */
    public function remove($id)
    {
        DB::get()->prepare("DELETE FROM language_const
                                    WHERE id_const=?")
                    ->bind(1, $id)
                    ->execute();
    }


} 