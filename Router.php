<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 23.08.15
 * Time: 03:37
 */

namespace RobinTail\EngineAPI;


class Router {
    /**
     * @var Router
     */
    private static $instance;
    /**
     * @var array
     */
    private $routes = array();


    /**
     * @return Router
     */
    public static function get()
    {
        if (!self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Adds a route to the router.
     *
     * @param string $path
     * @param callable $handler
     * @example
     *    / (1)
     *    /page/subpage (1)
     *    * (1)
     *    /category/:categoryName (2)
     *    /category/:categoryName/:productName (2)
     *    /vendor/*.js (3)
     */
    public function addRoute($path, $handler) {
        $path = trim($path, '/');
        $pathFragments = explode('/', $path);
        $patternFragments = array_map(
            function($bit) {
                if ($bit === '*') {
                    return '(.*)';
                } else if ($bit[0] === ':') {
                    return '([^\\/]+)';
                } else {
                    return implode('(.*)', array_map(array(Regex::class, 'escapeRegex'), explode('*', $bit)));
                }
            },
            $pathFragments
        );
        $pattern = '/^'.implode('\\/', $patternFragments).'$/';

        $this->routes[] = array(
            'pattern' => $pattern,
            'handler' => $handler
        );
    }

    /**
     * @return string
     */
    public function getRequestURI()
    {
        $req = strstr($_SERVER['REQUEST_URI'], $_SERVER['PHP_SELF']) ? strstr($_SERVER['REQUEST_URI'], $_SERVER['PHP_SELF'], true) : $_SERVER['REQUEST_URI']; // exclude this script
        $req = substr($req, 1); // exclude first slash
        $req = strstr($req, '?') ? strstr($req, '?', true) : $req; // exclude args
        return $req;
    }

    /**
     * @throws \Exception
     * @return bool
     */
    public function execute() {
        $result = false;
        $path = $this->getRequestURI();
        $path = trim($path, '/');
        foreach ($this->routes as $route) {
            if (preg_match($route['pattern'], $path, $match)) {
                try {
                    $args = array_slice($match, 1);
                    $result = call_user_func_array($route['handler'], $args);
                } catch (\Exception $e) {
                    throw $e;
                }
                if ($result) return true;
            }
        }
        return false;
    }

}