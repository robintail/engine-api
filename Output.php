<?

namespace RobinTail\EngineAPI;
use RobinTail\EngineAPI\Output\Snippets;
use RobinTail\EngineAPI\Output\Scripts;
use RobinTail\EngineAPI\Output\Styles;

/**
 * Class Output
 * @package RobinTail\EngineAPI
 */
class Output
{
    /**
     * Engine.API Version
     * Since Engine becomes the API composer package, we store the version number in repository tag
     * This constant is now holds a template constant, that triggers the handle method, installed by
     * render() for backward compatibility reasons. Works only for template parsing.
     * Use Git->getPackageVersion() in other cases.
     * (Engine.API based on Engine 2.3.0)
     */
    const VERSION = "<%version%>";
    /**
     * @var Output
     */
    private static $instance;
    /**
     * Output buffer
     * @var string
     */
    private $html = "";
    /**
     * Parameters passing into template
     * @var array
     */
    private $options = array();
    /**
     * Id or Code of template
     * @var int|string
     */
    private $templateIdOrCode;


    /**
     * @return Output
     */
    public static function get()
    {
        if (!self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Clears all options 
     */
    public function flush()
    {
        $this->html = "";
        $this->options = array();
        $this->templateIdOrCode = 0;
    }

    /**
     * @param string $key
     * @param string|callable $value
     * @return $this
     */
    public function writeOption($key, $value)
    {
        $this->options[$key] = $value;
        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function readOption($key)
    {
        return $this->options[$key];
    }

    /**
     * @param int|string $idOrCode
     * @return $this
     */
    public function writeTemplate($idOrCode)
    {
        $this->templateIdOrCode = $idOrCode;
        return $this;
    }

    /**
     * @return int|string
     */
    public function readTemplate()
    {
        return $this->templateIdOrCode;
    }

    /**
     * Builds and outputs html by template
     */
    public function render()
	{
		$this->writeOption('title_short', ($this->readOption('title_short') ? $this->readOption('title_short') . " - " : "") . "<#brand#>")
             ->writeOption('description', str_replace(array(chr(10),chr(13),"\""),array(" ", " ", "&quot;"), $this->readOption('description')))
		     ->writeOption('site_image', function () { return Settings::get()->readValue('site_image'); })
		     ->writeOption('redirect_link', $this->readOption('redirect') ? "<meta http-equiv=\"refresh\" content=\"0; url=".$this->readOption('redirect')."\" />" : "")
             ->writeOption('user_link', User\Current::get()->readId() ? Array(User\Current::get(), "readName") : "<#login#>" )
		     ->writeOption('main_menu', Array(Snippets::class, "mainMenu") )
		     ->writeOption('contains', "\r\n\r\n".$this->readOption('contains')."\r\n\r\n" . Snippets::usageReport() . Snippets::modals())
             ->writeOption('lang_selector', Array(Snippets::class, "langSelector") )
             ->writeOption('lang_selector_button', Array(Snippets::class, "langSelectorButton") )
             ->writeOption('lang_selector_admin', Array(Snippets::class, "langSelectorForAdminCP") )
             ->writeOption('lang_selector_button_admin', Array(Snippets::class, "langSelectorButtonForAdminCP") )
             ->writeOption('version', Array(new Git(), "getPackageVersion") )
             ->writeOption('admin_menu', Array(Snippets::class, "adminMenu") )
             ->writeOption('admin_menu_list', Array(Snippets::class, "adminMenuList") )
             ->writeOption('search', Array(Snippets::class, "search") )
             ->writeOption('search_standalone', Array(Snippets::class, "searchStandalone") )
             ->writeOption('user_menu', Array(Snippets::class, "userMenu") );

		$this->html = $this->parseTemplate();
		$this->stdout();
	}

    /**
     * Builds and outputs failure html
     * @param $contains
     */
    public function fatal($contains)
	{
		$this->html  = "<!DOCTYPE html><html><head><link href='/vendor/twbs/bootstrap/dist/css/bootstrap.min.css' rel='stylesheet'>";
        $this->html .= Scripts::get()->render();
        $this->html .= "</head><body><div class='container'>";
        $this->html .= $contains;
        $this->html .= "</div></body></html>";
		$this->stdout();
	}

    /**
     * Builds and outputs html with error message
     * @param $msg
     */
    public function error($msg)
	{
        $this->writeOption('title_short', '<#error#>')
             ->writeOption('contains', $msg)
             ->render();
	}

    /**
     * Builds and outputs html with redirect
     * @param $url
     */
    public function redirect($url)
	{
		$this->writeOption('redirect', $url)
		     ->writeOption('title_short', '<#redirect#>')
		     ->writeOption('contains', "<center><#redirect#><br><br><a href='".$this->readOption('redirect')."'><#redirect-help#></a></center>")
             ->render();
	}


    /**
     * Returns parsed html code of template
     * @return string
     */
    public function parseTemplate()
	{
        if (!$this->templateIdOrCode) $this->templateIdOrCode = Template::get()->getDefaultCode();
        if (is_numeric($this->templateIdOrCode))
        {
            $template = Template::get()->readById(intval($this->templateIdOrCode));
        } else {
            $template = Template::get()->readByCode($this->templateIdOrCode);
        }
        $html = $template->readHtml();

        foreach($template->readStyles() as $style_id)
        {
            $style = Style::get()->readById($style_id);
            if ($style->readLink())
            {
                Styles::get()->addStyleLink($style->readLink());
            } else {
                Styles::get()->addStyleText($style->readText());
            }
        }
        $this->writeOption('scripts', function () { return Scripts::get()->render(); })
             ->writeOption('styles', function () { return Styles::get()->render(); });
        // add list of options as option
        $this->writeOption('options', function() {
            $list_of_options = array_keys($this->options);
            sort($list_of_options);
            array_walk($list_of_options, function(&$item) {
                $item = "&lt;%".$item."%&gt;";
            } );
            return implode(", ",  $list_of_options);
        });
        $html = preg_replace_callback(
                    "/<%([a-z\-0-9_]+)%>/",
                    array($this, 'replaceOptionInTemplate'),
                    $html);
        $html = preg_replace_callback(
                    "/<%(options)%>/",
                    array($this, 'replaceOptionInTemplate'),
                    $html);
		return $html;		
	}

    /**
     * private callback function for ::parseTemplate()
     * @param array $matches
     * @return mixed
     */
    private function replaceOptionInTemplate($matches)
    {
        $result = $this->readOption($matches[1]);
        if (is_callable($result))
        {
            return call_user_func($result);
        } else {
            return $result;
        }
    }


    /**
     * Returns parsed html code with translated constants
     * Preserve constants in textareas
     * @param string $html
     * @param bool $override_lang_code
     * @return mixed
     */
    public function parseTranslate($html, $override_lang_code=false)
	{
		/*
		preg_match_all("/<#([a-z\-0-9]+)#>/", $html, $matches);
		foreach($matches[1] as $match)
		{
			$html = str_replace("<#".$match."#>", lang::translate($match), $html);
		}*/
		$textareas = array(); // save textareas as is
        $html = preg_replace_callback("/(<textarea.*>)(.*)(<\/textarea>)/siuU", function ($matches) use (&$textareas) {$textareas[] = $matches[2]; return $matches[1]."<%textarea_".(count($textareas)-1)."%>".$matches[3];}, $html);
        $html = preg_replace_callback("/<#([a-z\-0-9_]+)#>/", function($matches) use ($override_lang_code) {return Lang::get()->translate($matches[1], $override_lang_code);}, $html);
        $html = preg_replace_callback("/<%textarea_(\d+)%>/", function($matches) use ($textareas) {return $textareas[intval($matches[1])];}, $html);
		return $html;
	}

    /**
     * Do the output
     */
    private function stdout()
	{
        $this->html = $this->parseTranslate($this->html);
		echo $this->html;
        // update user language before exit
        User\Current::get()->updateLangCode(Lang::get()->getUsing());
		exit();
	}

}

