<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 01.08.15
 * Time: 19:28
 */

namespace RobinTail\EngineAPI\Menu;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Menu
 */
class Item {

    /**
     * @var int
     */
    private $id;
    /**
     * @var int|null
     */
    private $parent;
    /**
     * @var int|null
     */
    private $pageId;
    /**
     * @var string|null
     */
    private $title;
    /**
     * @var int
     */
    private $ordering;
    /**
     * @var bool
     */
    private $isDropdownRight;
    /**
     * @var bool
     */
    private $isActive;

    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int
     */
    public function readOrdering()
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     * @return $this
     */
    public function writeOrdering($ordering)
    {
        $this->ordering = (int) $ordering;
        return $this;
    }

    /**
     * @return int|null
     */
    public function readPageId()
    {
        return $this->pageId;
    }

    /**
     * @param int|null $pageId
     * @return $this
     */
    public function writePageId($pageId)
    {
        $this->pageId = $pageId ? (int) $pageId : null;
        return $this;
    }

    /**
     * @return int|null
     */
    public function readParent()
    {
        return $this->parent;
    }

    /**
     * @param int|null $parent
     * @return $this
     */
    public function writeParent($parent)
    {
        $this->parent = $parent ? (int) $parent : null;
        return $this;
    }

    /**
     * @return null|string
     */
    public function readTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return $this
     */
    public function writeTitle($title)
    {
        $this->title = $title ? $title : null;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsDropdownRight()
    {
        return $this->isDropdownRight;
    }

    /**
     * @param boolean $isDropdownRight
     * @return $this
     */
    public function writeIsDropdownRight($isDropdownRight)
    {
        $this->isDropdownRight = (bool) $isDropdownRight;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     * @return $this
     */
    public function writeIsActive($isActive)
    {
        $this->isActive = (bool) $isActive;
        return $this;
    }



} 