<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 19.11.15
 * Time: 19:35
 */

namespace RobinTail\EngineAPI\Ajax;

use RobinTail\EngineAPI\Ajax;
use RobinTail\EngineAPI\Input;
use RobinTail\EngineAPI\Lang;
use RobinTail\EngineAPI\Menu;
use RobinTail\EngineAPI\Output;
use RobinTail\EngineAPI\Page;
use RobinTail\EngineAPI\Style;
use RobinTail\EngineAPI\Template;
use RobinTail\EngineAPI\Upload;

/**
 * Class Admin
 * @package RobinTail\EngineAPI\Ajax
 */
class Admin {


    /**
     * check language code
     */
    public static function languageCode()
    {
        Ajax::checkIsAdmin();

        $code = Input::get()->read('lang_code');
        $id = Input::get()->readInt('id_lang');
        if (!$code) Ajax::error(Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));
        if (Lang::get()->isExists($code, $id)) {
            Ajax::error(Output::get()->parseTranslate("<#error-lang-code-exists#>"));
        } else {
            Ajax::message("OK");
        }
    }


    /**
     * check language constant code
     */
    public static function languageConstCode()
    {
        Ajax::checkIsAdmin();

        $code = Input::get()->read('const_code');
        $id = Input::get()->readInt('id_const');
        if (!$code) Ajax::error(Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));
        if (Lang\Constant::get()->isExists($code, $id)) {
            Ajax::error(Output::get()->parseTranslate("<#error-lang-const-code-exists#>"));
        } else {
            Ajax::message("OK");
        }
    }


    /**
     * toggle menu item to dropdown right (property)
     */
    public static function menuDropdownRight()
    {
        Ajax::checkIsAdmin();

        if (!Input::get()->readInt('id_link')) Ajax::error(Output::get()->parseTranslate("id_link - <#error-smth-not-specified#>"));
        if (!Input::get()->exist('is_dropdown_right')) Ajax::error(Output::get()->parseTranslate("is_dropdown_right - <#error-smth-not-specified#>"));
        Menu::get()->updateItemDropdownRight(Input::get()->readInt('id_link'), Input::get()->readBool('is_dropdown_right'));
        Ajax::message('OK');
    }


    /**
     * change menu item ordering
     */
    public static function menuOrdering()
    {
        Ajax::checkIsAdmin();

        if (!Input::get()->readInt('id_link')) Ajax::error(Output::get()->parseTranslate("id_link - <#error-smth-not-specified#>"));
        if (!Input::get()->exist('link_ordering')) Ajax::error(Output::get()->parseTranslate("link_ordering - <#error-smth-not-specified#>"));
        Menu::get()->updateItemOrdering(Input::get()->readInt('id_link'), Input::get()->readInt('link_ordering'));
        Ajax::message('OK');
    }


    /**
     * change menu item title
     */
    public static function menuTitle()
    {
        Ajax::checkIsAdmin();

        if (!Input::get()->readInt('id_link')) Ajax::error(Output::get()->parseTranslate("id_link - <#error-smth-not-specified#>"));
        if (!Input::get()->read('link_title_short')) Ajax::error(Output::get()->parseTranslate("link_title_short - <#error-smth-not-specified#>"));
        Menu::get()->updateTextItemTitle(Input::get()->readInt('id_link'), Input::get()->read('link_title_short'));
        Ajax::message('OK');
    }


    /**
     * check page url
     */
    public static function pageUrl()
    {
        Ajax::checkIsAdmin();

        $url = Input::get()->read('page_url');
        $id = Input::get()->readInt('id_page');
        if (!$url) Ajax::error(Output::get()->parseTranslate('<#error-url-not-specified#>'));
        // not required due to new router with one entry point
        //if (file_exists(Path::resolve($url))) Ajax::error(Output::get()->parseTranslate('<#error-file-or-directory-exists#>'));
        if (Page::get()->isExists($url, $id))
        {
            Ajax::error(Output::get()->parseTranslate('<#error-page-url-exists#>'));
        } else {
            Ajax::message("OK");
        }
    }


    /**
     * check style code
     */
    public static function styleCode()
    {
        Ajax::checkIsAdmin();

        $code = Input::get()->read('style_code');
        $id = Input::get()->readInt('id_style');
        if (!$code) Ajax::error(Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));
        if (Style::get()->isExists($code, $id)) {
            Ajax::error(Output::get()->parseTranslate("<#error-style-code-exists#>"));
        } else {
            Ajax::message("OK");
        }
    }


    /**
     * check template code
     */
    public static function templateCode()
    {
        Ajax::checkIsAdmin();

        $code = Input::get()->read('template_code');
        $id = Input::get()->readInt('id_template');
        if (!$code) Ajax::error(Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));
        if (Template::get()->isExists($code, $id)) {
            Ajax::error(Output::get()->parseTranslate("<#error-template-code-exists#>"));
        } else {
            Ajax::message("OK");
        }
    }


    /**
     * remove the upload item
     */
    public static function uploadRemove()
    {
        Ajax::checkIsAdmin();

        Upload::get()->remove(Input::get()->readInt('id_upload'));
        Ajax::message("OK");
    }


    /**
     * returns upload items
     */
    public static function uploads()
    {
        Ajax::checkIsAdmin();

        $pre = Upload::get()->readSome(
            Input::get()->read('period'),
            Input::get()->readInt('from')
        );
        $result = array();
        foreach($pre as $item)
        {
            $result[] = array(
                'id' => $item->readId(),
                'src' => $item->readSrc(),
                'thumb' => $item->readThumb()
            );
        }
        Ajax::json($result);
    }


    /**
     * returns groups of upload items (periods)
     */
    public static function uploadsPeriods()
    {
        Ajax::checkIsAdmin();

        $array = Upload::get()->readPeriods();
        Ajax::json( $array );
    }
    
} 