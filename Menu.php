<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 01.08.15
 * Time: 19:19
 */

namespace RobinTail\EngineAPI;


/**
 * Class Menu
 * @package RobinTail\EngineAPI
 */
class Menu {

    /**
     * @var Menu
     */
    private static $instance;

    /**
     * @var Menu\Item[]
     */
    private $menu=array();

    /**
     * @return Menu
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Cache menu
     * @throws \Exception
     */
    private function __construct()
    {
        DB::get()->prepare("SELECT menu.*
			               FROM menu
			               ORDER BY link_ordering")
            ->execute();
        while ($row = DB::get()->fetchRow())
        {
            $this->menu[$row['id_link']] = $this->createMenuItemFromArray($row);
        }
    }

    /**
     * @param $row
     * @return Menu\Item
     */
    private function createMenuItemFromArray($row)
    {
        return Menu\Item::create()
                ->writeId($row['id_link'])
                ->writeParent($row['parent_link_id'])
                ->writePageId($row['link_page_id'])
                ->writeTitle($row['link_title_short'])
                ->writeOrdering($row['link_ordering'])
                ->writeIsDropdownRight($row['is_dropdown_right']);
    }

    /**
     * @param int|null $parentId
     * @return Menu\Item[]
     */
    public function readAllByParentId($parentId)
    {
        $result = array();
        foreach($this->menu as $item)
        {
            if ($item->readParent() == $parentId) $result[$item->readId()] = $item;
        }
        return $result;
    }

    /**
     * @param int $id
     * @return Menu\Item
     */
    public function readById($id)
    {
        return isset($this->menu[$id]) ? $this->menu[$id] : Menu\Item::create();
    }


    /**
     * @param string $url
     * @return Menu\Item
     */
    public function readByPageUrl($url)
    {
        foreach($this->menu as $item)
        {
            if ($item->readPageId())
                if (Page::get()->readById($item->readPageId())->readUrl() == $url) return $item;
        }
        return Menu\Item::create();
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function remove($id)
    {
        DB::get()->prepare("UPDATE menu
                          SET parent_link_id=:parent_link_id
                          WHERE parent_link_id=:id_link")
            ->bind(':parent_link_id', $this->readById($id)->readParent() ? $this->readById($id)->readParent() : null)
            ->bind(':id_link', $id)
            ->execute();
        DB::get()->prepare("DELETE FROM menu
                          WHERE id_link=?")
            ->bind(1, $id)
            ->execute();
        return true;
    }

    /**
     * @param int|null $parentId
     * @param int $pageId
     * @return int
     * @throws \Exception
     */
    public function createPageItem($parentId, $pageId)
    {
        DB::get()->prepare("INSERT INTO menu (parent_link_id, link_page_id)
                                  SELECT :parent_link_id, :link_page_id")
            ->bind(':parent_link_id', $parentId ? $parentId : null)
            ->bind(':link_page_id', $pageId)
            ->execute();
        return DB::get()->getInsertId();
    }

    /**
     * @param int|null $parentId
     * @param $title
     * @return int
     * @throws \Exception
     */
    public function createTextItem($parentId, $title)
    {
        DB::get()->prepare("INSERT INTO menu (parent_link_id, link_title_short)
                                  SELECT :parent_link_id, :link_title_short")
            ->bind(':parent_link_id', $parentId ? $parentId : null)
            ->bind(':link_title_short', $title)
            ->execute();
        return DB::get()->getInsertId();
    }

    /**
     * @param int $id
     * @param string $title
     * @return bool
     */
    public function updateTextItemTitle($id, $title)
    {
        DB::get()->prepare("UPDATE menu
                           SET link_title_short=:link_title_short
                           WHERE id_link=:id_link")
            ->bind(':link_title_short', $title)
            ->bind(':id_link', $id)
            ->execute();
        return true;
    }

    /**
     * @param int $id
     * @param int $ordering
     * @return bool
     * @throws \Exception
     */
    public function updateItemOrdering($id, $ordering)
    {
        DB::get()->prepare("UPDATE menu
			              SET link_ordering=:link_ordering
			              WHERE id_link=:id_link")
            ->bind(':link_ordering', $ordering)
            ->bind('id_link', $id)
            ->execute();
        return true;
    }


    public function updateItemDropdownRight($id, $isDropdownRight)
    {
        DB::get()->prepare("UPDATE menu
			              SET is_dropdown_right=:is_dropdown_right
			              WHERE id_link=:id_link")
            ->bind(':is_dropdown_right', $isDropdownRight)
            ->bind('id_link', $id)
            ->execute();

    }

} 