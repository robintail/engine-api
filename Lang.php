<?

namespace RobinTail\EngineAPI;

use RobinTail\EngineAPI\Lang\Constant;

/**
 * Class Lang
 * @package RobinTail\EngineAPI
 */
class Lang
{
    /**
     * @var Lang
     */
    private static $instance;
    /**
     * Language code, using on website
     * @var bool|string
     */
    private $using = false;
    /**
     * Language code, using in admin cp
     * @var bool|string
     */
    private $usingInAdminCP = false;
    /**
     * @var Lang\Item[]
     */
    private $langs = array();

    /**
     * @return Lang
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @throws \Exception
     */
    private function __construct()
    {
        DB::get()->prepare("SELECT * FROM languages")
                ->execute();
        while($row = DB::get()->fetchRow()) {
            $this->langs[$row['lang_code']] = Lang\Item::create()
                                            ->writeId($row['id_lang'])
                                            ->writeCode($row['lang_code'])
                                            ->writeName($row['lang_name'])
                                            ->writeFlag($row['lang_flag'])
                                            ->writeIsEnabled($row['is_enabled'])
                                            ->writeIsProtected($row['is_protected'])
                                            ->writeIsDomestic($row['is_domestic'])
                                            ->writeIsInternational($row['is_international'])
                                            ->writeIsAdminCPAvailable($row['is_admincp_avail']);
        }
    }

    /**
     * @return Lang\Item[]
     */
    public function readAll()
    {
        return $this->langs;
    }


    /**
     * @param string $code
     * @return Lang\Item
     */
    public function readByCode($code)
    {
        return isset($this->langs[$code]) ? $this->langs[$code] : Lang\Item::create();
    }

    /**
     * @param int $id
     * @return Lang\Item
     */
    public function readById($id)
    {
        foreach($this->langs as $item)
        {
            if ($item->readId() == $id) return $item;
        }
        return Lang\Item::create();
    }

    /**
     * @param string $code
     * @param int $id_to_exclude
     * @return bool
     */
    public function isExists($code, $id_to_exclude)
    {
        if (isset($this->langs[$code]))
        {
            if ($this->langs[$code]->readId() == $id_to_exclude)
            {
                return false; // exists, but excluded
            } else {
                return true; // exists
            }
        }
        return false; // not exists
    }


    /**
     * @param int $id
     * @param string $code
     * @param string $name
     * @param string $flag
     * @throws \Exception
     */
    public function write($id, $code, $name, $flag)
    {
        if ($id)
        { // update
            DB::get()->prepare("UPDATE languages
                                SET lang_code=:code,
                                lang_name=:name,
                                lang_flag=:flag
                                WHERE id_lang=:id")
                    ->bind(':code', $code)
                    ->bind(':name', $name)
                    ->bind(':flag', $flag)
                    ->bind(':id', $id)
                    ->execute();
        } else { //insert
            DB::get()->prepare("INSERT INTO languages (lang_code, lang_name, lang_flag)
                                SELECT :code, :name, :flag")
                    ->bind(':code', $code)
                    ->bind(':name', $name)
                    ->bind(':flag', $flag)
                    ->execute();
        }
    }

    /**
     * @param int $id
     * @param string $err
     * @throws \Exception
     * @return bool
     */
    public function toggleEnabled($id, &$err="")
    {
        $lang = $this->readById($id);
        if ($lang->readIsEnabled()) {
            if ($lang->readIsDomestic()) {
                $err = "Can not disable language ".$lang->readCode()." because it is set as default domestic language. See settings.";
                return false;
            }
            if ($lang->readIsInternational()) {
                $err = "Can not disable language ".$lang->readCode()." because it is set as default international language. See settings.";
                return false;
            }
        }
        DB::get()->prepare("UPDATE languages
                            SET is_enabled = NOT is_enabled
                            WHERE id_lang=:id_lang")
                ->bind(':id_lang', $id)
                ->execute();
        return true;
    }

    /**
     * @param int $id
     * @param string $err
     * @return bool
     * @throws \Exception
     */
    public function remove($id, &$err="")
    {
        $lang = $this->readById($id);
        if ($lang->readIsProtected()) {
            $err = "Can not remove language ".$lang->readCode()." because it is protected.";
            return false;
        }
        if ($lang->readIsDomestic()) {
            $err = "Can not remove language ".$lang->readCode()." because it is set as default domestic language. See settings.";
            return false;
        }
        if ($lang->readIsInternational()) {
            $err = "Can not remove language ".$lang->readCode()." because it is set as default international language. See settings.";
            return false;
        }
        DB::get()->prepare("DELETE FROM languages
                            WHERE id_lang=?")
                ->bind(1, $id)
                ->execute();
        return true;
    }
    

    /**
     * Public method, returns using language on website
     * @return string
     */
    public function getUsing()
    {    
        if (!$this->using) $this->setUsing();
		return $this->using;
	}

    /**
     * Public method, return using language in admin cp
     * @return string
     */
    public function getUsingInAdminCP()
    {
        if (!$this->usingInAdminCP) $this->setUsingInAdminCP();
        return $this->usingInAdminCP;
    }

    /**
     * Internal method, that perform caching of identified user language
     */
    private function setUsing()
	{
        if (!$this->using) $this->using = $this->getFinalLang();
	}

    /**
     * Internal method, that perform caching of identified admin cp language
     */
    private function setUsingInAdminCP()
    {
        if (!$this->usingInAdminCP) $this->usingInAdminCP = $this->getFinalLangForAdminCP();
    }

    /**
     * Public method, that verify is the using language equals to default_international
     * @return bool
     */
    public function isInternational()
	{
		// returns is the using language is international default
        return $this->readByCode($this->getUsing())->readIsInternational();
	}

    /**
     * Returns a default domestic language code
     * @return string
     */
    private function getDomesticDefault()
    {
        foreach($this->langs as $item)
        {
            if ($item->readIsDomestic()) return $item->readCode();
        }
        return 'ru'; // fallback
    }

    /**
     * Sets a default domestic language by id
     * @param int $id
     * @param string $err
     * @return bool
     * @throws \Exception
     */
    public function setDomesticDefault($id, &$err="")
    {
        $lang = $this->readById($id);
        if (!$lang->readIsEnabled())
        {
            $err = "Can not set language ".$lang->readCode()." as domestic because it is disabled";
            return false;
        }
        DB::get()->prepare("update languages
                            set is_domestic = case id_lang when ? then 1 else 0 end")
                ->bind(1, $id)
                ->execute();
        return true;
    }

    /**
     * Sets a default international language by id
     * @param int $id
     * @param string $err
     * @return bool
     * @throws \Exception
     */
    public function setInternationalDefault($id, &$err="")
    {
        $lang = $this->readById($id);
        if (!$lang->readIsEnabled())
        {
            $err = "Can not set language ".$lang->readCode()." as international because it is disabled";
            return false;
        }
        DB::get()->prepare("update languages
                            set is_international = case id_lang when ? then 1 else 0 end")
            ->bind(1, $id)
            ->execute();
        return true;
    }


    /**
     * Returns a default international language code
     * @return string
     */
    private function getInternationalDefault()
    {
        foreach($this->langs as $item)
        {
            if ($item->readIsInternational()) return $item->readCode();
        }
        return 'en'; // fallback
    }

    /**
     * Internal method, that returns user browser language
     * @return string
     */
    private function getBrowserLang()
    {
    	// returns the browser language code
        $langcode = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';
        $langcode = (!empty($langcode)) ? explode(";", $langcode) : $langcode;
        $langcode = (!empty($langcode['0'])) ? explode(",", $langcode['0']) : $langcode;
        $langcode = (!empty($langcode['0'])) ? explode("-", $langcode['0']) : $langcode;
        return $langcode['0'];
    }


    /**
     * Private method that identify language, that will be used on website
     * @return string
     */
    private function getFinalLang()
	{
		// returns the finally language code via different attempts
		$byurl   = $_GET['lang'];
		$bycookie  = $_COOKIE['lang'];
		$byclient  = self::getBrowserLang();
		if ($this->isExists($byurl, 0))
		{
            if ($this->readByCode($byurl)->readIsEnabled()) {
                return $byurl;
            }
		}
		if ($this->isExists($bycookie, 0))
		{
            if ($this->readByCode($bycookie)->readIsEnabled()) {
                return $bycookie;
            }
		}
        if ($byclient)
		{
			if ($this->isExists($byclient, 0))
			{
                if ($this->readByCode($byclient)->readIsEnabled()) {
                    return $byclient;
                }
			} elseif ($byclient != $this->getDomesticDefault()) {
				return $this->getInternationalDefault();
			}
		} else {
			return $this->getDomesticDefault();
		}
		return $this->getInternationalDefault();
	}


    /**
     * Idenrify language, that will be used in Admin CP
     * @return string
     */
    private function getFinalLangForAdminCP()
    {
        $bycookie  = $_COOKIE['admincp_lang'];
        if ($this->isExists($bycookie, 0))
        {
            if ($this->readByCode($bycookie)->readIsAdminCPAvailable())
            {
                return $bycookie;
            }
        }
        return 'en';
    }

    /**
     * Returns a translation of language constant
     * @param string $constCode
     * @param bool $overrideLangCode
     * @return string
     */
    public function translate($constCode, $overrideLangCode=false)
	{
        if (Output::get()->readTemplate() == Template::get()->getAdminCPCode())
        {
            $final_lang_code = $this->getUsingInAdminCP();
        } else {
            $final_lang_code = ($overrideLangCode ?
                                    $this->isExists($overrideLangCode, 0) ?
                                        $overrideLangCode :
                                        $this->getUsing() :
                                $this->getUsing()
                                );
        }
		return Constant::get()->readByCode($constCode)->readTrans($final_lang_code);
	}


    /**
     * Sends a cookie with chosen language code
     * Will be fetched then by getFinalLang()
     */
    public static function sendCookieLang() {
        if (self::get()->isExists($_GET['lang'], 0))
        {
            if (self::get()->readByCode($_GET['lang'])->readIsEnabled()) {
                setrawcookie('lang', $_GET['lang'], time() + 86400 * 30, '/');
            }
        }
        $ref = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : "/";
        Output::get()->redirect($ref);
    }

    /**
     * Sends a cookie with chosen language code for admin cp
     * Will be fetched then by getFinalLangForAdminCP()
     */
    public static function sendCookieLangForAdminCP()
    {
        if (self::get()->isExists($_GET['lang'], 0))
        {
            if (self::get()->readByCode($_GET['lang'])->readIsAdminCPAvailable()) {
                setrawcookie('admincp_lang', $_GET['lang'], time() + 86400 * 30, '/');
            }
        }
        $ref = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : "/admin/";
        Output::get()->redirect($ref);
    }


}
