<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 19.07.15
 * Time: 10:59
 */

namespace RobinTail\EngineAPI\Template;


use RobinTail\EngineAPI\Regex;

/**
 * Class Item
 * @package RobinTail\EngineAPI\Template
 */
class Item {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $html;
    /**
     * @var string
     */
    private $description;
    /**
     * @var int[]
     */
    private $styles;

    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function writeCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @param string $html
     * @return $this
     */
    public function writeHtml($html)
    {
        // issue 75
        // $this->html = html_entity_decode($html, ENT_QUOTES, 'utf-8');
        $this->html = $html;
        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function writeDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param array $styles
     * @return $this
     */
    public function writeStyles($styles)
    {
        $this->styles = Regex::arrayIntval($styles, false);
        return $this;
    }


    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function readCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function readHtml()
    {
        return $this->html;
    }

    /**
     * @return string
     */
    public function readHtmlForEditor()
    {
        return htmlentities( $this->readHtml() , ENT_NOQUOTES , 'utf-8');
    }

    /**
     * @return string
     */
    public function readDescription()
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function readStyles()
    {
        return is_array($this->styles) ? $this->styles : array();
    }


} 