<?

namespace RobinTail\EngineAPI;

class Email
{	
	private static $server     = "smtp.mastermail.ru";       // Укажите адрес SMTP-сервера
	private static $auth	   = true;
	private static $port       = "25";                  // Порт SMTP-сервера.
	private static $username   = "eater@"; // Имя почтового ящика (пользователя)
	private static $password   = "";              // и пароль к нему.
	private static $localhost  = "localhost";
	private static $newline    = "\r\n";
	private static $timeout    = 30;


	public static function check($email)
	{
		$pattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
					'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
		return preg_match ($pattern, $email);
	}


	public static function custom($to, $subject, $message, $headers)
	{
		$smtpConnect = fsockopen(self::$server, self::$port, $errno, $errstr, self::$timeout);
		$smtpResponse = fgets($smtpConnect, 515);
		  if(empty($smtpConnect))
			{
			  $logArray['connection'] = "Failed to connect: $smtpResponse";
			  return $logArray;
			}
		  else
			{
			  $logArray['connection'] = "Connected: $smtpResponse";
			}
    
		  fputs($smtpConnect, "HELO ".self::$localhost . self::$newline);
		  $smtpResponse = fgets($smtpConnect, 515);
		  $logArray['heloresponse'] = $smtpResponse;

		  if (self::$auth)
		  {
			  fputs($smtpConnect,"AUTH LOGIN" . self::$newline);
			  $smtpResponse = fgets($smtpConnect, 515);
			  $logArray['authrequest'] = $smtpResponse;

			  fputs($smtpConnect, base64_encode(self::$username) . self::$newline);
			  $smtpResponse = fgets($smtpConnect, 515);
			  $logArray['authmhSmtpMail_username'] = $smtpResponse;

			  fputs($smtpConnect, base64_encode(self::$password) . self::$newline);
			  $smtpResponse = fgets($smtpConnect, 515);
			  $logArray['authmhSmtpMail_password'] = $smtpResponse;
		  }

		  fputs($smtpConnect, "MAIL FROM: ".self::$username . self::$newline);
		  $smtpResponse = fgets($smtpConnect, 515);
		  $logArray['mailmhSmtpMail_fromresponse'] = $smtpResponse;

		  fputs($smtpConnect, "RCPT TO: ".$to . self::$newline);
		  $smtpResponse = fgets($smtpConnect, 515);
		  $logArray['mailtoresponse'] = $smtpResponse;

		  fputs($smtpConnect, "DATA" . self::$newline);
		  $smtpResponse = fgets($smtpConnect, 515);
		  $logArray['data1response'] = $smtpResponse;

		  fputs($smtpConnect, "Subject: $subject\r\n$headers\r\n\r\n$message\r\n\r\n.\r\n");
		  $smtpResponse = fgets($smtpConnect, 515);
		  $logArray['data2response'] = $smtpResponse;

		  fputs($smtpConnect,"QUIT" . self::$newline);
		  $smtpResponse = fgets($smtpConnect, 515);
		  $logArray['quitresponse'] = $smtpResponse;
		return $logArray;

	}


	public static function send($StrTo, $StrBody, $StrSubject, $StrFrom=false, $override_lang_code=false)
	{
		// отправка письма
		if (!$StrFrom) $StrFrom = Settings::get()->readValue('email_from');
		$StrTo = trim(str_replace(";", ",", $StrTo));
		$StrTo = preg_replace("/(\s*),(\s*)/si", ",",$StrTo); // remove double spaces near comma
		$StrTo = preg_replace("/(\s+)/si", ",", $StrTo); // replace space separator to comma	
		$content = Output::get()->writeOption('title', $StrSubject)
                                ->writeOption('content', $StrBody)
                                ->writeOption('prefix', Settings::get()->readValue('email_prefix'))
                                ->writeOption('logo', Settings::get()->readValue('email_logo'))
                                ->writeOption('domain', $_SERVER['SERVER_NAME'])
                                ->writeTemplate('email')
                                ->parseTemplate();
        Output::get()->flush();
		$content = Output::get()->parseTranslate($content, $override_lang_code);
		$StrSubject = Output::get()->parseTranslate($StrSubject, $override_lang_code);
		return mail($StrTo, "=?utf-8?B?".base64_encode($StrSubject)."?=", 
			$content, "Content-type: text/html; charset=utf-8\r\nFrom: ".$StrFrom);
	}

	public static function send_with_attachments($StrTo, $StrBody, $StrSubject, $attach, $from="")
	{
		// отправка письма с аттачментами
		// массив аттачментов двумерный
		// у каждого аттачмента поля: filename, data
		$StrTo = trim(str_replace(";", ",", $StrTo));
		$StrTo = preg_replace("/(\s*),(\s*)/si", ",",$StrTo); // remove double spaces near comma
		$StrTo = preg_replace("/(\s+)/si", ",", $StrTo); // replace space separator to comma	

		$boundary1   =rand(0,9)."-".rand(10000000000,9999999999)."-".rand(10000000000,9999999999)."=:".rand(10000,99999);
		$boundary2   =rand(0,9)."-".rand(10000000000,9999999999)."-".rand(10000000000,9999999999)."=:".rand(10000,99999);

		$Headers     = "From: ".($from?$from:Settings::get()->readValue('email_from'))."\n".
					   "Reply-To: ".($from?$from:Settings::get()->readValue('email_from'))."\n".
					   "MIME-Version: 1.0\n".
					   "Content-Type: multipart/mixed;\n".
					   "    boundary=\"{$boundary1}\"\n";

		$attachments='';
		foreach($attach as $i => $att)
		{
			if (!is_array($att)) return false;
			$fname = $att[filename]; $data = $att[data]; $mime=$att[mime]?$att[mime]:fs::mime_content_type_my($att[filename]);

			$attachments.="--{$boundary1}\n".
						  "Content-Type: {$mime};\n".
						  "       name=\"{$fname}\"\n".
						  "Content-Transfer-Encoding: base64\n".
						  "Content-Disposition: attachment;\n".
						  "    filename=\"{$fname}\"\n".
						  "\n".
						  chunk_split(base64_encode($data));
		
		}

		$Body  = "This is a multi-part message in MIME format.\n".
				 "\n".
				 "--{$boundary1}\n".
				 "Content-Type: multipart/alternative;\n".
				 "    boundary=\"{$boundary2}\"\n".
				 "\n".
				 "--{$boundary2}\n".
				 "Content-Type: text/html;\n".
				 "    charset=\"utf-8\"\n".
				 "Content-Transfer-Encoding: quoted-printable\n".
				 "\n".
				 $StrBody."\n".
				 "--{$boundary2}--\n".
				 "\n".
				 $attachments."\n".
				 "--{$boundary1}--\n";

		 return @mail($StrTo, "=?utf-8?B?".base64_encode($StrSubject)."?=", $Body, $Headers);
	}


	public static function send_to_admin($StrFrom, $StrBody, $StrSubject)
	{
		$StrTo = Settings::get()->readValue('email_from');
		//return @mail($StrTo, $StrSubject, $StrBody, "Content-type: text/plain; charset=windows-1251\r\nContent-Transfer-Encoding: 8bit\r\nFrom: ".$StrFrom);
		return self::send($StrTo, $StrBody, $StrSubject, $StrFrom);
	}

	public static function get_list($list_code)
	{
		// получение адресатов именнованного списка рассылки
        /*
		DB::get()->query("SELECT id_user, id_contragent, full_name, short_name, first_name, last_name, middle_name, email
					FROM user_mail_lists
					INNER JOIN users ON (user_mail_lists.user_id=users.id_user)
					INNER JOIN contragents ON (users.contragent_id=contragents.id_contragent)
					WHERE list_code='".regex::quote($list_code)."' AND email IS NOT NULL");
		return DB::get()->result();
        */
	}

	public static function get_groups($groups=array())
	{
		// получение адресатов по перечню групп
        /*
		DB::get()->query("SELECT id_user, id_contragent, full_name, short_name, first_name, last_name, middle_name, email
					FROM users
					INNER JOIN contragents ON (users.contragent_id=contragents.id_contragent)
					INNER JOIN user_groups ON (users.group_id=user_groups.id_group)
					WHERE group_code IN ('".implode("','",regex::array_quote($groups))."')");
		return DB::get()->result();
        */
	}


	public static function pop_cmd($socket,$cmd=false,$long=false)
	{
		if ($cmd) fputs($socket, $cmd);
		if ($long)
		{
		   while (!feof($socket)) {
			  $buffer = chop(fgets($socket,1024));
			  $str .= "$buffer\r\n";
			  if(trim($buffer) == ".") break;
		   }
		} else {
			$str .= fread($socket, 1024);
		}
		return $str;
	}


	public static function pop_cmd_is_ok($str)
	{
		if ((substr($str, 0, 3) == "+OK")) return true;
		return false;
	}


	public static function pop_mail_part($data)
	{
		str_replace($data, "\r\n", "\n");
		$data = preg_split("/\n/", $data);
		$offset = array_search("", $data);
		$headers = array_slice($data, 0, $offset-1);
		foreach($headers as $k => $v) $headers[$k] = trim($v);
		$headers = implode("", $headers);
		preg_match("/charset=\"(.+)\"/U", $headers, $matches);
		$charset = $matches[1];
		preg_match("/name=\"(.+)\"/U", $headers, $matches);
		$filename = $matches[1];
		preg_match_all("/=\?(.+)\?B\?(.+)\?=/U", $filename, $matches);
		if (count($matches[2]))
		{
			$filename = "";
			foreach($matches[2] as $k => $v)
			{
				$filename .= iconv($matches[1][$k],"CP1251",base64_decode($matches[2][$k]));
			}
		} else {
			preg_match_all("/=\?(.+)\?Q\?(.+)\?=/U", $filename, $matches);
			if (count($matches[2]))
			{
				$filename = "";
				foreach($matches[2] as $k => $v)
				{
					$filename .= iconv($matches[1][$k],"CP1251",quoted_printable_decode($matches[2][$k]));
				}
			}
		}
		$data = array_slice($data, $offset+1, count($data)-4);
		$data = implode("\r\n", $data);
		if ($charset) 
		{
			$data = iconv($charset, "CP1251", $data);
		} else {
			preg_match("|\.([a-z0-9]{2,4})$|i", $filename, $ext);
			$ext = $ext[1];
			$data = array(
						'ext' => strtolower($ext),
						'filename' => strtolower($filename),
						'data' => base64_decode($data)
					);
		}
		return $data;
	}



}

?>