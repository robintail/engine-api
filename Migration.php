<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 08.07.15
 * Time: 12:30
 */

namespace RobinTail\EngineAPI;


use RobinTail\EngineAPI\Output\CLI;

class Migration extends DB\Settings {
    private static $instance;

    /**
     * @return Migration
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Creates migration log table if not exist (for more older versions)
     * @throws \Exception
     */
    private function __construct()
    {
        if (CLI::get()->isCLI())
        {
            if ($_SERVER['argc']!=3) die("Usage:\r\n php _migrator.php <adminEmail> <adminPassword>\r\n   This script will update database.");
            if (!User::get()->validate($_SERVER['argv'][1], $_SERVER['argv'][2], true)) die("Authentication failed: ".User::get()->readErrorMessage());
        } else {
            Output::get()->error("This script have to be launched in CLI mode.");
        }
        DB::get()->prepare("CREATE TABLE IF NOT EXISTS `migration` (
                              `id_migration` int(11) unsigned NOT NULL AUTO_INCREMENT,
                              `migration_timestamp` int(11) NOT NULL,
                              `executed_file` varchar(255) NOT NULL DEFAULT '',
                              `executed_at` int(11) NOT NULL,
                              `snapshot_file` varchar(255) NOT NULL DEFAULT '',
                              `commit` varchar(50) NOT NULL DEFAULT '',
                              PRIMARY KEY (`id_migration`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
                ->execute();
    }


    /**
     * Here is the settings where the migration files are located
     * @return string
     */
    public function getMigrationDir()
    {
        return Path::resolve("vendor/robintail/engine-api/migrationScripts");
    }


    /**
     * Here is the setting where the backup snapshots are located
     * @return string
     */
    public function getSnapshotsDir()
    {
        return Path::resolve("vendor/robintail/engine-api/migrationSnapshots");
    }

    /**
     * Create new snapshot of current database
     * @return string
     * @throws \Exception
     */
    private function makeSnapshot()
    {
        $filename = time().".sql";
        $command = "mysqldump --host=".parent::$host." --port=".parent::$port." --user=".parent::$user." --password=\"".parent::$pass."\" --result-file=\"".$this->getSnapshotsDir()."/".$filename."\" ".parent::$base;
        exec($command, $output, $returnValue);
        if ($returnValue !== 0) {
            throw new \Exception(implode("\r\n", $output));
        }
        return $filename;
    }

    /**
     * Restore database from the snapshot file
     * @param string $filename
     * @return bool
     * @throws \Exception
     */
    private function restoreFromSnapshot($filename)
    {
        $command = "mysql --host=".parent::$host. " --port=".parent::$port." --user=".parent::$user." --password=\"".parent::$pass."\" ".parent::$base." < ".$this->getSnapshotsDir()."/".$filename;
        exec($command, $output, $returnValue);
        if ($returnValue !== 0) {
            throw new \Exception(implode("\r\n", $output));
        }
        return true;
    }

    /**
     * Get sorted and filtered list of migration files have to be executed
     * @return array
     */
    private function getMigrationFiles()
    {
        $files = array();
        $last = $this->getLastMigrationTimestamp();
        $pre = scandir($this->getMigrationDir());
        sort($pre);
        foreach($pre as $filename){
            if (is_file($this->getMigrationDir()."/".$filename)) {
                if (preg_match("/^(\d+)\.sql$/", $filename, $matches) === 1) {
                    $timestamp = $matches[1];
                    if ($timestamp > $last) {
                        $files[] = array('filename' => $filename, 'timestamp' => $timestamp);
                    }
                }
            }
        }
        return $files;
    }

    /**
     * Get the timestamp of last migration file executed
     * @return int
     * @throws \Exception
     */
    public function getLastMigrationTimestamp()
    {
        DB::get()->prepare("SELECT MAX(migration_timestamp) as thelast
                            FROM migration")
                ->execute();
        if ($row = DB::get()->fetchRow())
        {
            return $row['thelast'];
        } else {
            return 0;
        }
    }

    /**
     * Log information about successfully migration file execution
     * @param int $migrationTimestamp
     * @param string $executedFile
     * @param string $snapshotFile
     * @param string $commitHash
     * @throws \Exception
     */
    private function logMigrationFile($migrationTimestamp, $executedFile, $snapshotFile, $commitHash)
    {
        DB::get()->prepare("INSERT INTO migration (migration_timestamp, executed_file, executed_at, snapshot_file, commit)
                            SELECT :migration_timestamp, :executed_file, :executed_at, :snapshot_file, :commit")
            ->bind(':migration_timestamp', $migrationTimestamp)
            ->bind(':executed_file', $executedFile)
            ->bind(':executed_at', time())
            ->bind(':snapshot_file', $snapshotFile)
            ->bind(':commit', $commitHash)
            ->execute();
    }

    /**
     * Proceed the migration action.
     * @return string
     */
    public function proceed()
    {
        $git = new Git();
        $isSnapshotFail = false;
        $isMigrationFail = false;
        $snapshotFilename = "";

        echo "\r\n".CLI::get()->getColoredString("Engine.API Database Migration", "white")."\r\nCurrent version: ".CLI::get()->getColoredString($git->getPackageVersion(),"green")."\r\n";

        /* close website */
        echo "Closing website... ";
        Settings::get()->write('maintenance_mode', 1);
        echo CLI::get()->getColoredString("OK","green")."\r\n";

        /* find migration files */
        $files = $this->getMigrationFiles();

        /* snapshots */
        if (count($files))
        {
            echo "Creating database snapshot... ";
            try {
                $result = $this->makeSnapshot();
                $snapshotFilename = $result;
                echo CLI::get()->getColoredString("OK","green")."\r\n";
            } catch (\Exception $e)
            {
                echo CLI::get()->getColoredString("FAILED","red")."\r\n".$e->getMessage()."\r\n";
                $isSnapshotFail = true;
            }
        }

        /* migration files execution */
        if (!$isSnapshotFail) {
            foreach ($files as $file) {
                if ($sql = file_get_contents($this->getMigrationDir() . "/" . $file['filename'])) {
                    $queries = preg_split("/;$/m", $sql);
                    echo "Executing migration script " . $file['filename'] . " [" . count($queries) . "]... \r\n  ";
                    foreach($queries as $q) {
                        try {
                            if (trim($q)) DB::get()->prepare($q)->execute();
                            echo "*";
                        } catch (\Exception $e) {
                            echo CLI::get()->getColoredString("FAILED", "red") . "\r\n" . $e->getMessage() . "\r\n";
                            $isMigrationFail = true;
                        }
                        if ($isMigrationFail) break 2; // break cycles of queries and files
                    }
                    echo CLI::get()->getColoredString("OK", "green") . "\r\n";
                    $this->logMigrationFile($file['timestamp'], $file['filename'], $snapshotFilename, "n/a");
                }
            }
        }

        /* rollback */
        if ($isMigrationFail)
        {
            echo "Restoring database from snapshot ".$snapshotFilename."... ";
            try {
                $this->restoreFromSnapshot($snapshotFilename);
                echo CLI::get()->getColoredString("OK","green")."\r\n";
            } catch (\Exception $e)
            {
                echo CLI::get()->getColoredString("FAILED","red")."\r\n".$e->getMessage()."\r\n";
            }
        }

        /* open website */
        echo "Opening website... ";
        Settings::get()->write('maintenance_mode', 0);
        echo CLI::get()->getColoredString("OK","green")."\r\n";

    }



} 