<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 09.07.15
 * Time: 22:58
 */

namespace RobinTail\EngineAPI;


class Path {

    /**
     * Return full path of the project root
     * @return string
     */
    public static function getRoot()
    {
        // up to engine-api/, robintail/ and vendor/
        return realpath(__DIR__.'/../../..');
    }

    /**
     * Returns full path of relative project path
     * @param string $projectPath
     * @return string
     */
    public static function resolve($projectPath)
    {
        $projectPath = str_replace('\\', '/', $projectPath);
        $absolute_path = self::getRoot() .
                        (substr($projectPath, 0, 1) === '/' ? '' : '/') .
                        $projectPath;
        return $absolute_path;
    }

    /**
     * Check process if the owner of file/directory
     * @param string $file
     * @return bool
     */
    public static function isProcessFileOwner($file = __FILE__)
    {
        return (posix_getuid() === stat($file)['uid']);
    }


} 