<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 24.07.15
 * Time: 20:19
 */

namespace RobinTail\EngineAPI;

use RobinTail\EngineAPI\Output\CLI;
use RobinTail\EngineAPI\Settings;

class Install extends DB\Settings {
    /**
     * @var Install
     */
    private static $instance;
    /**
     * @var \PDO
     */
    private $pdo;
    /**
     * @var string
     */
    private $adminName;
    /**
     * @var string
     */
    private $adminEmail;
    /**
     * @var string
     */
    private $adminPassword;

    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    private function __construct()
    {
        if (!CLI::get()->isCLI()) die("This script have to be executed in CLI mode (from command line).");
        chdir(Path::getRoot());
        if (!Path::isProcessFileOwner(Path::getRoot())) die("Process is not file owner of project root");
        if (file_exists('installed')) die("Lock file 'installed' is exists. Remove it to install.");
        if (file_exists('index.php')) die("Entry point file 'index.php' is exists. Remove it to install.");
        if (file_exists('tmp')) die("'tmp' directory is exists. Remove it to install.");
        if (file_exists('images')) die("'images' directory is exists. Remove it to install.");
        echo "\r\n".CLI::get()->getColoredString("Engine Installer","white")."\r\n";
        parent::setHost(readline("Enter database host (for localhost please use 127.0.0.1): "));
        parent::setPort(readline("Enter host port (3306 is default): "));
        parent::setBase(readline("Enter database name: "));
        do {
            $answer = strtolower(readline("Is the database '" . parent::$base . "' exists (y/n): "));
        } while (!in_array($answer, array('y', 'n')));
        parent::setBaseIsExists($answer == 'y');
        parent::setUser(readline("Enter database user name: "));
        parent::setPass(readline("Enter database password: "));
        echo "Creating MySQL connection... ";
        $this->pdo = new \PDO('mysql:host='.parent::$host.';port='.parent::$port, parent::$user, parent::$pass) or die("Error connecting to host. Please check your input.");
        echo CLI::get()->getColoredString("OK","green")."\r\n";
        $this->adminName = readline("Enter Engine admin name: ");
        $this->adminEmail = readline("Enter Engine admin email: ");
        $this->adminPassword = readline("Enter Engine admin password: ");
    }

    public function proceed()
    {
        $this->entryFile();
        $this->directories();
        $this->createDatabase();
        $this->createAdmin();
        echo "\r\n";
        echo CLI::get()->getColoredString("You are awesome ^_^","cyan")."\r\n";
    }

    private function createDatabase()
    {
        $database = <<<'HTM'

CREATE TABLE `captcha` (
  `id_captcha` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_create_unix` int(11) NOT NULL,
  `code` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_captcha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `languages` (
  `id_lang` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(2) NOT NULL DEFAULT '',
  `lang_name` varchar(20) NOT NULL DEFAULT '',
  `lang_flag` varchar(255) NOT NULL DEFAULT '',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_domestic` tinyint(1) NOT NULL DEFAULT '0',
  `is_international` tinyint(1) NOT NULL DEFAULT '0',
  `is_admincp_avail` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lang`),
  UNIQUE KEY `lang_code` (`lang_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `languages` (`id_lang`, `lang_code`, `lang_name`, `lang_flag`, `is_enabled`, `is_protected`, `is_domestic`, `is_international`, `is_admincp_avail`)
VALUES
  (1, 'ru', 'Русский', '/vendor/robintail/engine-api/images/flags/ru.gif', 1, 1, 1, 0, 1),
  (2, 'en', 'English', '/vendor/robintail/engine-api/images/flags/gb.gif', 1, 1, 0, 1, 1);


CREATE TABLE `language_const` (
  `id_const` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `const_code` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_const`),
  UNIQUE KEY `const_code` (`const_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `language_const` (`id_const`, `const_code`)
VALUES
	(1,'admin-cp'),
	(2,'auth-error'),
	(3,'brand'),
	(4,'cancel'),
	(5,'captcha-enter'),
	(6,'captcha-error'),
	(7,'captcha-help'),
	(8,'change-password'),
	(9,'email'),
	(10,'email-sent'),
	(11,'error'),
	(12,'error-email-empty'),
	(13,'error-message'),
	(14,'error-message-empty'),
	(15,'error-name-empty'),
	(16,'id-and-key-error'),
	(17,'key-error'),
	(18,'login'),
	(19,'logout'),
	(20,'message'),
	(21,'name'),
	(22,'new-password'),
	(23,'password'),
	(24,'password-changed'),
	(25,'password-changed-email'),
	(26,'password-length-error'),
	(27,'password-really-changed'),
	(28,'password-recovery'),
	(29,'quick-edit'),
	(30,'recover-password'),
	(31,'recovery-email-link'),
	(32,'recovery-email-notice'),
	(33,'recovery-email-sent'),
	(34,'recovery-form-notice'),
	(35,'recovery-key'),
	(36,'recovery-request'),
	(37,'recovery-start-info'),
	(38,'redirect'),
	(39,'redirect-help'),
	(40,'register'),
	(41,'register-email'),
	(42,'register-error'),
	(43,'remember-your-password'),
	(44,'remove'),
	(45,'send-me-recovery'),
	(46,'set-new-password'),
	(47,'user-not-found-error'),
	(48,'your-id'),
	(49,'your-key'),
	(50, 'search'),
	(51, 'search-results-for'),
	(52, 'feedback-link'),
	(53, 'message-from'),
	(54, 'translation-missing-title'),
	(55, 'translation-missing'),
	(56, 'pages'),
	(57, 'menu'),
	(58, 'styles'),
	(59, 'languages'),
	(60, 'lang-consts'),
	(61, 'settings'),
	(62, 'images'),
	(63, 'users'),
	(73, 'templates'),
	(75, 'self-update'),
	(76, 'self-update-warning'),
	(77, 'branch'),
	(78, 'version'),
	(79, 'commit'),
	(80, 'last-changes'),
	(81, 'hosting'),
	(82, 'host'),
	(83, 'root-path'),
	(84, 'extensions'),
	(95, 'new-page'),
	(96, 'title'),
	(97, 'template'),
	(98, 'confirm-page-remove'),
	(99, 'description'),
	(100, 'keywords'),
	(101, 'error-page-url-exists'),
	(102, 'error-url-not-specified'),
	(103, 'error-file-or-directory-exists'),
	(104, 'do-not-use'),
	(105, 'override-active-link'),
	(106, 'override-active-link-help'),
	(107, 'use-affix'),
	(108, 'use-affix-help'),
	(109, 'language'),
	(110, 'enable-translation'),
	(111, 'title-short'),
	(112, 'title-long'),
	(113, 'contains'),
	(114, 'save'),
	(115, 'load-more'),
	(116, 'confirm-image-remove'),
	(117, 'confirm-menu-remove'),
	(118, 'parent'),
	(119, 'element'),
	(120, 'ordering'),
	(121, 'dropdown-right'),
	(122, 'new-element'),
	(123, 'no-parent'),
	(124, 'page'),
	(125, 'text'),
	(126, 'new-style'),
	(127, 'code'),
	(128, 'type'),
	(129, 'confirm-style-remove'),
	(130, 'link'),
	(131, 'error-style-code-exists'),
	(132, 'error-smth-not-specified'),
	(133, 'link-to-css'),
	(134, 'link-to-css-help'),
	(135, 'or'),
	(136, 'style-text'),
	(137, 'style-text-help'),
	(138, 'new-template'),
	(139, 'confirm-template-remove'),
	(140, 'error-template-code-exists'),
	(141, 'choose'),
	(142, 'several'),
	(143, 'template-constants-help'),
	(144, 'new-language'),
	(145, 'flag'),
	(146, 'enabled'),
	(147, 'domestic'),
	(148, 'international'),
	(149, 'protected'),
	(150, 'confirm-language-remove'),
	(151, 'error-lang-code-exists'),
	(152, 'error-lang-const-code-exists'),
	(153, 'error-language-protected'),
	(154, 'flag-help'),
	(155, 'new-lang-const'),
	(156, 'confirm-lang-const-remove'),
	(157, 'value'),
	(158, 'error-setting-hidden'),
	(159, 'select-files'),
	(160, 'upload'),
	(161, 'insert-thumbnail'),
	(162, 'admin'),
	(163, 'admin-rights'),
	(164, 'new-user'),
	(165, 'confirm-user-remove'),
	(166, 'confirm-change-permissions');





CREATE TABLE `language_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(10) unsigned NOT NULL,
  `const_id` int(10) unsigned NOT NULL,
  `trans_value` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_trans`),
  UNIQUE KEY `lang_id` (`lang_id`,`const_id`),
  KEY `toconst` (`const_id`),
  CONSTRAINT `toconst` FOREIGN KEY (`const_id`) REFERENCES `language_const` (`id_const`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tolang` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `language_trans` (`id_trans`, `lang_id`, `const_id`, `trans_value`)
VALUES
	(1,1,3,'[БРЕНД]'),
	(2,1,8,'Сменить пароль'),
	(3,1,19,'Выход'),
	(4,1,18,'Вход'),
	(5,1,9,'EMail'),
	(6,1,23,'Пароль'),
	(7,1,30,'Я не помню пароль'),
	(8,1,38,'Перенаправление'),
	(9,1,39,'Нажмите сюда, если ничего не происходит'),
	(10,1,5,'Введите код безопасности'),
	(11,1,7,'Для предотвращения спама необходимо ввести цифровой код, изображенный на картинке, чтобы подтвердить, что вы являетесь человеком, а не роботом.'),
	(12,1,20,'Сообщение'),
	(13,1,2,'Ошибка авторизации'),
	(14,1,16,'ID и Ключ должны быть введены'),
	(15,1,26,'Пароль должен быть не короче 6 символов'),
	(16,1,6,'Код безопасности введен неправильно'),
	(17,1,47,'Пользователь не найден'),
	(18,1,11,'Ошибка'),
	(19,1,1,'Админ центр'),
	(20,1,13,'Сообщение об ошибке'),
	(21,1,29,'Исправить'),
	(22,1,44,'Удалить'),
	(23,1,21,'Имя'),
	(24,1,4,'Отмена'),
	(25,1,41,'<p>Мы благодарим вас за регистрацию. Мы заботимся о вашей конфиденциальности. Вход на сайт осуществляется по email адресу и введённому вами паролю. Пароль всегда можно восстановить или изменить.</p><p>Авторизация необходима для совершения заказов на сайте. В вашем распоряжении персональная адресная книга, с помощью которой вы сможете делать заказы для себя и своих знакомых, а также дарить полезные подарки друзьям.</p>'),
	(26,1,40,'Регистрация'),
	(27,1,42,'Ошибка регистрации'),
	(28,1,10,'Письмо отправлено'),
	(29,1,33,'Письмо с рекзивитами для восстановления пароля было успешно отправлено на учетный адрес. Реквизиты действительны до конца сегодняшнего дня'),
	(30,1,32,'Была запрошена возможность восстановления забытого пароля по вашему email. Если вы НЕ запрашивали восстановления пароля, просто проигнорируйте и удалите это письмо. Если вы действительно запросили восстановление пароля, высылаем вам реквизиты для завершения этой операции.'),
	(31,1,35,'Ключ'),
	(32,1,31,'Обратите внимание - этот ключ действителен до конца сегодняшнего дня. Чтобы восстановить пароль, пройдите по следующей ссылке и введите требуемые реквизиты в форму.'),
	(33,1,28,'Восстановление пароля'),
	(34,1,34,'Вы должны были получить письмо с указанием реквизитов восстановления пароля. В этом письме указаны ваш ID и Ключ. Введите пожалуйста эти данные, а также НОВЫЙ пароль, который вы хотите присвоить своей учетной записи, который должен быть НЕ КОРОЧЕ 6 символов.'),
	(35,1,48,'Ваш ID'),
	(36,1,49,'Ваш Ключ'),
	(37,1,22,'НОВЫЙ пароль'),
	(38,1,46,'Задать новый пароль'),
	(39,1,37,'Если вы забыли свой пароль, вы можете воспользоваться этой формой для восстановления. Чтобы восстановить пароль, введите пожалуйста свой email. После отправки этой формы, система вышлет письмо на ваш учетный email адрес с инструкцией для задания нового пароля вашей учетной записи. Инструкция для восстановления пароля действительна до конца сегодняшнего дня.'),
	(40,1,45,'Выслать данные на почту'),
	(41,1,24,'Пароль к вашей учетной записи был успешно изменен.'),
	(42,1,43,'Запомните ваш пароль, проверьте возможность входа на сайт.'),
	(43,1,25,'Пароль изменен'),
	(44,1,36,'Запрос на восстановление пароля'),
	(45,1,27,'Пароль был успешно изменен. Уведомляющее письмо отправлено на ваш учетный адрес.'),
	(46,1,17,'Ключ неверен'),
	(47,1,14,'Вы ничего не написали'),
	(48,1,15,'Вы не ввели имя'),
	(49,1,12,'Вы не ввели email'),
	(64,2,3,'[BRAND]'),
	(65,2,8,'Change password'),
	(66,2,19,'Logout'),
	(67,2,18,'Login'),
	(68,2,9,'EMail'),
	(69,2,23,'Password'),
	(70,2,30,'Recover password'),
	(71,2,38,'Redirecting'),
	(72,2,39,'Click here if nothing happens'),
	(73,2,5,'Enter the security code'),
	(74,2,7,'To prevent spam you have to enter numbers from this picture to make us sure you are not a robot.'),
	(75,2,20,'Message'),
	(76,2,2,'Authentication error'),
	(77,2,16,'ID and Key have to be entered'),
	(78,2,26,'Password have to be 6 or more chars of length'),
	(79,2,6,'Security code is invalid'),
	(80,2,47,'User not found'),
	(81,2,11,'Error'),
	(82,2,1,'Admin CP'),
	(83,2,13,'Error message'),
	(84,2,29,'Edit'),
	(85,2,44,'Remove'),
	(86,2,21,'Name'),
	(87,2,4,'Cancel'),
	(88,2,41,'<p>Thank you for registering. We take care of your privacy. Sign in is implemented with email address and password you entered. Password can always be recovered or changed.</p><p>Authorization is required to perform orders on the website. There is a personal address book, with which you can place orders for you and your friends, and also to give useful gifts.</p>'),
	(89,2,40,'Registration'),
	(90,2,42,'Registration error'),
	(91,2,10,'Email sent'),
	(92,2,33,'The email with password recovery information sent. It is valid until the day ends.'),
	(93,2,32,'There was requested the ability to recover a forgotten password for your email. If you are not requested for password recovery, just ignore and delete this email. If you really asked for password recovery, we send you details to complete this operation.'),
	(94,2,35,'Key'),
	(95,2,31,'Please note that this key is valid until the end of the day. To recover the password, go to the following link and enter the required details in the form.'),
	(96,2,28,'Password recovery'),
	(97,2,34,'You should have received a email with details of password recovery. In this email are indicated your ID and Key. Please enter the data, and the new password you want to assign to your account, which must be at least 6 characters.'),
	(98,2,48,'Account ID'),
	(99,2,49,'Account Key'),
	(100,2,22,'NEW password'),
	(101,2,46,'Set new password'),
	(102,2,37,'If you have forgotten your password, you can use this form to recover. To recover your password, please enter your email.\nAfter submitting this form, the system will send an email with instructions to set a new password for your account. Instructions to reset your password are valid until the end of the day.'),
	(103,2,45,'Recover my password'),
	(104,2,24,'Your account password has been changed.'),
	(105,2,43,'Please remember your password.'),
	(106,2,25,'Password changed'),
	(107,2,36,'Password recovery request'),
	(108,2,27,'Account password changed. Notification to your email sent.'),
	(109,2,17,'Account key is invalid'),
	(110,2,14,'You did not write anything'),
	(111,2,15,'You did not enter your name'),
	(112,2,12,'You did not enter your email'),
	(113, 1, 50, 'Поиск'),
	(114, 2, 50, 'Search'),
	(115, 1, 51, 'Результаты поиска для'),
	(116, 2, 51, 'Search results for'),
	(117, 1, 52, 'Обратная связь'),
	(118, 2, 52, 'Feedback'),
	(119, 1, 53, 'Сообщение от'),
	(120, 2, 53, 'Message from'),
	(121, 1, 54, 'Извините, перевод отсутствует'),
	(122, 2, 54, 'Sorry, translation missing'),
	(123, 1, 55, 'Перевод этой страницы на выбранном вами языке отсутствует &mdash; показан перевод на другом языке.'),
	(124, 2, 55, 'Page translation on selected language is not available &mdash; another translation shown.'),
	(125, 1, 56, 'Страницы'),
	(126, 2, 56, 'Pages'),
	(127, 1, 57, 'Меню'),
	(128, 2, 57, 'Menu'),
	(129, 1, 58, 'Стили'),
	(130, 2, 58, 'Styles'),
	(131, 1, 59, 'Языки'),
	(132, 2, 59, 'Languages'),
	(133, 1, 60, 'Языковые константы'),
	(134, 1, 61, 'Настройки'),
	(135, 2, 61, 'Settings'),
	(136, 1, 62, 'Изображения'),
	(137, 2, 62, 'Images'),
	(138, 1, 63, 'Пользователи'),
	(139, 2, 63, 'Users'),
	(140, 1, 73, 'Шаблоны'),
	(141, 2, 73, 'Templates'),
	(142, 2, 60, 'Language constants'),
	(146, 1, 75, 'Обновление'),
	(147, 2, 75, 'Self update'),
	(148, 1, 76, 'ВНИМАНИЕ: Это действие может повлечь конфликты, если ваши файлы были изменены. Вы можете отменить это с помощью команды: git merge --abort. Вы уверены, что хотите продолжить?'),
	(149, 2, 76, 'WARNING: This action may cause some conflicts if your files were changed. You can undo this action with command: git merge --abort. Are you sure you want to continue?'),
	(150, 1, 77, 'Ветка'),
	(151, 2, 77, 'Branch'),
	(152, 1, 78, 'Версия'),
	(153, 2, 78, 'Version'),
	(154, 1, 79, 'Коммит'),
	(155, 2, 79, 'Commit'),
	(156, 1, 80, 'Последние изменения'),
	(157, 2, 80, 'Last changes'),
	(158, 1, 81, 'Хостинг'),
	(159, 2, 81, 'Hosting'),
	(160, 1, 82, 'Хост'),
	(161, 2, 82, 'Host'),
	(162, 1, 83, 'Путь'),
	(163, 2, 83, 'Root path'),
	(164, 1, 84, 'Расширения'),
	(184, 2, 84, 'Extensions'),
	(185, 1, 95, 'Новая страница'),
	(186, 2, 95, 'New page'),
	(188, 1, 96, 'Заголовок'),
	(189, 2, 96, 'Title'),
	(191, 1, 97, 'Шаблон'),
	(192, 2, 97, 'Template'),
	(194, 1, 98, 'Вы действительно хотите удалить эту страницу?'),
	(195, 2, 98, 'Are you sure you want to remove this page?'),
	(197, 1, 99, 'Описание'),
	(198, 2, 99, 'Description'),
	(200, 1, 100, 'Слова'),
	(201, 2, 100, 'Keywords'),
	(203, 1, 101, 'Страница с таким URL уже существует'),
	(204, 2, 101, 'Page with this URL is already exists'),
	(206, 1, 102, 'URL не указан'),
	(207, 2, 102, 'URL not specified'),
	(209, 1, 103, 'Файл или каталог с таким именем уже существует'),
	(210, 2, 103, 'File or directory already exists'),
	(212, 1, 104, '-не использовать-'),
	(213, 2, 104, '-do not use-'),
	(215, 1, 105, 'Переопределение активного пункта меню'),
	(216, 2, 105, 'Override active menu item'),
	(218, 1, 106, 'Если данная страница не входит в главное меню, то можно установить значение активного пункта меню при её просмотре. Данная настройка имеет приоритет над меню.'),
	(219, 2, 106, 'If main menu does not include this page, you can set an active menu item for it. This feature has a priority over main menu settings.'),
	(221, 1, 107, 'Использовать оглавление'),
	(222, 2, 107, 'Use affixed table of contents'),
	(224, 1, 108, 'Оглавление создаётся автоматически на основе анализа вложенности заголовков содержимого страницы.'),
	(225, 2, 108, 'Table of contents will be generated automatically by analysis of text headers hierarchy.'),
	(227, 1, 109, 'Язык'),
	(228, 2, 109, 'Language'),
	(230, 1, 110, 'Задействовать этот перевод страницы'),
	(231, 2, 110, 'Enable this translation'),
	(233, 1, 111, 'Заголовок короткий'),
	(234, 2, 111, 'Short title'),
	(236, 1, 112, 'Заголовок длинный'),
	(237, 2, 112, 'Long title'),
	(239, 1, 113, 'Содержимое'),
	(240, 2, 113, 'Content'),
	(242, 1, 114, 'Сохранить'),
	(243, 2, 114, 'Save'),
	(245, 1, 115, 'Загрузить ещё'),
	(246, 2, 115, 'Load more'),
	(248, 1, 116, 'Удалить это изображение?'),
	(249, 2, 116, 'Remove this image?'),
	(250, 1, 117, 'Удалить элемент меню?'),
	(251, 2, 117, 'Remove menu element?'),
	(253, 1, 118, 'Родитель'),
	(254, 2, 118, 'Parent'),
	(256, 1, 119, 'Элемент'),
	(257, 2, 119, 'Element'),
	(259, 1, 120, 'Сортировка'),
	(260, 2, 120, 'Order'),
	(262, 1, 121, 'Выпадает справа'),
	(263, 2, 121, 'Dropdown right'),
	(265, 1, 122, 'Новый элемент'),
	(266, 2, 122, 'New element'),
	(268, 1, 123, '-без родителя-'),
	(269, 2, 123, '-no parent-'),
	(271, 1, 124, 'Страница'),
	(272, 2, 124, 'Page'),
	(274, 1, 125, 'Текст'),
	(275, 2, 125, 'Text'),
	(277, 1, 126, 'Новый стиль'),
	(278, 2, 126, 'New style'),
	(280, 1, 127, 'Код'),
	(281, 2, 127, 'Code'),
	(283, 1, 128, 'Тип'),
	(284, 2, 128, 'Type'),
	(286, 1, 129, 'Удалить этот стиль?'),
	(287, 2, 129, 'Remove this style?'),
	(289, 1, 130, 'Ссылка'),
	(290, 2, 130, 'Link'),
	(292, 1, 131, 'Этот код стиля уже существует'),
	(293, 2, 131, 'This style code is already exists'),
	(295, 1, 132, 'аргумент не определён'),
	(296, 2, 132, 'argument not specified'),
	(298, 1, 133, 'Ссылка на файл CSS'),
	(299, 2, 133, 'Link to CSS file'),
	(301, 1, 134, 'Ссылка имеет приоритет: если это поле заполнено, то ссылка используется вместо текста стиля.'),
	(302, 2, 134, 'Link has a priority: if this field is not empty, then link is used instead of style text.'),
	(304, 1, 135, 'или'),
	(305, 2, 135, 'or'),
	(307, 1, 136, 'Текст стиля'),
	(308, 2, 136, 'Style text'),
	(310, 1, 137, 'Опишите используемые классы. Теги &lt;style&gt; не нужны. Ссылка должна быть пустой для использования текста.'),
	(311, 2, 137, 'Declare your classes. Tags &lt;style&gt; are not required. Style link have to be empty to use this text.'),
	(313, 1, 138, 'Новый шаблон'),
	(314, 2, 138, 'New template'),
	(316, 1, 139, 'Удалить этот шаблон?'),
	(317, 2, 139, 'Remove this template?'),
	(319, 1, 140, 'Шаблон с таким кодом уже существует'),
	(320, 2, 140, 'Template with this code is already exists'),
	(322, 1, 141, 'Выберите'),
	(323, 2, 141, 'Choose'),
	(325, 1, 142, 'Несколько'),
	(326, 2, 142, 'Several'),
	(328, 1, 143, 'Доступно использование переменных шаблонов'),
	(329, 2, 143, 'Use can use template snippets'),
	(331, 1, 144, 'Новый язык'),
	(332, 2, 144, 'New language'),
	(334, 1, 145, 'Флаг'),
	(335, 2, 145, 'Flag'),
	(337, 1, 146, 'Включено'),
	(338, 2, 146, 'Enabled'),
	(340, 1, 147, 'Локальный'),
	(341, 2, 147, 'Domestic'),
	(343, 1, 148, 'Международный'),
	(344, 2, 148, 'International'),
	(346, 1, 149, 'Защита'),
	(347, 2, 149, 'Protected'),
	(349, 1, 151, 'Язык с таким кодом уже существует'),
	(350, 2, 151, 'Language with this code is already exists'),
	(352, 1, 152, 'Языковая константа с таким кодом уже существует'),
	(353, 2, 152, 'Language constant with this code is already exists'),
	(355, 1, 150, 'Удалить этот язык?'),
	(356, 2, 150, 'Remove this language?'),
	(358, 1, 153, 'Вы не можете редактировать защищённый язык'),
	(359, 2, 153, 'You can not edit protected language'),
	(361, 1, 154, 'Укажите относительный путь к маленькой картинке флага, например /vendor/robintail/engine-api/images/flags/gb.gif'),
	(362, 2, 154, 'Specify the relative path to small flag image, like /vendor/robintail/engine-api/images/flags/gb.gif'),
	(364, 1, 155, 'Новая константа'),
	(365, 2, 155, 'New constant'),
	(367, 1, 156, 'Удалить эту константу?'),
	(368, 2, 156, 'Remove this constant?'),
	(370, 1, 157, 'Значение'),
	(371, 2, 157, 'Value'),
	(373, 1, 158, 'Вы не можете редактировать скрытую настройку.'),
	(374, 2, 158, 'You can not edit hidden setting.'),
	(376, 1, 159, 'Выбрать файлы'),
	(377, 2, 159, 'Select files'),
	(379, 1, 160, 'Загрузить'),
	(380, 2, 160, 'Upload'),
	(382, 1, 161, 'Вставлять как эскиз'),
	(383, 2, 161, 'Insert as thumbnail'),
	(385, 1, 162, 'Администратор'),
	(386, 2, 162, 'Administrator'),
	(388, 1, 163, 'Права администратора'),
	(389, 2, 163, 'Administrator rights'),
	(391, 1, 164, 'Новый пользователь'),
	(392, 2, 164, 'New user'),
	(394, 1, 165, 'Удалить этого пользователя?'),
	(395, 2, 165, 'Remove this user?'),
	(397, 1, 166, 'Изменить права доступа этому пользователю?'),
	(398, 2, 166, 'Change permissions for this user?');




CREATE TABLE `templates` (
  `id_template` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(50) NOT NULL DEFAULT '',
  `template_html` text NOT NULL,
  `template_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_template`),
  UNIQUE KEY `template_code` (`template_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `pages` (
  `id_page` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_url` varchar(50) NOT NULL DEFAULT '',
  `template_id` int(10) unsigned NOT NULL DEFAULT '7',
  `override_active_link_id` int(10) unsigned DEFAULT NULL,
  `is_use_affix` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_page`),
  UNIQUE KEY `page_url` (`page_url`),
  KEY `totemplate` (`template_id`),
  KEY `tomenu` (`override_active_link_id`),
  CONSTRAINT `totemplate` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `page_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `is_trans_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `page_id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `page_title_short` varchar(150) NOT NULL DEFAULT '',
  `page_title_long` varchar(255) NOT NULL DEFAULT '',
  `page_html` longtext NOT NULL,
  `page_keywords` varchar(255) NOT NULL DEFAULT '',
  `page_description` varchar(255) NOT NULL DEFAULT '',
  `page_affix` longtext NOT NULL,
  PRIMARY KEY (`id_trans`),
  KEY `topage2` (`page_id`),
  KEY `tolang2` (`lang_id`),
  CONSTRAINT `tolang2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `topage2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id_page`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `menu` (
  `id_link` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_link_id` int(10) unsigned DEFAULT NULL,
  `link_page_id` int(10) unsigned DEFAULT NULL,
  `link_title_short` varchar(50) DEFAULT NULL,
  `link_ordering` int(11) NOT NULL DEFAULT '0',
  `is_dropdown_right` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_link`),
  KEY `toparent` (`parent_link_id`),
  KEY `topage` (`link_page_id`),
  CONSTRAINT `topage` FOREIGN KEY (`link_page_id`) REFERENCES `pages` (`id_page`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `toparent` FOREIGN KEY (`parent_link_id`) REFERENCES `menu` (`id_link`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE pages
ADD CONSTRAINT tomenu FOREIGN KEY (override_active_link_id) REFERENCES menu (id_link) ON DELETE SET NULL ON UPDATE CASCADE;


CREATE TABLE `styles` (
  `id_style` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_code` varchar(50) NOT NULL,
  `style_description` varchar(255) DEFAULT NULL,
  `style_link` varchar(255) DEFAULT NULL,
  `style_text` longtext,
  PRIMARY KEY (`id_style`),
  UNIQUE KEY `style_code` (`style_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `styles` (`id_style`, `style_code`, `style_description`, `style_link`, `style_text`)
VALUES
	(1, 'common', 'Стандартный стиль страницы', '', 'body\r\n{\r\n	margin: 100px 50px;\r\n}\r\n\r\n.mce-fullscreen\r\n{\r\n	padding-top: 60px !important;\r\n}\r\n\r\n.modal\r\n{\r\n	z-index: 65537;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb > img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\n.main-menu-button\r\n{\r\n    padding: 8px;\r\n}\r\n\r\n/* affix and scrollspy */\r\n\r\n.affix\r\n{\r\n   top: 65px;\r\n}\r\n\r\n.page-affix-dropdown-btn\r\n{\r\n    position: fixed;\r\n    left: 20px;\r\n    top: 65px;\r\n    z-index: 2;\r\n}\r\n\r\n.page-affix-dropdown-btn > .nav\r\n{\r\n    max-height: 65vh;\r\n    width: 65vw;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.page-affix-dropdown-btn .nav > li > a\r\n{\r\n    padding: 5px 10px !important;\r\n    color: #262626;\r\n}\r\n\r\n#page-affix .nav .nav\r\n{\r\n    display: none;\r\n}\r\n\r\n#page-affix > .nav > li > .nav\r\n{\r\n    font-size: smaller;\r\n}\r\n\r\n#page-affix > .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav > li > a\r\n{\r\n    padding-left: 25px !important;\r\n}\r\n\r\n#page-affix > .nav .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav .nav > li > a\r\n{\r\n    padding-left: 50px !important;\r\n}\r\n\r\n\r\n#page-affix li > a\r\n{\r\n    border-left: 2px solid transparent;\r\n    padding: 3px;\r\n}\r\n\r\n#page-affix li.active > a\r\n{\r\n    border-left: 2px solid black;\r\n}\r\n\r\n#page-affix li.active > .nav\r\n{\r\n    display: block;\r\n}\r\n\r\nh1 > a[id]:not([href]), h2 > a[id]:not([href]), h3 > a[id]:not([href]), h4 > a[id]:not([href])\r\n{\r\n    top: -130px;\r\n    position: relative;\r\n    display: block;\r\n}\r\n'),
	(2, 'admin', 'Админ-Центр', '', 'body {\r\n    background-image: none !important;\r\n}\r\n\r\n.modal\r\n{\r\n    z-index: 65537;\r\n}\r\n\r\n#filemanager #uploads .uploads-period\r\n{\r\n    margin-right: 20px;\r\n}\r\n\r\n#filemanager #uploads .uploads-period .uploads-period-icon\r\n{\r\n    font-size: 100px;\r\n    cursor: pointer;\r\n}\r\n\r\n#filemanager #uploads .loadmore\r\n{\r\n    clear: both;\r\n    margin-bottom: 25px;\r\n}\r\n\r\n#filemanager #uploads .uploads-period.active, #filemanager #uploads .uploads-terminator\r\n{\r\n    margin-right: 0px;\r\n    clear: both;\r\n}\r\n\r\n.sidebar {\r\n    background-color: #003D66;\r\n    text-align: center;\r\n    color: white;\r\n    height: 2000px;\r\n}\r\n\r\n.sidebar .btn-link\r\n{\r\n    color: white;\r\n}\r\n\r\n.sidebar > p > a > img{\r\n    width: 100px;\r\n    height: auto;\r\n    margin: 10px;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n    nav.navbar {\r\n        display: none;\r\n    }\r\n}\r\n\r\n.panel-admin{\r\n	min-height: 300px;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb > img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\na.robin-thumb > span.upload-killer {\r\n    display: none; \r\n    position: absolute; \r\n    right: 20px; \r\n    top: 20px; \r\n    padding: 15px; \r\n    background-color: rgba(0,0,0,0.25); \r\n    color: white; \r\n    border-radius: 50%;\r\n}\r\n\r\na.robin-thumb:hover > span.upload-killer {\r\n    display: inline-block !important;\r\n}');



INSERT INTO `templates` (`id_template`, `template_code`, `template_html`, `template_description`)
VALUES
    (3,'email','<html>\r\n <head>\r\n  <meta content=\'text/html; charset=utf-8\' http-equiv=\'Content-Type\'>\r\n </head>\r\n <body style=\'background-color: #eeeeee; font-face: sans-serif; padding-top: 40px;\' alink=\'#000099\' link=\'#000099\' text=\'#000000\' vlink=\'#000099\'>\r\n   <table align=\'center\' bgcolor=\'#ffffff\' border=\'0\' cellpadding=\'5\' cellspacing=\'0\' width=\'80%\'>\r\n    <tbody>\r\n     <tr>\r\n       <td style=\'padding-left: 50px; padding-top: 20px; padding-bottom: 20px;\' valign=\'middle\'><h3 style=\'margin: 0px;\'><%title%></h3></td>\r\n       <td style=\'padding-right: 50px; padding-top: 20px; padding-bottom: 20px;\' align=\'right\' valign=\'top\'>\r\n       <img alt=\'<%prefix%>\' src=\'<%logo%>\' border=\'0\' height=\'100px\'></td>\r\n     </tr><tr>\r\n      <td style=\'border-top: 1px solid #cccccc; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' valign=\'top\'>\r\n        <%content%>\r\n      </td>\r\n    </tr><tr>\r\n      <td style=\'background-color: #666666; color: white; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' nowrap=\'nowrap\' valign=\'top\'>\r\n    <span style=\'font-weight: bold; margin-right: 50px;\'><#brand#></span>\r\n   <span style=\'color: silver; margin-right: 50px;\'><#slogan#></span>\r\n    <a style=\'color: silver;\' href=\'http://<%domain%>\'><%domain%></a>\r\n       </td>\r\n     </tr>\r\n   </tbody>\r\n </table>\r\n <br>\r\n <div align=\'center\'><font color=\'#666666\' size=\'-2\'><a style=\'color: #666666;\' href=\'http://<%domain%>/login/recoveryform.php\'>Я не помню пароль</a>.</font></div>\r\n</body></html>','электронное письмо'),
	(7, 'page', '<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n	<link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n    <%styles%>\r\n  </head>\r\n  <body data-spy=\'scroll\' data-target=\'#page-affix\'>\r\n\r\n\r\n<nav class=\"navbar navbar-fixed-top navbar-default\" role=\"navigation\">\r\n	  <div class=\"container-fluid\">\r\n	    <!-- Brand and toggle get grouped for better mobile display -->\r\n	    <div class=\"navbar-header\">\r\n	      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\r\n	        <span class=\"sr-only\">Toggle navigation</span>\r\n	        <span class=\"icon-bar\"></span>\r\n	        <span class=\"icon-bar\"></span>\r\n	        <span class=\"icon-bar\"></span>\r\n	      </button>\r\n	      <a class=\"navbar-brand\" href=\"/\"><#brand#></a>\r\n	    </div>  		\r\n\r\n	    <!-- Collect the nav links, forms, and other content for toggling -->\r\n	    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n	      <ul class=\"nav navbar-nav\">\r\n        		<%main_menu%>\r\n	      </ul>\r\n	      <ul class=\"nav navbar-nav navbar-right\">\r\n		    <li class=\'dropdown\'>\r\n			    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">\r\n				    <%user_link%> <span class=\"caret\"></span>\r\n			    </a>\r\n			    <ul class=\"dropdown-menu\" role=\"menu\">\r\n				    <%user_menu%>\r\n			    </ul>\r\n		    </li>\r\n		    <%lang_selector%>\r\n	      </ul>\r\n	      <%search%>\r\n	    </div><!-- /.navbar-collapse -->\r\n\r\n	  </div><!-- /.container-fluid -->	    \r\n	</nav>\r\n\r\n<div class=\'container\'>\r\n<h1><%title_long%></h1>\r\n<%contains%>\r\n</div>\r\n\r\n    <%scripts%>\r\n  </body>\r\n</html>', 'страница'),
	(8, 'page_backup', '<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n	<link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n    <%styles%>\r\n  </head>\r\n  <body data-spy=\'scroll\' data-target=\'#page-affix\'>\r\n\r\n\r\n<nav class=\"navbar navbar-fixed-top navbar-default\" role=\"navigation\">\r\n	  <div class=\"container-fluid\">\r\n	    <!-- Brand and toggle get grouped for better mobile display -->\r\n	    <div class=\"navbar-header\">\r\n	      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\r\n	        <span class=\"sr-only\">Toggle navigation</span>\r\n	        <span class=\"icon-bar\"></span>\r\n	        <span class=\"icon-bar\"></span>\r\n	        <span class=\"icon-bar\"></span>\r\n	      </button>\r\n	      <a class=\"navbar-brand\" href=\"/\"><#brand#></a>\r\n	    </div>  		\r\n\r\n	    <!-- Collect the nav links, forms, and other content for toggling -->\r\n	    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n	      <ul class=\"nav navbar-nav\">\r\n        		<%main_menu%>\r\n	      </ul>\r\n	      <ul class=\"nav navbar-nav navbar-right\">\r\n		    <li class=\'dropdown\'>\r\n			    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">\r\n				    <%user_link%> <span class=\"caret\"></span>\r\n			    </a>\r\n			    <ul class=\"dropdown-menu\" role=\"menu\">\r\n				    <%user_menu%>\r\n			    </ul>\r\n		    </li>\r\n		    <%lang_selector%>\r\n	      </ul>\r\n	      <%search%>\r\n	    </div><!-- /.navbar-collapse -->\r\n\r\n	  </div><!-- /.container-fluid -->	    \r\n	</nav>\r\n\r\n<div class=\'container\'>\r\n<h1><%title_long%></h1>\r\n<%contains%>\r\n</div>\r\n\r\n    <%scripts%>\r\n  </body>\r\n</html>', 'страница'),
	(11, 'admin', '<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n\r\n    <%styles%>\r\n\r\n</head>\r\n<body>\r\n<nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"/engine-api/admin/cp\">Engine <small><%version%></small></a>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                <%lang_selector_admin%>\r\n                <%admin_menu%>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"hidden-xs col-sm-3 col-md-3 sidebar\">\r\n            <%lang_selector_button_admin%>\r\n            <p><a href=\'/engine-api/admin/cp\'><img src=\"/vendor/robintail/engine-api/images/engine/logo_transparent_white_600.png\"></a></p>\r\n            <div class=\"list-group\">\r\n                <%admin_menu_list%>\r\n            </div>\r\n            <p>Engine <%version%></p>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-9 col-md-9\">\r\n            <p class=\'hidden-xs\'> </p>\r\n            <%contains%>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<%scripts%>\r\n</body>\r\n</html>', 'Админ-центр');


CREATE TABLE `template_styles` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `style_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `template_id` (`template_id`,`style_id`),
  KEY `tostyle` (`style_id`),
  CONSTRAINT `tostyle` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id_style`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `totemplate2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `template_styles` (`rid`, `template_id`, `style_id`)
VALUES
	(13, 7, 1),
	(12, 11, 2);


INSERT INTO `pages` (`id_page`, `page_url`, `template_id`)
VALUES
	(1,'index',7);


INSERT INTO `page_trans` (`id_trans`, `page_id`, `lang_id`, `page_title_short`, `page_title_long`, `page_html`, `page_keywords`, `page_description`, `page_affix`)
VALUES
	(1, 1, 1, 'Engine', 'Здравствуй, Мир :)', '<p><img src=\"/vendor/robintail/engine-api/images/engine/logo_white_600.png\" alt=\"\" width=\"600\" height=\"600\" /></p>\r\n<h1 id=\"engine\">ENGINE</h1>\r\n<p>Спасибо за ваш интерес к этому проекту.</p>\r\n<h2 id=\"requirements\">Требования</h2>\r\n<ul>\r\n<li>PHP >= 5.4 (включая gd, iconv и PDO)</li>\r\n<li>MySQL</li>\r\n</ul>\r\n<h2 id=\"dependencies\">Зависимости</h2>\r\n<p>Репозитории:</p>\r\n<ul>\r\n<li>Bootstrap (<em>/vendor/twbs/bootstrap</em>)</li>\r\n<li>jQuery (<em>/vendor/components/jquery</em>)</li>\r\n<li>tinyMCE (<em>/vendor/tinymce/tinymce</em>)</li>\r\n<li>plUpload (<em>/vendor/moxiecode/plupload</em>)</li>\r\n<li>phpUnit (<em>/vendor/phpunit/phpunit</em>)</li>\r\n</ul>\r\n<p>Пакеты:</p>\r\n<ul>\r\n<li>Lytebox (<em>/vendor/markusfhay/lytebox</em>)</li>\r\n<li>Ace (<em>/vendor/ajaxorg/ace-builds</em>)</li>\r\n</ul>\r\n<h2 id=\"installation\">Установка</h2>\r\n<p>Через SSH соединение просто выполните эти команды в папке сайта:</p>\r\n<pre><code><span class=\"comment\">#!bash</span>\r\n<span class=\"title\">git</span> clone --<span class=\"built_in\">no</span>-checkout git<span class=\"variable\">@bitbucket</span>.org:robintail/engine.git .\r\ngit checkout master\r\n</code></pre>\r\n<p>После этого выполните эту команду для переименования файла настроек</p>\r\n<pre><code><span class=\"comment\">#!bash</span>\r\n<span class=\"title\">mv</span> kernel/DB/Settings_default.php kernel/DB/Settings.php\r\n</code></pre>\r\n<p>Затем отредактируйте этот файл, например через nano:</p>\r\n<pre><code><span class=\"comment\">#!bash</span>\r\n<span class=\"title\">nano</span> kernel/DB/Settings.php\r\n</code></pre>\r\n<p>Этот файл содержит следующий код:</p>\r\n<pre><code>#!php<span class=\"pi\">\r\nnamespace kernel\\DB;\r\n\r\n/**\r\n * Class Settings\r\n * @package kernel\\DB\r\n */\r\nclass Settings\r\n{\r\n    /**\r\n     * Specify your MySQL server host\r\n     * For localhost please use 127.0.0.1\r\n     * @var string\r\n     */\r\n    protected static $host = \'127.0.0.1\';\r\n\r\n    /**\r\n     * Specify your MySQL server port\r\n     * Default is 3306\r\n     * @var string\r\n     */\r\n    protected static $port = \'3306\';\r\n\r\n    /**\r\n     * Specify your MySQL user name\r\n     * @var string\r\n     */\r\n    protected static $user = \'root\';\r\n\r\n    /**\r\n     * Specify your MySQL user password\r\n     * @var string\r\n     */\r\n    protected static $pass = \'root\';\r\n\r\n    /**\r\n     * Specify your MySQL database name\r\n     * @var string\r\n     */\r\n    protected static $base = \'engine\';\r\n\r\n    /**\r\n     * If specified database does not exist set this property to false.\r\n     * Installer will create this database.\r\n     * @var bool\r\n     */\r\n    protected static $baseIsExists = true;\r\n}\r\n</span></code></pre>\r\n<p>Отредактируйте эти параметры, закройте с помощью Ctrl+X, согласитесь сохранить изменения с помощью Y и ENTER. Выполните CLI скрипт установки этой командой:</p>\r\n<pre><code>#!bash\r\nphp install.php <span class=\"tag\"><<span class=\"title\">adminName</span>></span> <span class=\"tag\"><<span class=\"title\">adminEmail</span>></span> <span class=\"tag\"><<span class=\"title\">adminPassword</span>></span>\r\n</code></pre>\r\n<h3 id=\"tune\">Настройка</h3>\r\n<p>Откройте сайт в браузере. Откройте выпадающее меню и выполните вход. Войдите в Админ Центр. Выберите Системные настройки и отредактируйте их.</p>\r\n<h2 id=\"runtime-error-reporting\">Ошибки во время выполнения</h2>\r\n<p>Ошибки во время выполнения не показываются посетителям по соображениям безопасности. Информация о последней ошибке находится в файле \"/tmp/last_error.txt\". Эта информация отправляется на почту каждые 15 минут. Engine использует настройку \"email_tech\", которая во время установки настраивается экивалентной адресу адмнистратора, но вы можете изменить её значение.</p>\r\n<h2 id=\"updates\">Обновления</h2>\r\n<p>Вы можете легко обновить Engine используя CLI скрипт самообновления через SSH. Скрипт автоматически обновит ваш код, используя git, создаст резервную копию базы данных в файл, выполнит SQL скрипты миграции и тесты. В случае ошибки он автоматически восстановит базу данных из файла резервной копии, а также восстановит ваш код к коммиту до начала обновления.</p>\r\n<pre><code>#!bash\r\nphp selfupdate.php <span class=\"tag\"><<span class=\"title\">adminEmail</span>></span> <span class=\"tag\"><<span class=\"title\">adminPassword</span>></span>\r\n</code></pre>\r\n<h2 id=\"tests\">Тесты</h2>\r\n<p>Тесты уже сконфигурированы в файле \'phpunit.xml\' и запускаются в процессе обновления.</p>\r\n<h2 id=\"tests\">Контакты</h2>\r\n<p>Пишите мне по адресу: robin_tail@me.com</p>\r\n<p> </p>', '', '', ''),
	(2, 1, 2, 'Engine', 'Hello World :)', '<p><img src=\"/vendor/robintail/engine-api/images/engine/logo_white_600.png\" alt=\"\" width=\"600\" height=\"600\" /></p>\r\n<h1 id=\"engine\">ENGINE</h1>\r\n<p>Thank you for your interest to this website cosy engine.</p>\r\n<h2 id=\"requirements\">Requirements</h2>\r\n<ul>\r\n<li>PHP >= 5.4 (with gd, iconv and PDO)</li>\r\n<li>MySQL</li>\r\n</ul>\r\n<h2 id=\"dependencies\">Dependencies</h2>\r\n<p>Third-part repositories:</p>\r\n<ul>\r\n<li>Bootstrap (<em>/vendor/twbs/bootstrap</em>)</li>\r\n<li>jQuery (<em>/vendor/components/jquery</em>)</li>\r\n<li>tinyMCE (<em>/vendor/tinymce/tinymce</em>)</li>\r\n<li>plUpload (<em>/vendor/moxiecode/plupload</em>)</li>\r\n<li>phpUnit (<em>/vendor/phpunit/phpunit</em>)</li>\r\n</ul>\r\n<p>Third-part packages:</p>\r\n<ul>\r\n<li>Lytebox (<em>/vendor/markusfhay/lytebox</em>)</li>\r\n<li>Ace (<em>/vendor/ajaxorg/ace-builds</em>)</li>\r\n</ul>\r\n<h2 id=\"installation\">Installation</h2>\r\n<p>Via SSH connection simply execute this commands in the empty website directory:</p>\r\n<pre><code><span class=\"comment\">#!bash</span>\r\n<span class=\"title\">git</span> clone --<span class=\"built_in\">no</span>-checkout git<span class=\"variable\">@bitbucket</span>.org:robintail/engine.git .\r\ngit checkout master\r\n</code></pre>\r\n<p>After that execute this command to rename default settings file:</p>\r\n<pre><code><span class=\"comment\">#!bash</span>\r\n<span class=\"title\">mv</span> kernel/DB/Settings_default.php kernel/DB/Settings.php\r\n</code></pre>\r\n<p>Then edit this file, for example with nano:</p>\r\n<pre><code><span class=\"comment\">#!bash</span>\r\n<span class=\"title\">nano</span> kernel/DB/Settings.php\r\n</code></pre>\r\n<p>This file contains code like this:</p>\r\n<pre><code>#!php<span class=\"pi\">\r\nnamespace kernel\\DB;\r\n\r\n/**\r\n * Class Settings\r\n * @package kernel\\DB\r\n */\r\nclass Settings\r\n{\r\n    /**\r\n     * Specify your MySQL server host\r\n     * For localhost please use 127.0.0.1\r\n     * @var string\r\n     */\r\n    protected static $host = \'127.0.0.1\';\r\n\r\n    /**\r\n     * Specify your MySQL server port\r\n     * Default is 3306\r\n     * @var string\r\n     */\r\n    protected static $port = \'3306\';\r\n\r\n    /**\r\n     * Specify your MySQL user name\r\n     * @var string\r\n     */\r\n    protected static $user = \'root\';\r\n\r\n    /**\r\n     * Specify your MySQL user password\r\n     * @var string\r\n     */\r\n    protected static $pass = \'root\';\r\n\r\n    /**\r\n     * Specify your MySQL database name\r\n     * @var string\r\n     */\r\n    protected static $base = \'engine\';\r\n\r\n    /**\r\n     * If specified database does not exist set this property to false.\r\n     * Installer will create this database.\r\n     * @var bool\r\n     */\r\n    protected static $baseIsExists = true;\r\n}\r\n</span></code></pre>\r\n<p>Edit these options, close with Ctrl+X, agree to save changes with Y and ENTER. Execute installer CLI script with this command:</p>\r\n<pre><code>#!bash\r\nphp install.php <span class=\"tag\"><<span class=\"title\">adminName</span>></span> <span class=\"tag\"><<span class=\"title\">adminEmail</span>></span> <span class=\"tag\"><<span class=\"title\">adminPassword</span>></span>\r\n</code></pre>\r\n<h3 id=\"tune\">Tune</h3>\r\n<p>In browser follow the website root. Click on the dropdown link in the main menu and log in. Go to Admin CP. Go to System Settings and edit their values.</p>\r\n<h2 id=\"runtime-error-reporting\">Runtime error reporting</h2>\r\n<p>By default all errors are not shown to your website users for security reasons. Last error information can be found in the file \"/tmp/last_error.txt\". Also, this information will be send to your email every 15 minutes. Engine uses the system setting \"email_tech\", which is automatically set up equals to your admin account email, provided while setup process, but you can change it.</p>\r\n<h2 id=\"updates\">Updates</h2>\r\n<p>You can easily update your Engine by using self-update CLI script via SSH. Self-update script will automatically update your code using git, backup your database to snapshot file, execute migration SQL scripts and tests. In case of failure it will automatically restore your database from the snapshot file and restore your code to commit before update started.</p>\r\n<pre><code>#!bash\r\nphp selfupdate.php <span class=\"tag\"><<span class=\"title\">adminEmail</span>></span> <span class=\"tag\"><<span class=\"title\">adminPassword</span>></span>\r\n</code></pre>\r\n<h2 id=\"tests\">Tests</h2>\r\n<h2 id=\"installation\"><span style=\"font-size: 11px;\">Tests are already configured in file \'phpunit.xml\' and can be executed with update process.</span></h2>\r\n<h2 id=\"tests\">Contact</h2>\r\n<p>Email me to: robin_tail@me.com</p>\r\n<p> </p>', '', '', '');


CREATE TABLE `settings_types` (
  `id_type` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_code` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_type`),
  UNIQUE KEY `type_code` (`type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `settings_types` (`id_type`, `type_code`)
VALUES
	(1, 'string'),
	(2, 'int'),
	(3, 'bool'),
	(4, 'float'),
	(5, 'html');


CREATE TABLE `settings` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `skey` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `key` (`skey`),
  KEY `totype` (`type_id`),
  CONSTRAINT `totype` FOREIGN KEY (`type_id`) REFERENCES `settings_types` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `settings` (`rid`, `skey`, `value`, `description`, `is_hidden`, `type_id`)
VALUES
	(1, 'thumb_size', '250', 'Размер стороны эскиза', 0, 2),
	(2, 'email_from', 'no@no.no', 'От кого отправлять почту', 0, 1),
	(3, 'index_page', 'index', 'URL главной страницы', 0, 1),
	(4, 'site_image', '', 'Стандартная картинка', 0, 1),
	(5, 'captcha_font_file', 'vendor/robintail/engine-api/images/captcha/digits2.png', 'путь к картинке с цифрами', 0, 1),
	(7, 'last_usage_report', '1437579168', 'последнее уведомление об использовании', 1, 2),
	(8, 'upload_dir', 'images/upload', 'каталог для загрузки изображений', 0, 1),
	(9, 'menu_depth', '&nbsp;&nbsp;&nbsp;', 'отображение вложенности меню более второго уровня', 0, 5),
	(10, 'uploads_show_per_cycle', '20', 'сколько подгружать эскизов за раз', 0, 2),
	(11, 'email_prefix', 'NN', 'префикс писем', 0, 1),
	(12, 'email_logo', 'images/logo.png', 'лого в письме', 0, 1),
	(13, 'email_tech', 'no@no.no', 'адрес технического администратора', 0, 1),
	(14, 'anti_brute_tries', '3', 'число попыток ввода пароля до временной блокировки', 0, 2),
	(15, 'anti_brute_time', '600', 'время блокировки аккаунта при неудачных попытках ввода пароля (в секундах)', 0, 2),
	(16, 'use_buttons_in_menu', '0', 'использовать кнопки в меню', 0, 3),
	(17, 'maintenance_mode', '0', 'закрытие сайта на обслуживание', 0, 3),
	(18, 'thumb_size_filemanager', '200', 'Размер стороны эскиза в менеджере загруженных файлов', 1, 2),
	(21, 'use_raw_page_editor', '0', 'Использовать редактор страниц без визуализации', 0, 3);







CREATE TABLE `uploads` (
  `id_upload` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_upload`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `id_user` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_name` varchar(255) NOT NULL DEFAULT '',
  `tries` int(11) NOT NULL DEFAULT '0',
  `date_last_try` int(11) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `lang_code` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `migration` (
  `id_migration` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `migration_timestamp` int(11) NOT NULL,
  `executed_file` varchar(255) NOT NULL DEFAULT '',
  `executed_at` int(11) NOT NULL,
  `snapshot_file` varchar(255) NOT NULL DEFAULT '',
  `commit` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `migration` (`id_migration`, `migration_timestamp`, `executed_file`, `executed_at`, `snapshot_file`, `commit`)
VALUES
	(1, 20150708, '20150708.sql', 0, '', ''),
	(2, 20150714, '20150714.sql', 0, '', ''),
	(3, 20150716, '20150716.sql', 0, '', ''),
	(4, 20150720, '20150720.sql', 0, '', ''),
	(5, 20150721, '20150721.sql', 0, '', ''),
	(6, 20150722, '20150722.sql', 0, '', ''),
	(7, 20150808, '20150808.sql', 0, '', ''),
	(8, 20150809, '20150809.sql', 0, '', ''),
	(9, 20150817, '20150817.sql', 0, '', ''),
	(10, 20150818, '20150818.sql', 0, '', ''),
	(11, 20150819, '20150819.sql', 0, '', ''),
	(12, 20150820, '20150820.sql', 0, '', ''),
	(13, 20150822, '20150822.sql', 0, '', ''),
	(14, 20150826, '20150826.sql', 0, '', ''),
	(15, 20150918, '20150918.sql', 0, '', ''),
	(16, 20150920, '20150920.sql', 0, '', ''),
	(17, 20150926, '20150926.sql', 0, '', ''),
	(18, 20150927, '20150927.sql', 0, '', ''),
	(19, 20150929, '20150929.sql', 0, '', ''),
	(20, 20151003, '20151003.sql', 0, '', ''),
	(21, 20151004, '20151004.sql', 0, '', ''),
	(22, 20151010, '20151010.sql', 0, '', ''),
	(23, 20151110, '20151110.sql', 0, '', ''),
	(24, 20151203, '20151203.sql', 0, '', '');




HTM;

        $this->pdo->query("SET NAMES utf8;") or die("Can not set charset");
        if (!parent::$baseIsExists) {
            echo "Creating database... ";
            $this->pdo->query("CREATE DATABASE ".parent::$base.";") or die("Can not create database");
            echo CLI::get()->getColoredString("OK","green")."\r\n";
        }
        $this->pdo->query("USE ".parent::$base.";") or die("Can not use database");

        $queries = preg_split("/;$/m", $database);
        echo "Executing queries [".count($queries)."]... ";
        foreach($queries as $q) {
            if (trim($q)) $this->pdo->query($q) or die("\r\nError in query. ".$q."\r\nError: ".$this->pdo->errorInfo()[2]);
            echo "*";
        }
        echo " ";
        echo CLI::get()->getColoredString("OK","green")."\r\n";

    }




    private function entryFile()
    {
        echo "Creating entry point file 'index.php'... ";
        $f = fopen("index.php", "w") or die("can not create file");
        $data = "<?

// autoload composer packages
require(\"vendor/autoload.php\");

// starter
RobinTail\\EngineAPI\\Starter::get()
    // database connection setup
    ->setDatabaseConnection('". parent::$host ."', '". parent::$port ."', '". parent::$base ."', '". parent::$user ."', '". parent::$pass ."')
    // standard page handler for root page requests
    ->addRoutePage()
    // standard search handler
    ->addRouteSearch()
    // start Engine ^_^
    ->start();
";
        fwrite($f, $data);
        fclose($f);

        echo CLI::get()->getColoredString("OK","green")."\r\n";

        echo "Creating migration launcher file '_migrator.php'... ";
        $f = fopen("index.php", "w") or die("can not create file");
        $data = "<?

// autoload composer packages
require(\"vendor/autoload.php\");

// use starter to set up connection
RobinTail\\EngineAPI\\Starter::get()
    // database connection setup
    ->setDatabaseConnection('". parent::$host ."', '". parent::$port ."', '". parent::$base ."', '". parent::$user ."', '". parent::$pass ."');
RobinTail\\EngineAPI\\Migration::get()
    ->proceed();
";
        fwrite($f, $data);
        fclose($f);

        echo CLI::get()->getColoredString("OK","green")."\r\n";
    }





    private function directories()
    {
        echo "Creating directories... ";
        mkdir(Path::resolve("images"), 0755) or die('can not create images dir');
        mkdir(Path::resolve("images/upload"), 0777) or die('can not create images/upload dir');
        mkdir(Path::resolve("tmp"), 0777) or die('can not create tmp dir');
        echo CLI::get()->getColoredString("OK","green")."\r\n";
    }






    private function createAdmin()
    {
        echo "Creating admin account... ";
        User::get()->write(
            User\Item::create()
                ->writeName($this->adminName)
                ->writeEmail($this->adminEmail)
                ->writePassword($this->adminPassword)
                ->writeIsAdmin(true)
        );
        echo CLI::get()->getColoredString("OK","green")."\r\n";
        echo "Writing lock file... ";
        Settings::get()->write('email_tech', $this->adminEmail);
        @fopen("installed", 'w') or die('Can not create lock file');
        echo CLI::get()->getColoredString("OK","green")."\r\n";
    }


}
