<?php

namespace RobinTail\EngineAPI\Security\Captcha;


interface methods
{
    public static function check($rid, $code);
    public static function set();
    public static function get($rid);
    public static function send();
}
