<?php

namespace RobinTail\EngineAPI\Security;


use RobinTail\EngineAPI\DB;
use RobinTail\EngineAPI\Path;
use RobinTail\EngineAPI\Settings;
use RobinTail\EngineAPI\Input;
use RobinTail\EngineAPI\GDlib;

/**
 * Class Captcha
 * @package RobinTail\EngineAPI\Security
 */
class Captcha implements Captcha\methods
{
    /**
     * Symbols in captcha quantity
     * @var int
     */
    public static $symbols = 6;
    /**
     * Captcha image width (px)
     * @var int
     */
    public static $width=200;
    /**
     * Captcha image height (px)
     * @var int
     */
    public static $height=80;
    /**
     * Digits images array
     * @var bool|array
     */
    public static $digits=false; // will be loaded
    /**
     * Digits height array
     * @var bool|int[]
     */
    public static $digit_height=false; // will be loaded

    /**
     * Loads digits images from font image file
     */
    public static function loadDigits()
    {
        self::$digits = array();
        $file = @imagecreatefrompng(Path::resolve(Settings::get()->readValue('captcha_font_file')));
        if (!$file) die("captcha: could not load font ".Path::resolve(Settings::get()->readValue('captcha_font_file')));
        $lastx=-1;
        for($x=0;$x<imagesx($file);$x++)
        {
            $color_array = imagecolorsforindex($file, imagecolorat($file,$x,0) );
            if ( (($color_array['red']==255) && ($color_array['green']==0) && ($color_array['blue']==0)) || ($x==imagesx($file)-1) )
            {
                $tmp = imagecreate($x-$lastx-1,imagesy($file));
                imagecopy($tmp,$file,0,0,$lastx+1,0,$x-$lastx-1,imagesy($file));
                imagecolortransparent($tmp, imagecolorat($tmp,0,0));
                self::$digits[] = $tmp;
                $lastx=$x;
            }
        }
        self::$digit_height = imagesy($file);
        imagedestroy($file);
    }


    /**
     * Get captcha secret code by id
     * @param int $rid
     * @return bool|string
     * @throws \Exception
     */
    public static function get($rid)
    {
        DB::get()->prepare("SELECT code FROM captcha WHERE id_captcha=?")
                        ->bind(1, $rid)
                        ->execute();
        if ($row=DB::get()->fetchRow())
        {
            return $row['code'];
        } else {
            return false;
        }
    }

    /**
     * Verify capctha secret code and id
     * @param int $rid
     * @param string $code
     * @return bool
     */
    public static function check($rid, $code)
    {
        if (!($rid  && $code)) return false;
        self::kill(); // kill old
        $real_code = self::get($rid);
        if ($real_code)
        {
            if ($real_code == $code)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates new captcha
     * @return bool|int
     * @throws \Exception
     */
    public static function set()
    {
        $code = "";
        for ($i=0;$i<self::$symbols;$i++)
        {
            $code .= mt_rand(0,9);
        }
        DB::get()->prepare("INSERT INTO captcha (date_create_unix,code)
                                    VALUES (?,?)")
                        ->bind(1, time())
                        ->bind(2, $code)
                        ->execute();
        DB::get()->prepare("SELECT id_captcha
                                    FROM captcha
                                    WHERE code=?
                                    ORDER BY ID_Captcha DESC
                                    LIMIT 0,1")
                        ->bind(1, $code)
                        ->execute();
        if ($row=DB::get()->fetchRow())
        {
            return $row['id_captcha'];
        } else {
            return false;
        }
    }

    /**
     * Removes old captcha records from database
     * @return bool
     * @throws \Exception
     */
    private static function kill()
    {
        $lifetime = 60*30; // 30 minutes
        $deadline = time() - $lifetime;
        DB::get()->prepare("DELETE FROM captcha
                                    WHERE date_create_unix <= ?")
                        ->bind(1, $deadline)
                        ->execute();
        return true;
    }

    /**
     * Sends captcha image to browser using GDLib
     */
    public static function send()
    {
        $rid = Input::get()->readInt('rid');
        if (!$rid) die("captcha: no rid key given");
        $code = self::get($rid);
        if ($code)
            GDlib::captcha(preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY));
        else
            die("captcha: requested key was not found in database");
    }


}
