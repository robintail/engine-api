<?

namespace RobinTail\EngineAPI\Security;

interface methods
{
	public static function getRecoveryHash($cryptedPassword);
	public static function cryptPassword($pwd);
	public static function getIP();
	public static function generatePassword();

}

?>