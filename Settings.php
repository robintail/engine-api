<?

namespace RobinTail\EngineAPI;

/**
 * Class Settings
 * @package RobinTail\EngineAPI
 */
class Settings
{
    /**
     * @var Settings
     */
    private static $instance;
    /**
     * @var Settings\Item[]
     */
    private $settings;

    /**
     * @return Settings
     */
    public static function get()
	{
		if (!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

    /**
     * @throws \Exception
     */
    private function __construct()
	{
		DB::get()->prepare("SELECT settings.*, settings_types.type_code
                            FROM settings
                            INNER JOIN settings_types ON settings.type_id=settings_types.id_type
                            ORDER BY skey")
                ->execute();
		$result=DB::get()->result();
		foreach($result as $row)
		{
			$this->settings[$row['skey']] = Settings\Item::create()
                                            ->writeKey($row['skey'])
                                            ->writeTypeCode($row['type_code'])
                                            ->writeValue($row['value'])
                                            ->writeDescription($row['description'])
                                            ->writeIsHidden($row['is_hidden']);
		}
	}

    /**
     * @param string $key
     * @return Settings\Item
     */
    public function readByKey($key)
    {
        return $this->settings[$key];
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function readValue($key)
	{
		return $this->settings[$key]->readValue();
	}

    /**
     * @return Settings\Item[]
     */
    public function readAll()
    {
        return $this->settings;
    }

    /**
     * @param string $key
     * @param string $v
     * @throws \Exception
     */
    public function write($key, $v)
	{
        // issue 75
        // $v = ($this->readByKey($key)->readTypeCode() == 'html' ? html_entity_decode($v, ENT_QUOTES, 'utf-8') : $v);
        // save
        DB::get()->prepare("UPDATE settings
                            SET value=:value
                            WHERE skey=:skey")
                ->bind(':value', $v)
                ->bind(':skey', $key)
                ->execute();
	}
	
}

?>