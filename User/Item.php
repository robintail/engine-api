<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 29.07.15
 * Time: 20:55
 */

namespace RobinTail\EngineAPI\User;
use RobinTail\EngineAPI\Security;

/**
 * Class Item
 * @package RobinTail\EngineAPI\User
 */
class Item {

    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $password;
    /**
     * @var string
     */
    protected $email;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var int
     */
    protected $tries;
    /**
     * @var int
     */
    protected $dateLastTry;
    /**
     * @var bool
     */
    protected $isAdmin;
    /**
     * @var string
     */
    protected $langCode;


    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function readDateLastTry()
    {
        return $this->dateLastTry;
    }

    /**
     * @param int $dateLastTry
     * @return $this
     */
    public function writeDateLastTry($dateLastTry)
    {
        $this->dateLastTry = (int) $dateLastTry;
        return $this;
    }

    /**
     * @return string
     */
    public function readEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function writeEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param boolean $isAdmin
     * @return $this
     */
    public function writeIsAdmin($isAdmin)
    {
        $this->isAdmin = (bool) $isAdmin;
        return $this;
    }

    /**
     * @return string
     */
    public function readLangCode()
    {
        return $this->langCode;
    }

    /**
     * @param string $langCode
     * @return $this
     */
    public function writeLangCode($langCode)
    {
        $this->langCode = $langCode;
        return $this;
    }

    /**
     * @return string
     */
    public function readName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function writeName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function readPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function writePassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function readTries()
    {
        return $this->tries;
    }

    /**
     * @param int $tries
     * @return $this
     */
    public function writeTries($tries)
    {
        $this->tries = (int) $tries;
        return $this;
    }




} 