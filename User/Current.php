<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 29.07.15
 * Time: 21:28
 */

namespace RobinTail\EngineAPI\User;
use RobinTail\EngineAPI\DB;
use RobinTail\EngineAPI\Security;
use RobinTail\EngineAPI\Lang;
use RobinTail\EngineAPI\User;

/**
 * Class Current
 * @package RobinTail\EngineAPI\User
 */
class Current extends Item {
    /**
     * @var Current
     */
    private static $instance;
    // использовать дополнительные проверки IP и браузера при автологине через куки
    // эта функция иногда может создавать проблемы для пользователей с динамическим адресом
    // или идентификационной строкой браузера
    /**
     * @var bool
     */
    private $useIPAndBrowserCheck = false;
    /**
     * @var string
     */
    private $errorMessage = "";


    /**
     * @return Current
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Alias to get()
     * From parent class User\Item
     * @return Current
     */
    public static function create()
    {
        return self::get();
    }

    /**
     * Try to load current user from cookie data
     */
    private function __construct()
    {
        // try to find and verity user by cookie
        if ($tryUser = $this->readUserByCookie())
        {
            if (!$this->isValidBrowserAndIp())
            {
                $this->writeErrorMessage("Изменился IP или браузер (cookie)");
                return;
            }
            // clone properties to current object
            foreach(get_object_vars($tryUser) as $propertyName => $propertyValue)
            {
                $this->$propertyName = $propertyValue;
            }
        } else {
            $this->writeErrorMessage( "Логин и пароль неверны (cookie)");
            return;
        }
    }


    /**
     * Проверка пользователя по кукам, возвращает объект пользователя или false
     * @return User\Item|false
     * @throws \Exception
     */
    private function readUserByCookie()
    {
        if ($_COOKIE['log'] && $_COOKIE['pwd']) {
            $tryUser = User::get()->readByEmail($_COOKIE['log']);
            if ($tryUser->readId()) {
                // user item or false when password does not match
                return (($_COOKIE['pwd'] == $tryUser->readPassword()) ? $tryUser : false);
            } else { // user not found
                return false;
            }
        } else { // no cookie data
            return false;
        }
    }

    /**
     * Checks (if enabled) is the browser and ip address are valid to cookie saved hashes
     * @return bool
     */
    private function isValidBrowserAndIp()
    {
        if (!$this->useIPAndBrowserCheck) return true;
        if (md5(Security::getIP()) != $_COOKIE['ip'] || md5($_SERVER['HTTP_USER_AGENT']) != $_COOKIE['browser'])
            return false;
        return true;
    }


    /**
     * @return string
     */
    public function readErrorMessage()
    {
        return $this->errorMessage;
    }


    /**
     * @param string $msg
     */
    private function writeErrorMessage($msg)
    {
        $this->errorMessage = $msg;
    }

    /**
     * Updates information in database about current user's language
     * @param string $code
     * @throws \Exception
     */
    public function updateLangCode($code)
    {
        if ($this->readId())
            if ($this->readLangCode() != $code)
                DB::get()->prepare("UPDATE users
                                    SET lang_code=:code
                                    WHERE id_user=:id")
                    ->bind(':code', $code)
                    ->bind(':id', $this->readId())
                    ->execute();
    }



} 