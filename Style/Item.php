<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 19.07.15
 * Time: 10:20
 */

namespace RobinTail\EngineAPI\Style;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Style
 */
class Item {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $link;
    /**
     * @var string
     */
    private $text;


    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }


    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }


    /**
     * @return string
     */
    public function readCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function writeCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function readDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function writeDescription($description)
    {
        $this->description = $description;
        return $this;
    }


    /**
     * @return string
     */
    public function readLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return $this
     */
    public function writeLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function readText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function readTextForEditor()
    {
        return htmlentities( $this->readText() , ENT_NOQUOTES , 'utf-8');
    }


    /**
     * @param string $text
     * @return $this
     */
    public function writeText($text)
    {
        // issue 75
        // $this->text = html_entity_decode($text, ENT_QUOTES, 'utf-8');
        $this->text = $text;
        return $this;
    }

} 