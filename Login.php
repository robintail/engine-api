<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 16.11.15
 * Time: 17:55
 */

namespace RobinTail\EngineAPI;


/**
 * Class Login
 * @package RobinTail\EngineAPI
 */
class Login {


    /**
     * Additional fields to forward while login and register process
     * @var string[]
     */
    private static $forwardFields = array();

    /**
     * @var bool|callable
     */
    private static $registerHandler = false;

    /**
     * Add an additional field to forward while login and register process
     * @param string $fieldName
     */
    public static function addForwardField($fieldName)
    {
        array_push(self::$forwardFields, $fieldName);
    }

    /**
     * Add a handler function for user register action
     * @param $callable
     */
    public static function addRegisterHandler($callable)
    {
        if (is_callable($callable)) self::$registerHandler = $callable;
    }


    /**
     * Logs in user with input email (log) and password (pwd)
     */
    public static function login()
    {
        $isValid = User::get()->validate(Input::get()->read('log'), Input::get()->read('pwd'));
        if (!$isValid) {
            Output::get()->error("<#auth-error#>: ".User::get()->readErrorMessage());
        }
        if (Input::get()->read('ref'))
        {
            $redirect = Input::get()->read('ref').
                (strstr(Input::get()->read('ref'),'?') ? "" : "?");
        } elseif ($_SERVER['HTTP_REFERER'])
        {
            $redirect = $_SERVER['HTTP_REFERER'].
                (strstr($_SERVER['HTTP_REFERER'],'?') ? "" : "?");
        } else {
            $redirect = '/?';
        }
        foreach(self::$forwardFields as $fld) // forward additional fields
        {
            $redirect .= '&'.(Input::get()->exist($fld) ? $fld.Input::get()->read($fld) : "");
        }
        Output::get()->redirect($redirect);
    }


    /**
     * Logs out user
     */
    public static function logout()
    {
        User::get()->flushCookie();
        Output::get()->redirect("/");
    }

    /**
     * Register new user with input name, email and password
     */
    public static function register()
    {
        $newid = User::get()->write(
            User\Item::create()
                ->writeName(Input::get()->read('name'))
                ->writeEmail(Input::get()->read('email'))
                ->writePassword(Input::get()->read('password'))
        );

        if ($newid)
        {
            if (is_callable(self::$registerHandler))
            {
                call_user_func(self::$registerHandler, $newid);
            }
            $mail = "<#register-email#>";
            $resultsend = Email::send(Input::get()->read('email'), $mail, "<#register#>");
            $html = "<form name='autologin' action='/engine-api/login/login' method='post'>
					    <input type='hidden' name='log' value='".trim(Input::get()->read('email'))."'>
					    <input type='hidden' name='pwd' value='".trim(Input::get()->read('password'))."'>
					    <input type='hidden' name='ref' value='".Input::get()->read('ref')."'>";
            foreach(self::$forwardFields as $fld)
            { // forward additional fields
                $html .= "<input type='hidden' name='".$fld."' value='".Input::get()->read($fld)."'>";
            }
            $html .= "  <center><button class='btn btn-primary btn-lg' type='submit'><#login#></button></center>
				    </form>";
            $sc = "<script>$(function() { document.forms['autologin'].submit(); });</script>";
            Output\Scripts::get()->addScript($sc);
            Output::get()->writeOption('title_short', 'Autologin')
                ->writeOption('contains', $html)
                ->render();
        } else {
            Output::get()->error("<#register-error#>: ".User::get()->readErrorMessage());
        }
    }

    /**
     * Shows the password recovery form
     */
    public static function recoveryForm()
    {
        $captcha = Output\Snippets::captcha(Security\Captcha::set());
        $out = <<<HTM
            <p><#recovery-start-info#></p>
            <br><br>
            <div class='container-fluid'>
                <div class='row'>
                    <div class='panel panel-default col-xs-12 col-sm-8'>
                        <div class='panel-body'>
                            <form action='/engine-api/recoverysearch' method='post'>
                                <div class='form-group'>
                                    <label>Email</label>
                                    <input class='form-control' name='myemail' value=''>
                                </div>
                                <br>
                                {$captcha}
                                <br><input type=submit class='btn btn-primary btn-lg' value='<#send-me-recovery#>'>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
HTM;

        Output::get()->writeOption('title_short', '<#password-recovery#>')
            ->writeOption('contains', $out)
            ->render();
    }


    /**
     * Proceed the password recovery form, search user and sends email
     */
    public static function recoverySearch()
    {
        if (!Security\Captcha::check(
            Input::get()->read('captcha_rid'),
            Input::get()->read('captcha'))
        ) {
            Output::get()->error("<#captcha-error#>");
        }

        if (User::get()->sendRecoveryEmail(Input::get()->read('myemail')))
        {
            $out = "<div class='panel panel-success'>
                        <div class='panel-heading'>
                            <h3 class='panel-title'><#email-sent#></h3>
                        </div>
                        <div class='panel-body'>
                            <#recovery-email-sent#>
                        </div>
                    </div>";
            Output::get()->writeOption('title_short', '<#password-recovery#>')
                ->writeOption('contains', $out)
                ->render();
        } else {
            Output::get()->error("Сбой");
        }
    }

    /**
     * Shows the password change form with recovery key
     */
    public static function recoveryKey()
    {
        $captcha = Output\Snippets::captcha(Security\Captcha::set());
        $out = <<<HTM
            <p><#recovery-form-notice#></p>
            <br><br>
            <div class='container-fluid'>
                <div class='row'>
                    <div class='panel panel-default col-xs-12 col-sm-8'>
                        <div class='panel-body'>
                            <form action='/engine-api/recoveryfinish' method='post'>
                                <div class='form-group'>
                                    <label><#your-id#></label>
                                    <input class='form-control' name='myid' value=''>
                                </div>
                                <div class='form-group'>
                                    <label><#your-key#></label>
                                    <input class='form-control' name='mykey' value=''>
                                </div>
                                <div class='form-group'>
                                    <label><#new-password#></label>
                                    <input class='form-control' type=password name='mypassword' value=''>
                                </div>
                                <br>
                                {$captcha}
                                <br><input class='btn btn-primary btn-lg' type=submit value='<#set-new-password#>'>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
HTM;

        Output::get()->writeOption('title_short', '<#password-recovery#>')
            ->writeOption('contains', $out)
            ->render();
    }


    /**
     * Changes user's password
     */
    public static function recoveryFinish()
    {
        // check inputs
        if ( ! (
            (Input::get()->readInt('myid')) &&
            (Input::get()->read('mykey')))
        ) {
            Output::get()->error("<#id-and-key-error#>");
        }

        // check password length
        if (strlen(Input::get()->read('mypassword')) < Security::getPasswordLength()) {
            Output::get()->error("<#password-length-error#>");
        }

        // check captcha
        if (!Security\Captcha::check(
            Input::get()->read('captcha_rid'),
            Input::get()->read('captcha'))
        ) {
            Output::get()->error("<#captcha-error#>");
        }

        // change password
        if (User::get()->changePassword(
            Input::get()->readInt('myid'),
            Input::get()->read('mykey'),
            Input::get()->read('mypassword'))
        ) {
            $out = "<div class='panel panel-success'>
                        <div class='panel-heading'>
                            <h3 class='panel-title'><#password-changed#></h3>
                        </div>
                        <div class='panel-body'>
                            <#password-really-changed#>
                        </div>
                    </div>";
            Output::get()->writeOption('title_short', '<#password-recovery#>')
                ->writeOption('contains', $out)
                ->render();
        } else {
            Output::get()->error(User::get()->readErrorMessage());
        }
    }

} 