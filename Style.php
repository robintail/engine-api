<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 01:53
 */

namespace RobinTail\EngineAPI;


/**
 * Class Style
 * @package RobinTail\EngineAPI
 */
class Style {

    /**
     * @var Style
     */
    private static $instance;

    /**
     * @return Style
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }



    /**
     * @param int $id
     * @return Style\Item
     * @throws \Exception
     */
    public function readById($id)
    {
        DB::get()->prepare("SELECT *
                            FROM styles
                            WHERE id_style=?")
                ->bind(1, intval($id))
                ->execute();
        if ($row = DB::get()->fetchRow())
        {
            return $this->createStyleItemFromArray($row);
        } else {
            return Style\Item::create();
        }
    }


    /**
     * @param int $code
     * @return Style\Item
     * @throws \Exception
     */
    public function readByCode($code)
    {
        DB::get()->prepare("SELECT *
                            FROM styles
                            WHERE style_code=?")
            ->bind(1, $code)
            ->execute();
        if ($row = DB::get()->fetchRow())
        {
            return $this->createStyleItemFromArray($row);
        } else {
            return Style\Item::create();
        }
    }

    /**
     * @param array $row
     * @return Style\Item
     */
    private function createStyleItemFromArray($row)
    {
        return Style\Item::create()
                ->writeId($row['id_style'])
                ->writeCode($row['style_code'])
                ->writeDescription($row['style_description'])
                ->writeLink($row['style_link'])
                ->writeText($row['style_text']);
    }

    /**
     * @return Style\Item[]
     * @throws \Exception
     */
    public function readAll()
    {
        $result = array();
        DB::get()->prepare("SELECT *
                            FROM styles
                            ORDER BY style_code")
                ->execute();
        while ($row = DB::get()->fetchRow())
        {
            $result[] = $this->createStyleItemFromArray($row);
        }
        return $result;
    }


    /**
     * @param string $code
     * @param int $idToExclude
     * @return bool
     * @throws \Exception
     */
    public function isExists($code, $idToExclude)
    {
        DB::get()->prepare("SELECT id_style
                            FROM styles
                            WHERE style_code=?")
            ->bind(1, $code)
            ->execute();
        if ($row = DB::get()->fetchRow())
        {
            if ($row['id_style'] == $idToExclude)
            {
                return false; // existed code of currently testing style
            } else {
                return true; // existed code of another style
            }
        }
        return false; // not exists
    }


    /**
     * @param Style\Item $style
     * @throws \Exception
     * @return bool
     */
    public function write($style)
    {
        if (intval($style->readId()))
        { // update
            DB::get()->prepare("UPDATE styles
                                SET style_code=:code,
                                    style_description=:description,
                                    style_link=:link,
                                    style_text=:text
                                WHERE id_style=:id")
                    ->bind(':code', $style->readCode())
                    ->bind(':description', $style->readDescription())
                    ->bind(':link', $style->readLink())
                    ->bind(':text', $style->readText())
                    ->bind(':id', $style->readId())
                    ->execute();
        } else { // insert
            DB::get()->prepare("INSERT INTO styles (style_code, style_description, style_link, style_text)
                                SELECT :code, :description, :link, :text")
                    ->bind(':code', $style->readCode())
                    ->bind(':description', $style->readDescription())
                    ->bind(':link', $style->readLink())
                    ->bind(':text', $style->readText())
                    ->execute();
            $style->writeId(DB::get()->getInsertId());
        }
        return true;
    }

    /**
     * @param int $id
     * @throws \Exception
     * @return bool
     */
    public function remove($id)
    {
        DB::get()->prepare("DELETE FROM styles WHERE id_style=?")
                ->bind(1, intval($id))
                ->execute();
        return true;
    }

}