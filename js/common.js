function build_select(id, data, keyfield, valuefield, selected)
{
	var s = document.createElement('select');
	s.id = id;
	s.className = 'form-control';
	for(elt in data)
	{
		s.options.add(new Option(data[elt][valuefield], data[elt][keyfield]));
	}
	s.value = selected;
	return s;
}

function build_dropdown(id, data, keyfield, valuefield, selected, onchange, placeholder)
{
	placeholder=(placeholder?placeholder:'select...');
	var out = '<li role="presentation" class="dropdown-header">'+placeholder+'</li>'; var title = ''; onchange = (onchange?onchange:''); 
	for(elt in data)
	{
		out += '<li><a value="'+data[elt][keyfield]+'" href="#" onclick="var btn=this.parentNode.parentNode.parentNode.firstChild; btn.className=btn.className.replace(/robin-btn-muted/,\'btn-default\'); btn.firstChild.nodeValue=this.innerText+\' \'; '+onchange+'; return false;">'+data[elt][valuefield]+'</a></li>';
		if (data[elt][keyfield] == selected) title = data[elt][valuefield];
	}
	out = '<div class="btn-group"><button type="button" class="btn '+(title?'btn-default':'robin-btn-muted')+' dropdown-toggle" data-toggle="dropdown">'+(title?title:placeholder)+' <span class="caret"></span></button><ul class="dropdown-menu" role="menu">' + out;
		out += '</ul></div>';
		return out;
}


function getScrollLeft()
{
    return $('body').scrollLeft();
}

function getScrollTop()
{
    return $('body').scrollTop();
}

function isObjectEmpty(obj)
{
    return Object.keys(obj).length === 0;
}

function modalAlert(title, message)
{
    $('div#msgbox .modal-title').text(title);
    $('div#msgbox .modal-body').text(message);
    $('div#msgbox').modal('show');
}


function ajax(url, data, cb, victim) {
    var spinner = $('<img />')
        .attr('src', '/vendor/robintail/engine-api/images/small/spinner.gif')
        .css({
            position: 'absolute',
            zIndex: 999,
            border: 'none',
            display: 'inline-block',
            left: ($(victim).offset().left + $(victim).width() - 10) + 'px',
            top: $(victim).offset().top + 'px'
        })
        .appendTo(document.body);
    $.post('http://'+document.location.host+'/'+url, data, function(result) {
        cb(victim, result);
        spinner.remove();
    }, 'json')
        .fail(function(data, status, xhr){
            modalAlert("Ajax query failed", "Sorry, but there was an error: " + status + " " + xhr.message);
        });
}


function switch_by_digits(value, text1, text2, text5)
{
	value = Math.floor(Number(value));
	d = Number(String(value).substr(-1));
	if (value>9)
	{
		p = Number(String(value).substr(-2, 1)); 
	} else {
		p = 0;
	}
	if (p==1)
	{
		return text5;
	} else {
		if (d==1)
		{
			return text1;
		} else if (Array(2,3,4).indexOf(d)>=0 ) { 
			return text2;
		} else { 
			return text5;
		}
	}
}


function format_number(v)
{
	numeral.language('ru');
	return numeral(v).format('0,0.00');
}

function deformat_number(v)
{
	numeral.language('ru');
	return numeral().unformat(v);
}

function tiny_init()
{
	tinymce.init({
		selector:'textarea',
		language: 'ru',
		plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    document_base_url: 'http://'+document.location.host+'/',
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fullscreen",
        extended_valid_elements: "span[id|class|style],script[src|language|type|charset]",
	    file_picker_callback: function(callback, value, meta) {
	        if (meta.filetype == 'image') {
	            file_manager(callback);
	        }
		}
	});	
}

function ace_init(id_ace, mode)
{
    var editor = ace.edit(id_ace);
    editor.setTheme('ace/theme/idle_fingers');
    editor.getSession().setMode("ace/mode/" + mode);
    editor.setAutoScrollEditorIntoView(true);
    editor.setOption("minLines", 10);
    editor.setOption("maxLines", 30);
}

function ace_fetch(id_textarea, id_ace)
{
    $('#'+id_textarea).text( ace.edit(id_ace).getSession().getValue() );
}

// fix dropdown-menu
$('ul.dropdown-menu').on('click', function(event){ event.stopPropagation(); });
