/**
 * Created by Robin on 23.06.15.
 */

function check_const_code()
{
    var victim = $("input[name='const_code']")[0];
    ajax(
        "/engine-api/ajax/admin/language_const_code",
        {
            "const_code" : victim.value,
            "id_lang" : $("input[name='id_const']")[0].value
        },
        after_check_const_code,
        victim
    );
}

function after_check_const_code(obj, json)
{
    var holder = $("input[name='const_code']").parent();
    if (json.error == "1")
    {
        holder.removeClass('has-success').addClass('has-error');
        holder.find('span.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-warning-sign');
        holder.find('div.help-block').html(json.value);
    } else {
        holder.removeClass('has-error').addClass('has-success');
        holder.find('span.glyphicon').removeClass('glyphicon-warning-sign').addClass('glyphicon-ok');
        holder.find('div.help-block').html("");
    }
}
