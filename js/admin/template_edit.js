/**
 * Created by Robin on 23.06.15.
 */

ace_init('template_html_ace', 'html');

function check_template_code()
{
    var victim = $("input[name='template_code']")[0];
    ajax(
        "/engine-api/ajax/admin/template_code",
        {
            "template_code" : victim.value,
            "id_template" : $("input[name='id_template']")[0].value
        },
        after_check_template_code,
        victim
    );
}

function after_check_template_code(obj, json)
{
    var holder = $("input[name='template_code']").parent();
    if (json.error == "1")
    {
        holder.removeClass('has-success').addClass('has-error');
        holder.find('span.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-warning-sign');
        holder.find('div.help-block').html(json.value);
    } else {
        holder.removeClass('has-error').addClass('has-success');
        holder.find('span.glyphicon').removeClass('glyphicon-warning-sign').addClass('glyphicon-ok');
        holder.find('div.help-block').html("");
    }
}

function toggle_options_list()
{
    $('#options_list').toggle();
}