/**
 * Created by Robin on 16.07.15.
 */

function toggle_extensions()
{
    $('#extensions').toggle();
}

function version_compare(a, b) {
    if (a === b) {
        return 0;
    }

    var a_components = a.split(".");
    var b_components = b.split(".");

    var len = Math.min(a_components.length, b_components.length);

    // loop while the components are equal
    for (var i = 0; i < len; i++) {
        // A bigger than B
        if (parseInt(a_components[i]) > parseInt(b_components[i])) {
            return 1;
        }

        // B bigger than A
        if (parseInt(a_components[i]) < parseInt(b_components[i])) {
            return -1;
        }
    }

    // If one's a prefix of the other, the longer one is greater.
    if (a_components.length > b_components.length) {
        return 1;
    }

    if (a_components.length < b_components.length) {
        return -1;
    }

    // Otherwise they are the same.
    return 0;
}


function check_for_updates(url)
{
    $.ajax({
        type: "GET",
        url: url,
        success: after_check_for_updates,
        dataType: 'json'
    });
}

function after_check_for_updates(json)
{
    for(var i = 0; i < json.values.length ; i++)
    {
        var pre = json.values[i];
        if (pre.type === 'tag')
        {
            if (version_compare(pre.name, settings.version) > 0)
            {
                settings.updates.push({
                    'version': pre.name,
                    'description': pre.target.message
                });
            }
        }
    }
    if (json.values.length)
    {
        check_for_updates(json.next);
    } else {
        show_updates();
    }
}

function show_updates()
{
    if (settings.updates.length)
    {
        var text = '<strong>Updates available:</strong><br />' +
                    settings.updates.map(function(elt){
                        return elt.version.toString() + '&nbsp;&mdash;&nbsp;' + elt.description
                    }).join('<br />') +
                    '<br /><strong>Use<samp> composer update </samp>command.</strong>';
        $('<div>' + text + '</div>')
            .addClass('alert')
            .addClass('alert-warning')
            .appendTo('#panel-engine-api');
    }
}

if (settings.version) {
    settings.updates = [];
    check_for_updates('https://api.bitbucket.org/2.0/repositories/robintail/engine-api/refs');
}
