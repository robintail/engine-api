/**
 * Created by Robin on 10.07.15.
 */


ace_init('style_text_ace', 'css');

function check_style_code()
{
    var victim = $("input[name='style_code']")[0];
    ajax(
        "/engine-api/ajax/admin/style_code",
        {
            "style_code" : victim.value,
            "id_style" : $("input[name='id_style']")[0].value
        },
        after_check_style_code,
        victim
    );
}

function after_check_style_code(obj, json)
{
    var holder = $("input[name='style_code']").parent();
    if (json.error == "1")
    {
        holder.removeClass('has-success').addClass('has-error');
        holder.find('span.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-warning-sign');
        holder.find('div.help-block').html(json.value);
    } else {
        holder.removeClass('has-error').addClass('has-success');
        holder.find('span.glyphicon').removeClass('glyphicon-warning-sign').addClass('glyphicon-ok');
        holder.find('div.help-block').html("");
    }
}

