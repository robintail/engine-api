if (settings.use_raw_page_editor) {
    for(var i=0; i<settings.languages.length; i++) {
        ace_init('page_html_ace_' + settings.languages[i], 'html');
    }
} else {
    tiny_init();
}
var file_manager_cb;

function file_manager(callback)
{
	file_manager_cb = callback;
	$('#filemanager').modal('show');
	uploads_refresh();
}

function uploads_refresh()
{
	$('div#filemanager #uploads').html('');
	uploads_periods_get();
}

function uploads_periods_get()
{
    ajax(
        "/engine-api/ajax/admin/uploads_periods",
        {},
        after_uploads_periods_get,
        $('body')[0]
    );
}

function after_uploads_periods_get(obj, json)
{
    for(i in json)
    {
        $('div#filemanager #uploads').append('<div class="pull-left text-center uploads-period" data-uploads-shown="0" data-period="'+json[i].period+'"><span class="glyphicon glyphicon-calendar uploads-period-icon" style="color: rgba('+Math.round(Math.random()*100)+','+Math.round(Math.random()*100)+','+Math.round(Math.random()*100)+',1);"></span><p>'+json[i].period+'</p></div>');
    }
    $('div#filemanager #uploads').append('<div class="uploads-terminator"></div>');
    $('div#filemanager #uploads .uploads-period span.uploads-period-icon').on('click', uploads_period_open);
}

function uploads_period_open(event)
{
    var holder = this.parentNode;
    $(holder)
        .removeClass('pull-left')
        .addClass('active');
    $(this)
        .off('click')
        .on('click', uploads_period_close);
    uploads_get(holder);
}

function uploads_period_close(event)
{
    var holder = this.parentNode;
    $(holder)
        .find('a.robin-thumb').remove();
    $(holder)
        .find('.loadmore').remove();
    $(holder)
        .attr('data-uploads-shown', 0)
        .removeClass('active')
        .addClass('pull-left');
    $(this)
        .off('click')
        .on('click', uploads_period_open);
}

function uploads_get(holder)
{
	ajax(
        "/engine-api/ajax/admin/uploads",
        {
            "from" : holder.getAttribute('data-uploads-shown'),
            "period" : holder.getAttribute('data-period')
        },
        after_uploads_get,
        holder
    );
}

function after_uploads_get(obj, json)
{
	$(obj).find('.loadmore').remove();
	for(i in json)
	{
		$(obj).append('<a href="#" class="pull-left robin-thumb" onclick="thumb_click(this, \'/'+settings.upload_dir+'/'+json[i].src+'\'); return false;"><span class="glyphicon glyphicon-remove upload-killer" data-id="'+json[i].id+'"></span><img src="/'+settings.upload_dir+'/'+json[i].thumb+'" style="width: '+settings.thumb_size_filemanager+'px; height: auto;"></a>');
		obj.setAttribute('data-uploads-shown', Number(obj.getAttribute('data-uploads-shown')) + 1);
	}
	$(obj).append('<div class="loadmore text-center"><button onclick="uploads_get(this.parentNode.parentNode);" class="btn btn-default" ' + (isObjectEmpty(json) ? 'disabled' : '') + '>' + settings.load_more + '</button></div>');
    after_uploads_shown();
}

function after_uploads_shown()
{
    $('div#filemanager #uploads .robin-thumb .upload-killer')
        .off('click')
        .on('click', function(event){
            event.stopPropagation();
            if (confirm(settings.confirm_image_remove))
            {
                upload_remove( this );
            }
        });
}

function upload_remove( remover )
{
    ajax(
        "/engine-api/ajax/admin/upload_remove",
        {
            "id_upload" : remover.getAttribute('data-id')
        },
        after_upload_remove,
        remover
    );
}

function after_upload_remove(obj, json)
{
    $(obj).parent().fadeOut();
}

function thumb_click(link, src)
{
	if ($('div#filemanager #force_thumb').get()[0].checked)
	{
		var thumb = $(link).find('img').attr('src');
		tinyMCE.execCommand("mceInsertContent", false, '<a href="'+src+'" class="lytebox robin-thumb" data-lyte-options="group:page"><img src="'+thumb+'"></a>');
		after_file_selected(link);
	} else {
		file_manager_cb(src);
		$('#filemanager').modal('hide');
	}
}

function after_file_selected(link)
{
	var victim = $(link);
	victim.animate({
		top: '-20px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '0px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '-15px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '0px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '-10px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '0px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '-5px'
	}, {duration: 100, queue: true});
	victim.animate({
		top: '0px'
	}, {duration: 100, queue: true});	
}

function check_page_url()
{
    var victim = $("input[name='page_url']")[0];
    ajax(
        "/engine-api/ajax/admin/page_url",
        {
            "page_url" : victim.value,
            "id_page" : $("input[name='id_page']")[0].value
        },
        after_check_page_url,
        victim
    );
}

function after_check_page_url(obj, json)
{
    var holder = $("input[name='page_url']").parent();
    if (json.error == "1")
    {
        holder.removeClass('has-success').addClass('has-error');
        holder.find('span.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-warning-sign');
        holder.find('div.help-block').html(json.value);
    } else {
        holder.removeClass('has-error').addClass('has-success');
        holder.find('span.glyphicon').removeClass('glyphicon-warning-sign').addClass('glyphicon-ok');
        holder.find('div.help-block').html("");
    }
}

function toggle_trans_enabled(chb)
{
    var lang = chb.getAttribute('id').split('[')[1];
    lang = lang.substr(0, lang.length-1);
    if (chb.checked) {
        $('div#translation_'+lang).show();
    } else {
        $('div#translation_'+lang).hide();
    }
}

function before_submit()
{
    if (settings.use_raw_page_editor) {
        for (var i = 0; i < settings.languages.length; i++) {
            ace_fetch('page_html_'+settings.languages[i], 'page_html_ace_'+settings.languages[i]);
        }
    }
}