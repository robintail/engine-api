var uploader = new plupload.Uploader({
    runtimes : 'html5',
     
    browse_button : 'pickfiles', // you can pass in id...
    container: document.getElementById('container'), // ... or DOM Element itself
     
    url : "/engine-api/admin/upload",
    unique_names: true,
     
    filters : {
        max_file_size : '10mb',
        mime_types: [
            {title : "Image files", extensions : "jpg,gif,png"}
        ]
    },
      
 
    init: {
        PostInit: function() {
            document.getElementById('filelist').innerHTML = '';
            document.getElementById('uploadfiles').onclick = function() {
                uploader.start();
                return false;
            };
        },
 
        FileUploaded: function (up, file, response)
        {
			response = jQuery.parseJSON( response.response );
			if (response.error)
			{
				document.getElementById('console').innerHTML += "\nError #" + response.error.code + ": " + response.error.message;	
			}
        },

        FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
                document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            });
        },
 
        UploadProgress: function(up, file) {
            document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
        },
 
        Error: function(up, err) {
            document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
        },

        UploadComplete: function(up, files)
        {
        	document.getElementById('filelist').innerHTML = '';
			uploads_refresh();
        }
    }
});

uploader.init();