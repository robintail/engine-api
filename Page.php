<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 25.06.15
 * Time: 16:16
 */

namespace RobinTail\EngineAPI;


/**
 * Class Page
 * @package RobinTail\EngineAPI
 */
class Page {

    /**
     * @var Page
     */
    private static $instance;

    /**
     * @return Page
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Quick return all pages in array
     * @return Page\Item[]
     * @throws \Exception
     */
    public function readAll()
    {
        $result = array();
        DB::get()->prepare("SELECT *
                            FROM pages
                            ORDER BY page_url")
                ->execute();
        $pages = DB::get()->result();
        foreach($pages as $page)
        {
            DB::get()->prepare("SELECT *
                                FROM page_trans
                                INNER JOIN languages ON page_trans.lang_id=languages.id_lang
                                WHERE page_id=?")
                    ->bind(1, $page['id_page'])
                    ->execute();
            $trans = DB::get()->result();
            $result[] = $this->createPageItemFromArray($page, $trans);
        }
        return $result;
    }

    /**
     * @param string $url
     * @return Page\Item
     * @throws \Exception
     */
    public function readByUrl($url)
    {
        DB::get()->prepare("SELECT *
                            FROM pages
                            WHERE page_url=?")
                ->bind(1, $url)
                ->execute();
        if ($page = DB::get()->fetchRow())
        {
            DB::get()->prepare("SELECT *
                                FROM page_trans
                                INNER JOIN languages ON page_trans.lang_id=languages.id_lang
                                WHERE page_id=?")
                ->bind(1, $page['id_page'])
                ->execute();
            $trans = DB::get()->result();
            return $this->createPageItemFromArray($page, $trans);
        } else {
            return Page\Item::create();
        }
    }

    /**
     * @param int $id
     * @return Page\Item
     * @throws \Exception
     */
    public function readById($id)
    {
        DB::get()->prepare("SELECT *
                            FROM pages
                            WHERE id_page=?")
            ->bind(1, intval($id))
            ->execute();
        if ($page = DB::get()->fetchRow())
        {
            DB::get()->prepare("SELECT *
                                FROM page_trans
                                INNER JOIN languages ON page_trans.lang_id=languages.id_lang
                                WHERE page_id=?")
                ->bind(1, $page['id_page'])
                ->execute();
            $trans = DB::get()->result();
            return $this->createPageItemFromArray($page, $trans);
        } else {
            return Page\Item::create();
        }
    }


    /**
     * @param string $url
     * @param int $idToExclude
     * @return bool
     * @throws \Exception
     */
    public function isExists($url, $idToExclude)
    {
        DB::get()->prepare("SELECT id_page
                            FROM pages
                            WHERE page_url=?")
                ->bind(1, $url)
                ->execute();
        if ($row = DB::get()->fetchRow())
        {
            if ($row['id_page'] == $idToExclude)
            {
                return false; // existed url of currently testing page
            } else {
                return true; // existed url of another page
            }
        }
        return false; // not exists
    }


    /**
     * @param array $page
     * @param array $trans
     * @return Page\Item
     */
    private function createPageItemFromArray($page, $trans)
    {
        $page_object = Page\Item::create()
                        ->writeId($page['id_page'])
                        ->writeUrl($page['page_url'])
                        ->writeTemplateId($page['template_id'])
                        ->writeOverrideActiveLinkId($page['override_active_link_id'])
                        ->writeIsUseAffix($page['is_use_affix']);
        foreach($trans as $t) {
            $trans_object = Page\Item\Trans::create()
                ->writeId($t['id_trans'])
                ->writeIsEnabled($t['is_trans_enabled'])
                ->writePageId($t['page_id'])
                ->writeLangId($t['lang_id'])
                ->writeTitleShort($t['page_title_short'])
                ->writeTitleLong($t['page_title_long'])
                ->writeKeywords($t['page_keywords'])
                ->writeDescription($t['page_description'])
                ->writeHtml($t['page_html'])
                ->writeAffix($t['page_affix']);
            $page_object->writeTrans($t['lang_code'], $trans_object);
        }
        return $page_object;
    }


    /**
     * @param Page\Item $page
     * @throws \Exception
     * @return bool
     */
    public function write($page)
    {
        if (intval($page->readId()))
        { // update page
            DB::get()->prepare("UPDATE pages
                                SET page_url = :url,
                                override_active_link_id = :override_active_link_id,
                                template_id = :template_id,
                                is_use_affix = :is_use_affix
                                WHERE id_page = :id")
                    ->bind('url', $page->readUrl())
                    ->bind('override_active_link_id', $page->readOverrideActiveLinkId())
                    ->bind('template_id', $page->readTemplateId())
                    ->bind('is_use_affix', $page->readIsUseAffix())
                    ->bind('id', $page->readId())
                    ->execute();
        } else { // insert page
            DB::get()->prepare("INSERT INTO pages (page_url, override_active_link_id, template_id, is_use_affix)
                                SELECT :url, :override_active_link_id, :template_id, :is_use_affix")
                    ->bind('url', $page->readUrl())
                    ->bind('override_active_link_id', $page->readOverrideActiveLinkId())
                    ->bind('template_id', $page->readTemplateId())
                    ->bind('is_use_affix', $page->readIsUseAffix())
                    ->execute();
            $page->writeId(DB::get()->getInsertId());
        }
        foreach($page->readTransLanguages() as $langCode)
        {
            $trans = $page->readTrans($langCode);

            // remove translations
            DB::get()->prepare("DELETE FROM page_trans
                            WHERE page_id=:page_id AND lang_id=:lang_id")
                ->bind(':page_id', $page->readId())
                ->bind(':lang_id', $trans->readLangId() ? $trans->readLangId() : Lang::get()->readByCode($langCode)->readId())
                ->execute();

            // insert translations

            DB::get()->prepare("INSERT INTO page_trans (is_trans_enabled, page_id, lang_id, page_title_short,
                                page_title_long, page_html, page_keywords, page_description, page_affix)
                                SELECT :is_trans_enabled, :page_id, :lang_id, :page_title_short,
                                :page_title_long, :page_html, :page_keywords, :page_description, :page_affix")
                    ->bind(':is_trans_enabled', $trans->readIsEnabled())
                    ->bind(':page_id', $page->readId())
                    ->bind(':lang_id', $trans->readLangId() ? $trans->readLangId() : Lang::get()->readByCode($langCode)->readId())
                    ->bind(':page_title_short', $trans->readTitleShort())
                    ->bind(':page_title_long', $trans->readTitleLong())
                    ->bind(':page_html', $trans->readHtml())
                    ->bind(':page_keywords', $trans->readKeywords())
                    ->bind(':page_description', $trans->readDescription())
                    ->bind(':page_affix', $trans->readAffix())
                    ->execute();
        }
        return true;
    }


    /**
     * @param int $id
     * @throws \Exception
     * @return bool
     */
    public function remove($id)
    {
        DB::get()->prepare("DELETE FROM pages WHERE id_page=?")
                ->bind(1, intval($id))
                ->execute();
        return true;
    }


    /**
     * @param string[] $words
     * @return array
     * @throws \Exception
     */
    public function search($words=array())
    {
        // check array
        if (!count($words)) return array();
            
        // prepare the SQL query
        $sql = "SELECT page_id, lang_code
                            FROM page_trans
                            INNER JOIN languages ON page_trans.lang_id = languages.id_lang
                            WHERE ";

        foreach($words as $k => $word)
        {
            $sql .= $k ? " AND " : "";
            $sql .= "               (
                                page_title_short LIKE ? OR
                                page_title_long LIKE ? OR
                                page_html LIKE ?
                            )";
        }

        $stmt = DB::get()->prepare($sql);

        // bind arguments
        $i=0;
        foreach($words as $word)
        {
            for($e=1; $e<=3; $e++) { // $e(max) is the number of checked fields in index
                $i++;
                $stmt->bind($i, '%'.$word.'%');
            }
        }

        // execute
        $stmt->execute();

        // fetch result
        return DB::get()->result($stmt);
    }

} 