<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.11.15
 * Time: 02:23
 */

namespace RobinTail\EngineAPI;
use RobinTail\EngineAPI\Security\Captcha;


/**
 * Class Starter
 * @package RobinTail\EngineAPI
 */
class Starter {
    /**
     * @var Starter
     */
    private static $instance;
    /**
     * @var string
     */
    private $localeAll = 'ru_RU.UTF-8';
    /**
     * @var string
     */
    private $localeNumeric = 'C';
    /**
     * @var string
     */
    private $timezone = 'Europe/Moscow';
    /**
     * @var callable
     */
    private $routeFail = null;
    /**
     * @var bool
     */
    private $cleanSearchDisabled = false;


    // constructors


    /**
     * Singleton constructor
     * @return Starter
     */
    public static function get()
    {
        if (!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    /**
     * private constructor
     */
    private function __construct() {
        $this->addEngineRoutes();
    }


    // public methods


    /**
     * Set up database connection
     * @param string $host
     * @param string $port
     * @param string $base
     * @param string $user
     * @param string $pass
     * @return $this
     */
    public function setDatabaseConnection($host, $port, $base, $user, $pass)
    {
        DB\Settings::setHost($host);
        DB\Settings::setPort($port);
        DB\Settings::setBase($base);
        DB\Settings::setUser($user);
        DB\Settings::setPass($pass);
        return $this;
    }

    /**
     * Set all locales
     * @param string $localeAll
     * @return $this
     */
    public function setLocaleAll($localeAll)
    {
        $this->localeAll = $localeAll;
        return $this;
    }

    /**
     * Set locale for numeric
     * @param string $localeNumeric
     * @return $this
     */
    public function setLocaleNumeric($localeNumeric)
    {
        $this->localeNumeric = $localeNumeric;
        return $this;
    }

    /**
     * Set timezone
     * @param string $timezone
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * Add route handler
     * @param string $path
     * @param callable $handler
     * @return $this
     */
    public function addRoute($path, $handler)
    {
        Router::get()->addRoute($path, $handler);
        return $this;
    }

    /**
     * Add route for page handling
     * Path prefix may be empty
     * If handler is empty, standard page handler will be used (handlePage)
     * @param string $pathPrefix
     * @param bool|callable $handler
     * @return $this
     */
    public function addRoutePage($pathPrefix = "", $handler = false)
    {
        $this->addRoute(
            trim($pathPrefix, "/ ") . "/*",
            $handler ? $handler : array($this, "handlePage")
        );
        return $this;
    }

    /**
     * Stores route fail handler
     * @param callable $handler
     * @return $this
     */
    public function addRouteFail($handler)
    {
        $this->routeFail = $handler;
        return $this;
    }

    /**
     * Add static route
     * @param string $path
     * @return $this
     */
    public function addRouteStatic($path)
    {
        $this->addRoute($path, array($this, "handleFile"));
        return $this;
    }


    /**
     * Add route for search requests
     * If handler is empty, standard search handler will be used (handleSearch)
     * Disable clean search for (example) Chinese website language
     * @param string $path
     * @param bool|callable $handler
     * @param bool $disableCleanSearch
     * @return $this
     */
    public function addRouteSearch($path = 'engine-api/searchresult', $handler = false, $disableCleanSearch = false)
    {
        $this->cleanSearchDisabled = $disableCleanSearch;
        $this->addRoute(
            trim($path, "/ "),
            $handler ? $handler : array($this, 'handleSearch')
        );
        return $this;
    }

    /**
     * Add field to forward while login and register process
     * @param $fieldName
     * @return $this
     */
    public function addLoginForwardField($fieldName)
    {
        Login::addForwardField($fieldName);
        return $this;
    }

    /**
     * Add handler for user register action
     * Called with argument of new user id
     * @param $callable
     * @return $this
     */
    public function addRegisterHandler($callable)
    {
        Login::addRegisterHandler($callable);
        return $this;
    }

    /**
     * Add item to admin menu
     * By default place it on third place from the end
     * @param string $url
     * @param string $title
     * @param int $popFromTheEnd
     * @return $this
     */
    public function addAdminMenuItem($url, $title, $popFromTheEnd=2)
    {
        $stack = array();
        for($i=0; $i<$popFromTheEnd; $i++)
        {
            array_push($stack, array_pop(Output\Snippets::$adminMenu));
        }
        array_push(Output\Snippets::$adminMenu, array('url' => $url, 'title' => $title));
        for($i=0; $i<$popFromTheEnd; $i++)
        {
            array_push(Output\Snippets::$adminMenu, array_pop($stack));
        }
        return $this;
    }

    /**
     * Add snippet callback to Output with option name (template constant)
     * @param string $optionName
     * @param callable $callback
     */
    public function addSnippet($optionName, $callback)
    {
        Output::get()->writeOption($optionName, $callback);
    }


    // final public method



    /**
     * Engine starter
     */
    public function start()
    {
        $this->startEnv();
        $this->checkMntMode();
        $this->startRouter();
    }


    // private methods



    /**
     * Set up errors, time, locale, handlers
     */
    private function startEnv()
    {
        // display errors
        ini_set('display_errors', true);

        // errors are equals to all except notices
        Error_Reporting(E_ALL & ~E_NOTICE);

        // locale is RU excluding numeric
        setlocale(LC_ALL, $this->localeAll);
        setlocale(LC_NUMERIC, $this->localeNumeric);

        // time zone is Moscow
        date_default_timezone_set($this->timezone);

        // use error handling for libxml
        libxml_use_internal_errors(true);

        // error handler
        set_error_handler(array(__NAMESPACE__.'\Error', 'ErrorHandler'), E_ALL & ~E_NOTICE);

        // exception handler
        set_exception_handler(array(__NAMESPACE__.'\Error', 'ExceptionHandler'));

        // assertion failure throws and exception
        assert_options(ASSERT_ACTIVE, 1);
        assert_options(ASSERT_WARNING, 0);
        assert_options(ASSERT_CALLBACK, function($file, $line, $code, $desc = null) { throw new \Exception($file.":".$line." (".$code.") ".$desc); });
    }

    /**
     * check website is closed
     */
    private function checkMntMode()
    {
        if (!User\Current::get()->readIsAdmin())
            if (Settings::get()->readValue('maintenance_mode'))
            {
                Output::get()->error('Website is now in maintenance mode. Please visit us later.');
            }
    }

    /**
     * set up engine routes
     */
    private function addEngineRoutes()
    {
        $this
        // login, logout, register
            ->addRoute('engine-api/login/login', array(Login::class, 'login'))
            ->addRoute('engine-api/login/logout', array(Login::class, 'logout'))
            ->addRoute('engine-api/login/register', array(Login::class, 'register'))
        // password recovery
            ->addRoute('engine-api/login/recoveryform', array(Login::class, 'recoveryForm'))
            ->addRoute('engine-api/login/recoverysearch', array(Login::class, 'recoverySearch'))
            ->addRoute('engine-api/login/recoverykey', array(Login::class, 'recoveryKey'))
            ->addRoute('engine-api/login/recoveryfinish', array(Login::class, 'recoveryFinish'))
        // captcha
            ->addRoute('engine-api/captcha', array(Captcha::class, 'send'))
        // language selection on website and admin cp
            ->addRoute('engine-api/setlang', array(Lang::class, 'sendCookieLang'))
            ->addRoute('engine-api/admin/setlang', array(Lang::class, 'sendCookieLangForAdminCP'))
        // admin cp
            ->addRoute('engine-api/admin/cp', array(Admin::class, 'CP'))
            ->addRoute('engine-api/admin/language', array(Admin::class, 'language'))
            ->addRoute('engine-api/admin/language_edit', array(Admin::class, 'languageEdit'))
            ->addRoute('engine-api/admin/language_remove', array(Admin::class, 'languageRemove'))
            ->addRoute('engine-api/admin/language_enable', array(Admin::class, 'languageEnable'))
            ->addRoute('engine-api/admin/language_domestic', array(Admin::class, 'languageDomestic'))
            ->addRoute('engine-api/admin/language_international', array(Admin::class, 'languageInternational'))
            ->addRoute('engine-api/admin/language_const', array(Admin::class, 'languageConst'))
            ->addRoute('engine-api/admin/language_const_edit', array(Admin::class, 'languageConstEdit'))
            ->addRoute('engine-api/admin/language_const_remove', array(Admin::class, 'languageConstRemove'))
            ->addRoute('engine-api/admin/menu', array(Admin::class, 'menu'))
            ->addRoute('engine-api/admin/menu_edit', array(Admin::class, 'menuEdit'))
            ->addRoute('engine-api/admin/menu_remove', array(Admin::class, 'menuRemove'))
            ->addRoute('engine-api/admin/pages', array(Admin::class, 'pages'))
            ->addRoute('engine-api/admin/page_edit', array(Admin::class, 'pageEdit'))
            ->addRoute('engine-api/admin/page_remove', array(Admin::class, 'pageRemove'))
            ->addRoute('engine-api/admin/settings', array(Admin::class, 'settings'))
            ->addRoute('engine-api/admin/settings_edit', array(Admin::class, 'settingsEdit'))
            ->addRoute('engine-api/admin/styles', array(Admin::class, 'styles'))
            ->addRoute('engine-api/admin/style_edit', array(Admin::class, 'styleEdit'))
            ->addRoute('engine-api/admin/style_remove', array(Admin::class, 'styleRemove'))
            ->addRoute('engine-api/admin/templates', array(Admin::class, 'templates'))
            ->addRoute('engine-api/admin/template_edit', array(Admin::class, 'templateEdit'))
            ->addRoute('engine-api/admin/template_remove', array(Admin::class, 'templateRemove'))
            ->addRoute('engine-api/admin/upload', array(Admin::class, 'upload'))
            ->addRoute('engine-api/admin/uploads', array(Admin::class, 'uploads'))
            ->addRoute('engine-api/admin/users', array(Admin::class, 'users'))
            ->addRoute('engine-api/admin/user_admin', array(Admin::class, 'userAdmin'))
            ->addRoute('engine-api/admin/user_edit', array(Admin::class, 'userEdit'))
            ->addRoute('engine-api/admin/user_remove', array(Admin::class, 'userRemove'))
        // ajax
            ->addRoute('engine-api/ajax/admin/language_code', array(Ajax\Admin::class, 'languageCode'))
            ->addRoute('engine-api/ajax/admin/language_const_code', array(Ajax\Admin::class, 'languageConstCode'))
            ->addRoute('engine-api/ajax/admin/menu_dropdown_right', array(Ajax\Admin::class, 'menuDropdownRight'))
            ->addRoute('engine-api/ajax/admin/menu_ordering', array(Ajax\Admin::class, 'menuOrdering'))
            ->addRoute('engine-api/ajax/admin/menu_title', array(Ajax\Admin::class, 'menuTitle'))
            ->addRoute('engine-api/ajax/admin/page_url', array(Ajax\Admin::class, 'pageUrl'))
            ->addRoute('engine-api/ajax/admin/style_code', array(Ajax\Admin::class, 'styleCode'))
            ->addRoute('engine-api/ajax/admin/template_code', array(Ajax\Admin::class, 'templateCode'))
            ->addRoute('engine-api/ajax/admin/upload_remove', array(Ajax\Admin::class, 'uploadRemove'))
            ->addRoute('engine-api/ajax/admin/uploads', array(Ajax\Admin::class, 'uploads'))
            ->addRoute('engine-api/ajax/admin/uploads_periods', array(Ajax\Admin::class, 'uploadsPeriods'))
        // static files in vendor dir
            ->addRouteStatic("vendor/*.css")
            ->addRouteStatic("vendor/*.js")
            ->addRouteStatic('vendor/*.jpg')
            ->addRouteStatic('vendor/*.gif')
            ->addRouteStatic('vendor/*.png')
            ->addRouteStatic('vendor/*.woff')
            ->addRouteStatic('vendor/*.woff2')
            ->addRouteStatic('vendor/*.ttf');
    }

    /**
     * set up route fail callback and Execute routing
     */
    private function startRouter()
    {
        $this->addRoute('*', is_callable($this->routeFail) ? $this->routeFail : array($this, 'handleFail'));
        Router::get()->execute();
    }


    // standard handlers

    /**
     * Handles page request
     * Returns false on error
     * @param string $url
     * @return bool
     */
    public function handlePage($url)
    {
        $page = Page::get()->readByUrl($url);
        if (!$page->readId()) return false;
        $trans = $page->readTrans(Lang::get()->getUsing());
        if (!$trans->readIsEnabled())
        {
            $trans = $page->readTrans($page->readTransLanguagesEnabled()[0]);
            if (!$trans->readIsEnabled()) return false; // translation is not enabled
            $noTranslationWarning = '<div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><#translation-missing-title#>&nbsp;&rarr;&nbsp;'.Lang::get()->readById($trans->readLangId())->readName().'</h3>
                                        </div>
                                        <div class="panel-body">
                                            <#translation-missing#>
                                        </div>
                                    </div>';
        } else {
            $noTranslationWarning = '';
        }

        if ($page->readIsUseAffix()) {
            $content =  "<div class='row'>
                            <div class='col-md-9'>
                                <div class='btn-group page-affix-dropdown-btn visible-xs visible-sm'>
                                    <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Оглавление <span class='caret'></span></button>
                                    <ul class='dropdown-menu nav' role='menu'>" . $trans->readAffix() . "</ul>
                                </div>" .
                                $noTranslationWarning . $trans->readHtml() . "
                            </div>
                            <div class='hidden-xs hidden-sm col-md-3'>
                                <nav id='page-affix' data-spy='affix' data-offset-top='65'>
                                    <ul class='nav'>" . $trans->readAffix() . "</ul>
                                </nav>
                            </div>
                        </div>";
        } else {
            $content = $noTranslationWarning . $trans->readHtml();
        }
        Output::get()->writeOption('title_short', $trans->readTitleShort())
            ->writeOption('title_long', $trans->readTitleLong())
            ->writeOption('contains', $content)
            ->writeOption('keywords', $trans->readKeywords())
            ->writeOption('description', $trans->readDescription())
            ->writeOption('menu_active', $page->readOverrideActiveLinkId() ? $page->readOverrideActiveLinkId() : $url)
            ->writeTemplate($page->readTemplateId())
            ->render();
        return true;
    }

    /**
     * Handles routing fail
     * Redirects to index page
     * @param string $req
     * @return bool
     */
    public function handleFail($req)
    {
        $this->handlePage(Settings::get()->readValue('index_page'));
        return true;
    }

    /**
     * Handles file request
     * Sends file
     * @return bool
     */
    public function handleFile() {
        if (!Output\File::send(Path::resolve(Router::get()->getRequestURI())))
            return false;
        return true;
    }


    /**
     * Handles search request
     */
    public function handleSearch() {
        $text = Input::get()->read('text'); // read input string
        $words = Regex::cleanSearch($text, $this->cleanSearchDisabled); // clean string to words
        $result = Page::get()->search($words); // perform the search
        $html = "<h3><#search-results-for#> ".implode(" ", $words)."</h3>";
        foreach($result as $k => $row)
        {
            $page = Page::get()->readById( $row['page_id'] );
            $lang = Lang::get()->readByCode($row['lang_code']);
            $trans = $page->readTrans( $lang->readCode() );
            $clear = strip_tags($trans->readHtml());
            $html .= "<p>
                        <a href='/".$page->readUrl()."?lang=".$lang->readCode()."'>
                            <strong>".
                             ($k+1).". ".$trans->readTitleLong()." [".$lang->readName()."]
                            </strong>
                        </a>
                      </p>
                      ";
            foreach($words as $word)
            {
                $pos = mb_stripos($clear, $word, 0, 'utf-8');
                if ($pos!==false) {
                    $part = mb_substr($clear, $pos < 300 ? 0 : $pos - 300, $pos + 300, 'utf-8');
                    $part = preg_replace("/".$word."/siu", "<strong>" . $word . "</strong>", $part);
                    $html .= "
                        <p>
                            <em>".
                                $part."&hellip;
                            </em>
                        </p>";
                }
            }
        }
        Output::get()->writeOption('title_short', '<#search#>')
            ->writeOption('contains', $html)
            ->render();
    }

} 