<?

namespace RobinTail\EngineAPI;

/**
 * Class Regex
 * @package RobinTail\EngineAPI
 */
class Regex
{
    /**
     * Minimal search request length or search word length
     * @var int
     */
    public static $searchMin = 2;


    /**
     * Clears and returns int value or NULL
     * @param $v
     * @return int|null
     */
    public static function intNull( $v )
	{
		// число или NULL
		return intval($v) ? intval($v) : null;
	}

    /**
     * Clears and returns float value (accept strings) or NULL
     * @param $v
     * @return float|null
     */
    public static function floatNull( $v )
	{
        $result = self::float($v);
		return $result ? $result : null;
	}

    /**
     * Returns float (from string)
     * @param $v
     * @return float
     */
    public static function float($v)
    {
        $find = array(" ", ",");
        $replace = array("", ".");
        return floatval(str_replace($find, $replace, $v));
    }

    /**
     * Returns float value with 2 digits after comma (using round)
     * @param $t
     * @return float
     */
    public static function decimal2( $t )
	{
		return round(self::float($t),2);
	}

    /**
     * Convert text dates to Unix time
     * @param string $t
     * @return int
     */
    public static function dateUnix( $t )
	{
		//date_default_timezone_set('UTC'); // dst OFF
		$t = trim($t);
		$d = "";
		if (strstr($t, ".")) $d = ".";
		if (strstr($t, "/")) $d = "/";
		if (strstr($t, "\\")) $d = "\\";
		if (strstr($t, "-")) $d = "-";
		if ($d) {
			$arr = explode($d, $t);
			return mktime(0,0,0,intval($arr[1]),intval($arr[0]),intval($arr[2])); 
		} else {
			return 0;
		}
	}


    /**
     * Removes new lines
     * @param string $txt
     * @return string
     */
    public static function cleanNewlines($txt)
	{
		return str_replace(array("\r","\n"),array("",""),$txt);
	}

    /**
     * Escapes all special regular expression chars
     * @param string $txt
     * @return string
     */
    public static function escapeRegex($txt)
    {
        return preg_replace('/([.?*+^$[\]\\(){}|-])/', '\\\\$1', $txt);
    }


    /**
     * Cleans and returns an array of integers
     * Optionally with zeros
     * @param mixed[] $array
     * @param bool $withZeros
     * @return int[]
     */
    public static function arrayIntval($array, $withZeros=true)
	{
		if (is_array($array))
		{
			foreach($array as $k=>$v)
			{
				if (is_array($v))
				{
					$array[$k] = self::arrayIntval($v);
				} else {
					if (intval($v) || ($withZeros))
					{
						$array[$k] = intval($v);
					} else {
						unset($array[$k]);
					}
				}
			}
		} else {
            $array = array();
        }
		return $array;
	}


    /**
     * Cleans and returns an array of floats
     * Optionaly with zeros
     * @param mixed[] $array
     * @param bool $withZeros
     * @return float[]
     */
    public static function arrayFloatval($array, $withZeros=true)
	{
		if (is_array($array))
		{
			foreach($array as $k=>$v)
			{
				if (is_array($v))
				{
					$array[$k] = self::arrayFloatval($v);
				} else {
					if (self::float($v) || ($withZeros))
					{
						$array[$k] = self::float($v);
					} else {
						unset($array[$k]);
					}
				}
			}
		} else {
            $array = array();
        }
		return $array;
	}

    /**
     * Breaks search request to cleaned words
     * @param string $txt
     * @param bool $disableCharsFilter
     * @return string[]
     */
    public static function cleanSearch($txt, $disableCharsFilter = false)
	{
		if (!$disableCharsFilter)
            $txt=trim(preg_replace("/[^A-Za-zА-ЯЁа-яё0-9\ ]/siu"," ",trim($txt)));
		$txt=trim(preg_replace("/\s+/", " ", $txt));
		$unclean = explode(' ',$txt);
		$words = array();
		foreach($unclean as $word)
		{
			$word = trim($word);
			if(strlen($word)>=self::$searchMin)
			{
				$words[] = $word;
			}
		}
		if ((!count($words)) and (strlen($txt)>=self::$searchMin)) $words = array($txt);
		return $words;
	}

    /**
     * Alias to formatNumber()
     * @param float $v
     * @return string
     */
    public static function buildNumber($v) { return self::formatNumber($v); }

    /**
     * Formats number with thousand and comma separator
     * @param float $v
     * @return string
     */
    public static function formatNumber($v)
	{
		return number_format($v, 2, ",", " ");
	}

    /**
     * Formats Unix time interval to hours
     * @param int $v
     * @return string
     */
    public static function buildHours($v)
	{
		return str_pad(intval($v),2,"0",STR_PAD_LEFT).":".str_pad(intval(($v-intval($v))*60),2,"0",STR_PAD_LEFT);
	}


    /**
     * Returns the language compatible form of word for number
     * @param int|float $value
     * @param string $text1
     * @param string $text2
     * @param string $text5
     * @return string
     */
    public static function switchByDigits($value, $text1, $text2, $text5)
	{
		$value = intval($value);
		$d = substr($value, -1); // last digit
		if ($value>9)
		{
			$p = substr($value, -2, 1); // pre last digit
		} else {
			$p = 0;
		}
		if ($p==1) // 112, 5315, 685818
		{
			return $text5;
		} else {
			if ($d==1) // 1, 21, 331
			{
				return $text1;
			} elseif (in_array($d,array(2,3,4))) { // 2, 33, 54, 163
				return $text2;
			} else { 
				return $text5;
			}
		}
	}


    /**
     * @deprecated
     * @param $summa
     * @return int
     */
    public static function getSwitcherByDigits($summa)
	{	
	    // возвращает тип написания полного наименования валюты
	    // 1 - РУБЛЬ
	    // 2 - РУБЛЯ
	    // 5 - РУБЛЕЙ
	    $summa = intval($summa);
	    $txt = substr($summa, -2);
	    $s2 = intval($txt);
	    $txt = substr($summa, -1);
	    $s1 = intval($txt);
	    if (($s2 > 9) And ($s2 < 20))
	            return 5;
	    else if ($s1 == 1)
	            return 1;
	    else if (($s1 > 1) And ($s1 < 5))
	            return 2;
	    else
	            return 5;
	}

    /**
     * @deprecated
     * @param $in
     * @param string $currency
     * @param bool $short
     * @return string
     * @throws \Exception
     */
    public static function  costToWords($in, $currency = "RUR", $short = false)
	{
        $out=""; $sot=array(); $razr1=array(); $razr2=array(); $razr3=array();
        DB::get()->prepare("SELECT Short_Currency_Doc, Short_Cent_Doc, Full_Currency_1, Full_Currency_2, Full_Currency_5, Full_Cent_1, Full_Cent_2, Full_Cent_5
        			        FROM Currency
        			        WHERE " . (is_numeric($currency) ? "ID_Currency=?" : "Short_Currency=?"))
                ->bind(1,$currency)
                ->execute();
        $row = DB::get()->fetchRow();
        //******** Обработка округления ****************
        $in = ROUND($in, 2);
        if ($in > 2147483647) return "[ОШИБКА ПЕРЕПОЛНЕНИЯ]";
        $txt = number_format($in, 2, ".", "");
        $des = intval(substr($txt, 0, strpos($txt, ".", 1)));
        if ($des < intval($in)) $in = $in - 1;
        //****************************************************
        $val = $in;        
        $sot[1] = $in - Intval($in / 1000) * 1000;
        $in = Intval($in / 1000);
        $sot[2] = $in - Intval($in / 1000) * 1000;
        $in = Intval($in / 1000);
        $sot[3] = $in - Intval($in / 1000) * 1000;
        $in = Intval($in / 1000);
        $sot[4] = $in - Intval($in / 1000) * 1000;
        $in = Intval($in / 1000);
        $sot[5] = $in - Intval($in / 1000) * 1000;
        $in = Intval($in / 1000);
        

        //Единицы
        $razr1[1] = "Один";
        $razr1[2] = "Два";
        $razr1[3] = "Три";
        $razr1[4] = "Четыре";
        $razr1[5] = "Пять";
        $razr1[6] = "Шесть";
        $razr1[7] = "Семь";
        $razr1[8] = "Восемь";
        $razr1[9] = "Девять";
        $razr1[11] = "Одна";
        $razr1[12] = "Две";
        //Десятки
        $razr2[2] = "Двадцать";
        $razr2[3] = "Тридцать";
        $razr2[4] = "Сорок";
        $razr2[5] = "Пятьдесят";
        $razr2[6] = "Шестьдесят";
        $razr2[7] = "Семьдесят";
        $razr2[8] = "Восемьдесят";
        $razr2[9] = "Девяносто";
        $razr2[10] = "Десять";
        $razr2[11] = "Одиннадцать";
        $razr2[12] = "Двенадцать";
        $razr2[13] = "Тринадцать";
        $razr2[14] = "Четырнадцать";
        $razr2[15] = "Пятнадцать";
        $razr2[16] = "Шестнадцать";
        $razr2[17] = "Семнадцать";
        $razr2[18] = "Восемнадцать";
        $razr2[19] = "Девятнадцать";
        //Сотни
        $razr3[1] = "Сто";
        $razr3[2] = "Двести";
        $razr3[3] = "Триста";
        $razr3[4] = "Четыреста";
        $razr3[5] = "Пятьсот";
        $razr3[6] = "Шестьсот";
        $razr3[7] = "Семьсот";
        $razr3[8] = "Восемьсот";
        $razr3[9] = "Девятьсот";
        //*********** Рубли **************
        for ($i = 5; $i>=1; $i--)
        {
                if ($sot[$i] > 0)
                {
                        $r3 = Intval($sot[$i] / 100);
                        $r2 = Intval(($sot[$i] - Intval($sot[$i] / 100) * 100) / 10);
                        $r1 = $sot[$i] - Intval($sot[$i] / 100) * 100 - Intval(($sot[$i] - Intval($sot[$i] / 100) * 100) / 10) * 10;
                        $des = $sot[$i] - Intval($sot[$i] / 100) * 100;
                        switch($i)
                        {
                                case 1:
                                        $txt = "";
                                        break;
                                case 2:
                                        if (($des > 9) And ($des < 20))
                                                $txt = " тысяч";
                                        else if ($r1 == 1) 
                                                $txt = " тысяча";
                                        else if (($r1 > 1) And ($r1 < 5))
                                                $txt = " тысячи";
                                        else
                                                $txt = " тысяч";
                                        break;
                                case 3:
                                        if (($des > 9) And ($des < 20))
                                                $txt = " миллионов";
                                        else if ($r1 = 1)
                                                $txt = " миллион";
                                        else if (($r1 > 1) And ($r1 < 5))
                                                $txt = " миллиона";
                                        else
                                                $txt = " миллионов";
                                        break;
                                case 4:
                                        if (($des > 9) And ($des < 20))
                                                $txt = " миллиардов";
                                        else if ($r1 = 1)
                                                $txt = " миллиард";
                                        else if (($r1 > 1) And ($r1 < 5)) 
                                                $txt = " миллиарда";
                                        else
                                                $txt = " миллиардов";
                                        break;
                                case 5:
                                        if (($des > 9) And ($des < 20))
                                                $txt = " триллионов";
                                        else if ($r1 = 1) 
                                                $txt = " триллион";
                                        else if (($r1 > 1) And ($r1 < 5))
                                                $txt = " триллиона";
                                        else
                                                $txt = " триллионов";
                                        break;
                        }
                        if ($r3 > 0) $out .= " " . $razr3[$r3];
                        if ($r2 > 1) 
                        {
                                $out .= " " . $razr2[$r2];
                                if ($r1 > 2)
                                        $out .=  " " . $razr1[$r1];
                                else if (($r1 > 0) And ($r1 < 3))
                                        if ($i == 2)
                                                $out .= " " . $razr1[$r1 + 10];
                                        else
                                                $out .= " " . $razr1[$r1];
                        }
                        else if ($r2 == 1)
                                $out .= " " . $razr2[$des];
                        else if ($r2 == 0) 
                                        if ($r1 > 2)
                                                $out .=  " " . $razr1[$r1];
                                        else if (($r1 > 0) And ($r1 < 3))
                                                if ($i == 2)
                                                        $out .= " " . $razr1[$r1 + 10];
                                                else
                                                        $out .= " " . $razr1[$r1];
                        $out .= $txt;
                }
        }


        if (intval($val) > 0)
            $out .=  " " . ($short ? $row['short_currency_doc'] : $row['full_currency_' . self::getSwitcherByDigits($des)]) . " ";
        //*********** Копейки **************
        $txt = number_format($val, 2, ".", "");
        $des = intval(substr($txt, 1+strpos($txt, ".", 1))); 
        $r2 = Intval($des / 10);
        $r1 = Intval($des - Intval($des / 10) * 10);
        if ($currency)
 	    	$out .= ( $des < 10 ? "0" . $des : $des) . " " . ($short ? $row['short_cent_doc'] : $row['full_cent_' . self::getSwitcherByDigits($des)]);
        return ucfirst(trim($out));
	}


    /**
     * Transliterate russian chars to english chars combinations
     * @param string $txt
     * @return string
     */
    public static function translit($txt)
	{
    	$rus = "а,б,в,г,д,е,ё,ж,з,и,й,к,л,м,н,о,п,р,с,т,у,ф,х,ц,ч,ш,щ,ь,ы,ъ,э,ю,я";
        $rus = explode(',', $rus);
    	$eng = array("a", "b", "v", "g", "d", "e", "yo", "j", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "sch", "", "y", "", "e", "yu", "ya");
        mb_regex_encoding('utf-8');
        mb_internal_encoding('utf-8');
    	foreach($rus as $k => $v) $txt = mb_ereg_replace($v, $eng[$k], mb_strtolower($txt));
        return $txt;
	}

    /**
     * Cleans argument for login: low case english chars and digits
     * @param string $txt
     * @return string
     */
    public static function login($txt)
	{
    	$txt = strtolower($txt);
    	return preg_replace("/[^a-z0-9]/", "", $txt);
	}

    /**
     * Transliterate argument, cleans it and returns a valid anchor name
     * @param string $txt
     * @return string
     */
    public static function anchor($txt)
    {
        $txt = self::translit($txt);
        return str_replace(" ", "-", trim(preg_replace("/[^a-z0-9 ]/", "", $txt)));
    }


    /**
     * Returns an array of hashtags from text
     * @param string $txt
     * @return string[]
     */
    public static function fetchHashtags($txt)
	{
		preg_match_all("/\#([A-Za-z0-9\-\_А-Яа-яЁё]+)/siu", $txt, $matches);
		return $matches[1];
	}

}
