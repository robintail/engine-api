<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 27.06.15
 * Time: 05:55
 */

namespace RobinTail\EngineAPI;


use RobinTail\EngineAPI\Settings;

class Upload {

    private static $instance;

    /**
     * @return Upload
     */
    public static function get()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    /**
     *
     */
    private function __construct()
    {
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function readPeriods()
    {
        DB::get()->prepare("SELECT DISTINCT DATE_FORMAT(from_unixtime(user_at), '%Y_%m') as period
                            FROM uploads
                            ORDER BY user_at DESC")
                ->execute();
        return DB::get()->result();
    }


    /**
     * @param string $period
     * @param int $from
     * @throws \Exception
     * @return Upload\Item[]
     */
    public function readSome($period, $from)
    {
        $result = array();
        DB::get()->prepare("SELECT *
			              FROM uploads
			              WHERE DATE_FORMAT(from_unixtime(user_at), '%Y_%m') = :period
			              ORDER BY id_upload DESC
			              LIMIT :from, :count")
            ->bind(':from', intval($from))
            ->bind(':period', $period)
            ->bind(':count', intval(Settings::get()->readValue('uploads_show_per_cycle')))
            ->execute();
        while($row = DB::get()->fetchRow())
        {
            $result[] = $this->createUploadItemFromArray($row);
        }
        return $result;
    }

    /**
     * @param int $id
     * @return Upload\Item
     * @throws \Exception
     */
    public function readById($id)
    {
        DB::get()->prepare("SELECT *
                            FROM uploads
                            WHERE id_upload=?")
                ->bind(1, intval($id))
                ->execute();
        return $this->createUploadItemFromArray(DB::get()->fetchRow());
    }

    private function createUploadItemFromArray($row)
    {
        return Upload\Item::create()
                    ->writeId($row['id_upload'])
                    ->writeSrc($row['src'])
                    ->writeThumb($row['thumb'])
                    ->writeUserId($row['user_id'])
                    ->writeUserAt($row['user_at']);
    }


    /**
     * @param Upload\Item $item
     * @throws \Exception
     * @return bool
     */
    public function save($item)
    {
        if (intval($item->readId())) { // update
            DB::get()->prepare("UPDATE uploads
                                SET src = :src, thumb = :thumb
                                WHERE id_upload = :id")
                ->bind('src', $item->readSrc())
                ->bind('thumb', $item->readThumb())
                ->bind('id', $item->readId())
                ->execute();
        } else { //insert
            DB::get()->prepare("INSERT INTO uploads (src, thumb, user_id, user_at)
                                SELECT :src, :thumb, :user_id, :user_at")
                ->bind(':src', $item->readSrc())
                ->bind(':thumb', $item->readThumb())
                ->bind(':user_id', $item->readUserId() ? $item->readUserId() : User\Current::get()->readId())
                ->bind(':user_at', $item->readUserAt() ? $item->readUserAt() : time())
                ->execute();
            $item->writeId(DB::get()->getInsertId());
        }
        return true;
    }

    public function remove($id)
    {
        $item = $this->readById($id);
        if ($item->readId()) {
            if ($item->readSrc()) unlink(Path::resolve(Settings::get()->readValue('upload_dir')) . '/' . $item->readSrc());
            if ($item->readThumb()) unlink(Path::resolve(Settings::get()->readValue('upload_dir')) . '/' . $item->readThumb());
            DB::get()->prepare("DELETE FROM uploads
                                WHERE id_upload=?")
                ->bind(1, intval($id))
                ->execute();
        }
        return true;
    }


} 