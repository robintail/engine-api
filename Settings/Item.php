<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 22.07.15
 * Time: 19:39
 */

namespace RobinTail\EngineAPI\Settings;
use RobinTail\EngineAPI\Regex;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Settings
 */
class Item {

    /**
     * @var string
     */
    private $key;
    /**
     * @var mixed
     */
    private $value;
    /**
     * @var string
     */
    private $description;
    /**
     * @var bool
     */
    private $isHidden;
    /**
     * @var string
     */
    private $typeCode;

    /**
     * @return string
     */
    public function readTypeCode()
    {
        return $this->typeCode;
    }

    /**
     * @param string $typeCode
     * @return $this
     */
    public function writeTypeCode($typeCode)
    {
        $this->typeCode = $typeCode;
        return $this;
    }

    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return string
     */
    public function readDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function writeDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsHidden()
    {
        return $this->isHidden;
    }

    /**
     * @param boolean $isHidden
     * @return $this
     */
    public function writeIsHidden($isHidden)
    {
        $this->isHidden = (bool) $isHidden;
        return $this;
    }

    /**
     * @return string
     */
    public function readKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function writeKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function readValue()
    {
        switch ($this->readTypeCode())
        {
            case 'int':
                return intval($this->value);
                break;
            case 'bool':
                return (bool) $this->value;
                break;
            case 'float':
                return Regex::float($this->value);
                break;
            case 'html':
            case 'string':
            default:
                return $this->value;
        }
    }

    public function readHtmlValueForEditor()
    {
        return htmlentities( $this->readValue() , ENT_NOQUOTES , 'utf-8');
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function writeValue($value)
    {
        $this->value = $value;
        return $this;
    }




} 