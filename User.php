<?

namespace RobinTail\EngineAPI;

use RobinTail\EngineAPI\Output\CLI;

class User
{
    private static $instance;
	private $errorMessage = "";


    /**
     * @return User
     */
    public static function get()
	{
		if (!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}


    /**
     *
     */
    private function __construct()
    {
    }


    /**
     * @return User\Item[]
     * @throws \Exception
     */
    public function readAll()
    {
        $result = array();
        DB::get()->prepare("SELECT *
                            FROM users
                            ORDER BY user_name")
                ->execute();
        while($row = DB::get()->fetchRow())
        {
            $result[$row['id_user']] = $this->createUserItemFromArray($row);
        }
        return $result;
    }

    /**
     * @param int $id
     * @return User\Item
     */
    public function readById($id)
    {
        DB::get()->prepare("SELECT *
                            FROM users
                            WHERE id_user=?")
                ->bind(1, intval($id))
                ->execute();
        if ($row = DB::get()->fetchRow())
        {
            return $this->createUserItemFromArray($row);
        } else {
            return User\Item::create();
        }
    }


    /**
     * @param $email
     * @return User\Item
     * @throws \Exception
     */
    public function readByEmail($email)
    {
        DB::get()->prepare("SELECT *
                            FROM users
                            WHERE user_email=?")
            ->bind(1, $email)
            ->execute();
        if ($row = DB::get()->fetchRow())
        {
            return $this->createUserItemFromArray($row);
        } else {
            return User\Item::create();
        }
    }

    /**
     * @param array $row
     * @return User\Item
     */
    private function createUserItemFromArray($row)
    {
        return User\Item::create()
            ->writeId($row['id_user'])
            ->writeEmail($row['user_email'])
            ->writeName($row['user_name'])
            ->writePassword($row['user_password'])
            ->writeIsAdmin($row['is_admin'])
            ->writeDateLastTry($row['date_last_try'])
            ->writeTries($row['tries'])
            ->writeLangCode($row['lang_code']);
    }



    /**
     * @param string $log
     * @param string $pwd
     * @param bool $checkIsAdmin
     * @throws \Exception
     * @return bool
     */
    public function validate($log, $pwd, $checkIsAdmin=false)
    {
        // check input data
        if (!$log) {
            $this->writeErrorMessage("Вы не ввели логин");
            return false;
        }
        if (!$pwd) {
            $this->writeErrorMessage("Вы не ввели пароль");
            return false;
        }
        // find user
        $user = $this->readByEmail($log);
        if (!$user->readId()) {
            $this->writeErrorMessage("Логин указан неверно");
            return false;
        }
        // check brute ability
        if (($user->readTries() >= intval(Settings::get()->readValue('anti_brute_tries'))) && (time() - $user->readDateLastTry() <= intval(Settings::get()->readValue('anti_brute_time')) )) {
            $this->writeErrorMessage("Доступ к этому аккаунту заблокирован до " . date("H:i:s", $user->readDateLastTry() + intval(Settings::get()->readValue('anti_brute_time'))));
            return false;
        }
        // brute force resistance
        if (Security::cryptPassword($pwd) != $user->readPassword()) {
            DB::get()->prepare("UPDATE users
                                SET tries = tries + 1,
                                date_last_try = :date
                                WHERE user_email = :email")
                ->bind(':date', time())
                ->bind(':email', $log)
                ->execute();
            if (intval(Settings::get()->readValue('anti_brute_tries')) - $user->readTries() - 1 > 0) {
                $this->writeErrorMessage("Пароль указан неверно. Осталось " . (intval(Settings::get()->readValue('anti_brute_tries')) - $user->readTries() - 1) . " попыток");
                return false;
            } else {
                $this->writeErrorMessage("Пароль указан неверно. Доступ к аккаунту заблокирован на " . (intval(Settings::get()->readValue('anti_brute_time')) / 60) . " минут");
                return false;
            }
        }
        // check is_admin if requested
        if ($checkIsAdmin)
        {
            if (!$user->readIsAdmin()) {
                $this->writeErrorMessage("Отсутствуют права администратора");
                return false;
            }
        }
        // unset brute force markers
        DB::get()->prepare("UPDATE users
                            SET tries = 0,
                            date_last_try = 0
                            WHERE user_email = ?")
            ->bind(1, $log)
            ->execute();
        // save login and password to cookie for future autologin
        $this->saveCookie($log, $pwd);
        return true;
    }

    /**
     * writes login and password in cookies
     * @param string $log
     * @param string $pwd
     */
    private function saveCookie($log, $pwd)
    {
        if (PHPunit::isHeadersSent()) return;
        if (CLI::get()->isCLI()) return;
        setrawcookie('log', $log, time()+90*86400,'/');
        setrawcookie('pwd', Security::cryptPassword($pwd), time()+90*86400,'/');
        setrawcookie('ip', md5(Security::getIP()), time()+90*86400,'/');
        setrawcookie('browser', md5($_SERVER['HTTP_USER_AGENT']), time()+90*86400,'/');
    }

    /**
     * cleans cookies
     */
    public function flushCookie()
    {
        if (PHPunit::isHeadersSent()) return;
        if (CLI::get()->isCLI()) return;
        setrawcookie('log',"",time()-86400,'/');
        setrawcookie('pwd',"",time()-86400,'/');
        setrawcookie('ip',"",time()-86400,'/');
        setrawcookie('browser',"",time()-86400,'/');
    }




    /**
     * @return string
     */
    public function readErrorMessage()
    {
        return $this->errorMessage;
    }


    /**
     * @param string $msg
     */
    private function writeErrorMessage($msg)
    {
        $this->errorMessage = $msg;
    }


    /**
     * Creates or updates the user
     * @param User\Item $item
     * @return int
     * @throws \Exception
     */
    public function write($item)
	{
		// создание юзера
		if (!$item->readName()) { $this->writeErrorMessage("Имя не указано"); return false; }
		if (!$item->readEmail()) { $this->writeErrorMessage("Адрес почты не указан"); return false; }
        $test = $this->readByEmail($item->readEmail());
		if ($test->readId() != $item->readId()) { $this->writeErrorMessage("Адрес почты уже зарегистрирован"); return false; }
        if (!$item->readId()) {
            if (!$item->readPassword()) { $this->writeErrorMessage("Пароль не указан"); return false; }
            DB::get()->prepare("INSERT INTO users (user_name, user_email, user_password, is_admin)
                                SELECT :name, :email, :password, :is_admin")
                ->bind(':name', $item->readName())
                ->bind(':email', $item->readEmail())
                ->bind(':password', Security::cryptPassword($item->readPassword()))
                ->bind(':is_admin', $item->readIsAdmin())
                ->execute();
            $item->writeId(DB::get()->getInsertId());
        } else {
            DB::get()->prepare("UPDATE users
                                SET user_name = :name,
                                user_email = :email,
                                " . ($item->readPassword() ? "user_password = :password," : "") . "
                                is_admin = :is_admin
                                WHERE id_user = :id_user")
                    ->bind(':name', $item->readName())
                    ->bind(':email', $item->readEmail())
                    ->bind(':is_admin', $item->readIsAdmin())
                    ->bind(':id_user', $item->readId());
            if ($item->readPassword()) DB::get()->bind(':password', Security::cryptPassword($item->readPassword()));
            DB::get()->execute();
        }
        return $item->readId();
	}

    /**
     * @param int $id
     * @return bool
     */
    public function remove($id)
    {
        DB::get()->prepare("DELETE FROM users
                            WHERE id_user=?")
                ->bind(1, intval($id))
                ->execute();
        return true;
    }

    /**
     * @param string $email
     * @return bool
     * @throws \Exception
     */
    public function sendRecoveryEmail($email)
    {
        $user = $this->readByEmail($email);
        if ($user->readId())
        {
            $mail = "<p><#recovery-email-notice#></p>
			<p>EMail: ".$user->readEmail()."</p>
			<p>ID: ".$user->readId()."</p>
			<p><#recovery-key#>: ".Security::getRecoveryHash($user->readPassword())."</p>
			<p><#recovery-email-link#> http://".$_SERVER['SERVER_NAME']."/login/recoverykey.php</p>";
            $resultsend = Email::send($user->readEmail(), $mail, "<#recovery-request#>", false, $user->readLangCode());
            return (bool) $resultsend;
        } else {
            $this->writeErrorMessage("<#user-not-found-error#>");
            return false;
        }
    }

    /**
     * Anonymously changes user password using recovery key
     * @param int $id
     * @param string $key
     * @param string $newPassword
     * @return bool
     * @throws \Exception
     */
    public function changePassword($id, $key, $newPassword)
    {
        $user = $this->readById($id);
        if ($user->readId())
        {
            if ( Security::getRecoveryHash($user->readPassword()) == trim($key) )
            {
                DB::get()->prepare("UPDATE users
                                    SET user_password=:user_password
                                    WHERE id_user=:id_user")
                    ->bind(':user_password', Security::cryptPassword(trim($newPassword)))
                    ->bind(':id_user', $user->readId())
                    ->execute();
                $mail = "<p><#password-changed#></p>
				<p>Email: ".$user->readEmail()."</p>
				<p><#password#>: ".$newPassword."</p>
				<p><#remember-your-password#></p>";
                Email::send($user->readEmail(), $mail, "<#password-changed-email#>", false, $user->readLangCode());
                return true;
            } else {
                $this->writeErrorMessage("<#key-error#>");
                return false;
            }
        } else {
            $this->writeErrorMessage("<#user-not-found-error#>");
            return false;
        }

    }



}
