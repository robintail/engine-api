<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 15.07.15
 * Time: 13:31
 */

namespace RobinTail\EngineAPI;


class PHPunit {


    /**
     * Initiates database for tests from fake xml file
     */
    public static function initFakeDatabaseForTests()
    {
        DB::getTest();
    }

    /**
     * Get value of private property of reflection of class object
     * @param object $class
     * @param string $propertyName
     * @return mixed
     */
    public static function getPrivateProperty($class, $propertyName)
    {
        $reflection = new \ReflectionClass(get_class($class));
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);
        return $property->getValue();
    }

    /**
     * Verify is header information already sent by phpunit
     * @return bool
     */
    public static function isHeadersSent()
    {
        if (headers_sent($file))
        {
            if (strstr($file, 'phpunit')) return true;
        }
        return false;
    }

} 