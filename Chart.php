<?

namespace RobinTail\EngineAPI;

use RobinTail\EngineAPI\Output\Scripts;

class Chart
{

	public static function create($options)
	{
		// options: type, title, width, height, data
		// axis-x
		$id = 'chart_'.uniqid();
		$json = Arr2dim::jsArray($options['data']);
		$stacked = $options['stacked'] ? ', isStacked: true' : '';
		switch($options['type'])
		{
			case 'area':
				$func = "AreaChart";
				break;
			case 'pie':
				$func = "PieChart";
				break;
			case 'column':
				$func = "ColumnChart";
				break;
			case 'line':
			case "LineChart":
			default:
				$func = "LineChart";
		}
		$sc = <<<HTM
		<script type="text/javascript">
			$(function() {
			      google.load("visualization", "1", {packages:["corechart"], callback: 
			      	function () {
				        var data = google.visualization.arrayToDataTable({$json});
				        var options = {
				          title: '{$options['title']}',
				          hAxis: {slantedText: true, slantedTextAngle: 45}
				          {$stacked}
				        };
				        var chart = new google.visualization.{$func}(document.getElementById('{$id}'));
				        chart.draw(data, options);
			      }});
			});
	    </script>
HTM;
		$out = <<<HTM
	    <div id="{$id}" style="width: {$options['width']}; height: {$options['height']};"></div>
HTM;
		Scripts::get()->addScript($sc);
		return $out;
	}


}

?>