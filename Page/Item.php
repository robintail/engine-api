<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 18.07.15
 * Time: 13:29
 */

namespace RobinTail\EngineAPI\Page;
use RobinTail\EngineAPI\Lang;
use RobinTail\EngineAPI\Regex;


/**
 * Class Item
 * @package RobinTail\EngineAPI\Page
 */
class Item {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $url;
    /**
     * @var int
     */
    private $templateId;
    /**
     * @var int|null
     */
    private $overrideActiveLinkId;
    /**
     * @var bool
     */
    private $isUseAffix;
    /**
     * @var Item\Trans[]
     */
    private $trans=array();

    /**
     * @return Item
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int
     */
    public function readTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @param int $templateId
     * @return $this
     */
    public function writeTemplateId($templateId)
    {
        $this->templateId = (int) $templateId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function readOverrideActiveLinkId()
    {
        return $this->overrideActiveLinkId;
    }

    /**
     * @param int|null $overrideActiveLinkId
     * @return $this
     */
    public function writeOverrideActiveLinkId($overrideActiveLinkId)
    {
        $this->overrideActiveLinkId = Regex::intNull($overrideActiveLinkId);
        return $this;
    }

    /**
     * Returns all page translation languages
     * @return string[]
     */
    public function readTransLanguages()
    {
        return array_keys($this->trans);
    }

    /**
     * Returns all enabled page translation languages
     * @return string[]
     */
    public function readTransLanguagesEnabled()
    {
        $result = array();
        foreach($this->trans as $lang => $trans)
        {
            if ($trans->readIsEnabled()) $result[] = $lang;
        }
        return $result;
    }

    /**
     * @param string $langCode
     * @return Item\Trans
     */
    public function readTrans($langCode)
    {
        return isset($this->trans[$langCode]) ? $this->trans[$langCode] : Item\Trans::create();
    }

    /**
     * @param string $langCode
     * @param Item\Trans $trans
     * @return $this
     */
    public function writeTrans($langCode, $trans)
    {
        $this->trans[$langCode] = $trans;
        return $this;
    }

    /**
     * @return string
     */
    public function readUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function writeUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsUseAffix()
    {
        return $this->isUseAffix;
    }

    /**
     * @param boolean $isUseAffix
     * @return $this
     */
    public function writeIsUseAffix($isUseAffix)
    {
        $this->isUseAffix = (bool) $isUseAffix;
        return $this;
    }



    /**
     * Quick access to short title translation in currently using language
     * @return string
     */
    public function readTitleShortInUsingLang()
    {
        if ($this->readTrans(Lang::get()->getUsing())->readIsEnabled()) {
            return $this->readTrans(Lang::get()->getUsing())->readTitleShort();
        } elseif ($this->readTrans($this->readTransLanguagesEnabled()[0])->readIsEnabled())
        {
            return $this->readTrans($this->readTransLanguagesEnabled()[0])->readTitleShort();
        }
        return "";
    }


    /**
     * @return string
     */
    public function readKeywordsInUsingLang()
    {
        if ($this->readTrans(Lang::get()->getUsing())->readIsEnabled()) {
            return $this->readTrans(Lang::get()->getUsing())->readKeywords();
        } elseif ($this->readTrans($this->readTransLanguagesEnabled()[0])->readIsEnabled())
        {
            return $this->readTrans($this->readTransLanguagesEnabled()[0])->readKeywords();
        }
        return "";
    }

    /**
     * @return string
     */
    public function readDescriptionInUsingLang()
    {
        if ($this->readTrans(Lang::get()->getUsing())->readIsEnabled()) {
            return $this->readTrans(Lang::get()->getUsing())->readDescription();
        } elseif ($this->readTrans($this->readTransLanguagesEnabled()[0])->readIsEnabled())
        {
            return $this->readTrans($this->readTransLanguagesEnabled()[0])->readDescription();
        }
        return "";
    }


} 