<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 18.07.15
 * Time: 13:33
 */

namespace RobinTail\EngineAPI\Page\Item;

use RobinTail\EngineAPI\Regex;

/**
 * Class Trans
 * @package RobinTail\EngineAPI\Page\Item
 */
class Trans {

    /**
     * @var int
     */
    private $id;
    /**
     * @var bool
     */
    private $isEnabled;
    /**
     * @var int
     */
    private $page_id;
    /**
     * @var int
     */
    private $lang_id;
    /**
     * @var string
     */
    private $titleShort;
    /**
     * @var string
     */
    private $titleLong;
    /**
     * @var string
     */
    private $html;
    /**
     * @var string
     */
    private $keywords;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $affix;

    /**
     * @return Trans
     */
    public static function create()
    {
        return new self();
    }

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return string
     */
    public function readAffix()
    {
        return $this->affix;
    }

    /**
     * @param string $affix
     * @return $this
     */
    public function writeAffix($affix)
    {
        // issue 75
        //$this->affix = html_entity_decode($affix, ENT_QUOTES, 'utf-8');
        $this->affix = $affix;
        return $this;
    }

    /**
     * Automatically generates affix from readHtml() and write using writeAffix()
     * Add anchors to headers in html and rewrite in using writeHtml()
     * @return $this
     */
    public function cacheAffixFromHtml()
    {
        if ($tmp = $this->readHtml()) {
            $dom = new \DOMDocument();
            $dom->loadHTML('<?xml encoding="UTF-8">' . $tmp);
            $affix = new \DOMDocument();
            $affix->loadHTML('<?xml encoding="UTF-8"><html><body></body></html>');
            $lastDepth = 0;
            $this->cacheAffixIteration($dom, $affix->getElementsByTagName('body')->item(0), $lastDepth);
            if ($rootUl = $affix->getElementById('rootUl')) {
                $result = trim(substr($affix->saveHTML($rootUl), strlen('<ul id="rootUl">'), -strlen('</ul>')));
                $this->writeAffix($result);
                if ($domBody = $dom->getElementsByTagName('body')->item(0))
                {
                    $result = trim(substr($dom->saveHTML($domBody), strlen('<body>'), -strlen('</body>')));
                    $this->writeHtml($result);
                }
            }
        }
        return $this;
    }

    /**
     * Remove all child anchor nodes (from header node)
     * @param \DOMNode $sourceNode
     */
    private function removeAnchorsIteration($sourceNode)
    {
        for($i=0; $i<$sourceNode->childNodes->length; $i++)
        {
            $childNode = $sourceNode->childNodes->item($i);
            if (strtolower($childNode->nodeName) == 'a')
            {
                /** @var \DOMElement $childNode */
                if (!$childNode->hasAttribute('href'))
                {
                    $sourceNode->removeChild($childNode);
                    $i--;
                }
            } elseif ($childNode->hasChildNodes()) {
                $this->removeAnchorsIteration($childNode);
            }
        }
    }

    /**
     * @param \DOMNode $sourceNode
     * @return string
     */
    private function createAnchor($sourceNode)
    {
        $iterator=0;
        $newAnchor = $sourceNode->ownerDocument->createElement('a');
        do {
            $iterator++;
            $newId = trim(Regex::anchor($sourceNode->textContent)).($iterator>1 ? $iterator : '');
            $newId = $newId ? $newId : uniqid('affix_');
        } while ($sourceNode->ownerDocument->getElementById($newId) !== null);
        $newAnchor->setAttribute('id', $newId);
        $sourceNode->appendChild($newAnchor);
        return $newId;
    }

    /**
     * @param \DOMNode $sourceNode
     * @param \DOMNode $affixNode
     * @param int $lastDepth
     */
    private function cacheAffixIteration($sourceNode, $affixNode, &$lastDepth)
    {
        for($i=0; $i<$sourceNode->childNodes->length; $i++)
        {
            $childNode = $sourceNode->childNodes->item($i);
            if (in_array(strtolower($childNode->nodeName), array('h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7')))
            {
                // remove all old anchor elements
                $this->removeAnchorsIteration($childNode);
                // creating ul hierarcy if required
                $depth = intval(substr($childNode->nodeName, 1, 1));
                if ($depth > $lastDepth)
                {
                    /**
                     * @var \DOMElement $newUl
                     */
                    $newUl = $affixNode->ownerDocument->createElement('ul');
                    if ($depth == 1) {
                        $newUl->setAttribute('id', 'rootUl');
                    } else {
                        $newUl->setAttribute('class', 'nav');
                    }
                    $affixNode->appendChild($newUl);
                    $affixNode = $newUl;
                } elseif ($depth < $lastDepth) {
                    for($e = $lastDepth; $e*2+2 > $depth; $e--) {
                        $affixNode = $affixNode->parentNode;
                    }
                } else {
                    $affixNode = $affixNode->parentNode;
                }
                // append new child to affix
                $lastDepth = $depth;
                /**
                 * @var \DOMElement $newLi
                 */
                $newLi = $affixNode->ownerDocument->createElement('li');
                $affixNode->appendChild($newLi);
                $affixNode = $newLi;
                $newA = $affixNode->ownerDocument->createElement('a', $childNode->nodeValue);
                $newA->setAttribute('href', '#'.$this->createAnchor($childNode));
                $newLi->appendChild($newA);
            } elseif ($childNode->hasChildNodes()) {
                $this->cacheAffixIteration($childNode, $affixNode, $lastDepth);
            }
        }
    }

    /**
     * @return string
     */
    public function readDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function writeDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function readHtml()
    {
        return $this->html;
    }

    /**
     * @return string
     */
    public function readHtmlForEditor()
    {
        return htmlentities( $this->readHtml() , ENT_NOQUOTES , 'utf-8');
    }

    /**
     * @param string $html
     * @return $this
     */
    public function writeHtml($html)
    {
        // issue 75
        // $this->html = html_entity_decode($html, ENT_QUOTES, 'utf-8');
        $this->html = $html;
        return $this;
    }

    /**
     * @return int
     */
    public function readId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function writeId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return boolean
     */
    public function readIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param boolean $isEnabled
     * @return $this
     */
    public function writeIsEnabled($isEnabled)
    {
        $this->isEnabled = (bool) $isEnabled;
        return $this;
    }



    /**
     * @return string
     */
    public function readKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     * @return $this
     */
    public function writeKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @return int
     */
    public function readLangId()
    {
        return $this->lang_id;
    }

    /**
     * @param int $lang_id
     * @return $this
     */
    public function writeLangId($lang_id)
    {
        $this->lang_id = (int) $lang_id;
        return $this;
    }

    /**
     * @return int
     */
    public function readPageId()
    {
        return $this->page_id;
    }

    /**
     * @param int $page_id
     * @return $this
     */
    public function writePageId($page_id)
    {
        $this->page_id = (int) $page_id;
        return $this;
    }

    /**
     * @return string
     */
    public function readTitleLong()
    {
        return $this->titleLong;
    }

    /**
     * @param string $titleLong
     * @return $this
     */
    public function writeTitleLong($titleLong)
    {
        $this->titleLong = $titleLong;
        return $this;
    }

    /**
     * @return string
     */
    public function readTitleShort()
    {
        return $this->titleShort;
    }

    /**
     * @param string $titleShort
     * @return $this
     */
    public function writeTitleShort($titleShort)
    {
        $this->titleShort = $titleShort;
        return $this;
    }

} 