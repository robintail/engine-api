<?

namespace RobinTail\EngineAPI;

class Arr2dim
{

	public static function table($array2, $fields=array(), $other=array())
	{
		// converts arr2dim to html table
		// use fields array to rename header
		// use other for options:
		//	- class - table class
		//	- style_type_field - field in arr2dim to specify the style type
		//	- styles - an array defines style types
		$i=0; $result = "<table class='".$other['class']."'>";
		foreach($array2 as $pre)
		{
			$i++; $row=array(); $style=array();
			if (($other['style_type_field']) && (is_array($other['styles']))) 
				$rowstyle = $other['styles'][ $pre[ $other['style_type_field'] ] ];
			if (!count($fields))
			{
				$fields = array_combine(array_keys($pre), array_keys($pre));
			}
			foreach($fields as $k => $v)
			{
				$row[$v] = is_numeric($pre[$k]) ? Regex::formatNumber($pre[$k]) : $pre[$k];
				$style[$v] = is_numeric($pre[$k]) ? "text-align: right; white-space: nowrap;" : '';
			}
			if ($i==1)
			{
				$result .= "<tr><th>".implode("</th><th>", array_keys($row) )."</th></tr>";
			}
			$result .= "<tr".($rowstyle?" style='".$rowstyle."'":'').">";
			foreach($row as $k => $v)
			{
				$result .= "<td".($style[$k]?" style='".$style[$k]."'":'').">".$v."</td>";
			}
			$result .= "</tr>";
			//$result .= $prefix[common].implode($separator[common], $row ) . $postfix[common];
		}
		return $result."</table>";
	}

	public static function replaceHeaders($result,$keys)
	{
		// replace headers in Result array with keys from Keys
		// using $k => $v to set new header (1-line array)
		foreach($result as $rowindex => $row)
		{
			foreach($keys as $k => $v)
			{
				if (isset($result[$rowindex][$k])) 
				{
					$result[$rowindex][$v] = $result[$rowindex][$k];					
				}
				unset($result[$rowindex][$k]);
			}
		}
		return $result;
	}

    /**
     * Tests is the argument a valid 2dim array
     * @param mixed $test
     * @return bool
     */

    public static function is2dim($test)
	{
        if (!is_array($test)) return false;
		foreach($test as $row)
		{
			if (is_array($row)) return true;
		}
		return false;
	}

    /**
     * Convert array to 2dim array with fields 'id' and 'value'
     * @param array $arr
     * @return array
     */

    public static function make2dim($arr)
	{
        assert( is_array($arr), "Argument should be an array");
		$arr2 = array();
		foreach($arr as $k=>$v)
		{
			$arr2[$k] = array('id'=>$k, 'value'=>$v);
		}
		return $arr2;
	}

    /**
     * Convert 2dim array to array with keys from field 'key' and valued from field 'value'
     * @param array $array2
     * @param string $key
     * @param string $value
     * @return array
     */

    public static function kv($array2, $key, $value)
	{
        assert( is_array($array2), "Argument should be an array");
        $pre = array();
        foreach($array2 as $row) $pre[$row[$key]] = $row[$value];
		return $pre;
	}

	public static function csv($array2, $fields=array())
	{
		$i=0; $result = "";
		$regex_from = array(chr(9),"\"",chr(13)); // chr(13),chr(10));
		$regex_to   = array(chr(32),"\"\"",""); // chr(15),chr(12));
		$separator = "\"".chr(9)."\"";
		$prefix = "\"";
		$postfix = "\"\n";
		foreach($array2 as $pre)
		{
			$i++;
			if (!count($fields))
			{
				$fields = array_combine(array_keys($pre), array_keys($pre));
			}
			foreach($fields as $k => $v)
			{
				$row[$v] = is_numeric($pre[$k]) ? str_replace(".",",",$pre[$k]) : $pre[$k];
			}
			if ($i==1)
			{
				$result .= $prefix.implode($separator, str_replace($regex_from,$regex_to,array_keys($row)) ).$postfix;
			}
			$result .= $prefix.implode($separator, str_replace($regex_from,$regex_to,$row) ) . $postfix;
		}
		return $result;
	}

    /**
     * Insert 1dim array into 2dim array (optionaly to top)
     * @param $array2
     * @param $array_inc
     * @param bool $on_top
     * @return array
     */
    public static function inc($array2, $array_inc, $on_top = false)
	{
        assert(is_array($array2), "Argument should be an array");
        assert(is_array($array_inc), "Argument should be an array");
		return $on_top ? array_merge(array($array_inc), $array2) : array_merge($array2, array($array_inc));
	}

    /**
     * Fetch unique sorted pairs of ucfirst('value') + 'key' fields
     * Using without 'value' is deprecated to function keys()
     * @param array $array2
     * @param string $key
     * @param string $value
     * @return array
     */

    public static function distinct($array2, $key, $value)
    {
        assert(is_array($array2), "Argument should be an array");
        $result = array();
        foreach($array2 as $array)
        {
            if (!array_key_exists( ucfirst($array[$value].$array[$key]), $result))
            {
                $result[ucfirst($array[$value].$array[$key])] = array($key => $array[$key], $value => $array[$value]);
            }
        }
        ksort($result);
        return $result;
    }


    /**
     * Fetch unique keys of 2dim array (optionaly with empty)
     * @param $array2
     * @param $key
     * @param bool $with_empty
     * @return array
     */
    public static function keys($array2, $key, $with_empty=true)
	{
        assert(is_array($array2), "Argument should be an array");
		$keys = array();
		foreach($array2 as $array)
		{
			if (!in_array($array[$key], $keys))
				if (($with_empty) || ($array[$key]))
					$keys[]=$array[$key];
		}
		return $keys;
	}

    public static function filter($array2, $keyfield, $filter_value)
	{
		$result = array();
		foreach($array2 as $row)
		{
			if (is_array($filter_value))
			{
				if (in_array($row[$keyfield], $filter_value)) $result[] = $row;
			} else {
				if ($row[$keyfield] == $filter_value) $result[] = $row;
			}
		}
		return $result;
	}

    public static function filter_mega_drive_by_marat($array2, $keyfield, $filter_value)
	{
		return array_filter($array2, function($row) use ($keyfield, $filter_value) {
			return is_array($filter_value) ? in_array($row[$keyfield], $filter_value) : $row[$keyfield] == $filter_value;
		});
	}


    public static function suggest($array2, $keyfield, $valuefield, $value)
	{
		foreach($array2 as $row)
		{
			if (strtolower($row[$valuefield]) == strtolower($value)) return $row[$keyfield];
		}
		return false;
	}

    public static function index($array2, $field, $value)
	{
		foreach($array2 as $k=>$row)
		{
			if (strtolower($row[$field]) == strtolower($value)) return $k;
		}
		return false;
	}


    public static function sum($array2, $field)
	{
		$result = 0;
		foreach($array2 as $row)
		{
			$result += $row[ $field ];
		}
		return $result;
	}

    public static function transpose($array2, $field_to_row_name, $fields_to_rows_array, $field_to_columns, $field_to_columns_limited_values=array())
	{
		// convert an array2 so that fields_to_rows_array become values in new field_to_row_name
		// and values of field_to_column become new columns
		// it is able to limit values of field_to_column with field_to_columns_limited_values
		$result = array();
		foreach($array2 as $row)
		{
			foreach($fields_to_rows_array as $k=>$v)
			{
				$result[$k][$field_to_row_name] = $v;
				if (!count($field_to_columns_limited_values) || in_array($row[$field_to_columns], $field_to_columns_limited_values))
					$result[$k][$row[$field_to_columns]] = $row[$v];
			}
		}
		return $result;
	}

	public static function pivot($array2, $field_to_rows, $field_to_columns, $field_to_value)
	{
		$result = array();
		$fields = self::kv($array2, $field_to_columns, $field_to_columns);
		foreach($array2 as $row)
		{
			if (!$result[$row[$field_to_rows]]) $result[$row[$field_to_rows]] = array($field_to_rows=>$row[$field_to_rows]);
			foreach($fields as $field)
			{
				$result[$row[$field_to_rows]][$field] += ($row[$field_to_columns]==$field ? $row[$field_to_value] : 0); 
			}
		}
		return $result;
	}

	public static function jsArray($array2)
	{
		// convert to javascript array
		$result = "["; $row_number=-1;
		foreach($array2 as $row)
		{
			$row_number++;
			if (!$row_number) 
			{
				$cols = array_keys($row);
				foreach($cols as &$col) $col = "'".str_replace("'", "\\'", $col)."'";
				$result .= "[".implode(",",$cols)."],";
			}
			$cols = $row;
			foreach($cols as &$col) $col = is_numeric($col) ? $col : ($col ? "'".str_replace("'", "\\'", $col)."'" : 'null');
			$result .= "[".implode(',',$cols)."],";
		}
		return substr($result,0,-1)."]";
	}
}
?>