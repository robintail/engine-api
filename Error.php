<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 20.06.15
 * Time: 02:38
 */

namespace RobinTail\EngineAPI;


class Error {
    const EMAIL_FREQ = 900; // seconds to send email notifications

    public static function getLogDir()
    {
        return Path::resolve('tmp');
    }

    public static function getLogFile()
    {
        return self::getLogDir()."/last_error.txt";
    }

    public static function getEmailFlag()
    {
        return self::getLogDir()."/last_error_email.txt";
    }

    public static function ErrorHandler($num, $str, $file, $line) {
        throw new \ErrorException($str, 0, $num, $file, $line);
    }

    public static function ExceptionHandler(\Exception $e)
    {
        $txt = "Error at ".date('d.m.Y H:i:s')."\r\n".
            "Request ".$_SERVER['REQUEST_URI']."\r\n".
            "Type: ".$e->getCode()."\r\n".
            "Message: ".$e->getMessage()."\r\n".
            "File: ".$e->getFile()."\r\n".
            "Line: ".$e->getLine()."\r\n".
            "Trace: ".$e->getTraceAsString()."\r\n";
        if (file_exists(self::getEmailFlag()))
        {
            $last = @filemtime(self::getEmailFlag()) or 0;
        } else {
            $last = 0;
        }
        if (time() - $last > self::EMAIL_FREQ)
        {
            try{
                $email_to = Settings::get()->readValue('email_tech');
            } catch(\Exception $e)
            {
                $email_to = 'info@'.$_SERVER['SERVER_NAME'].', admin@'.$_SERVER['SERVER_NAME'].', abuse@'.$_SERVER['SERVER_NAME'];
            }
            try {
                @fwrite(@fopen(self::getEmailFlag(), 'w'), "Email sent at ".time());
            } catch (\Exception $e) { /* */ }
            $notified = @mail($email_to, 'Error on website '.$_SERVER['SERVER_NAME'], $txt);
        }
        try {
            $written = @fwrite(@fopen(self::getLogFile(), 'w'), $txt);
        } catch (\Exception $e) { /* */ }
        $html = '<div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Something went wrong...</h3>
                </div>
                <div class="panel-body">
                    <p>We apologize for this unfortunate situation. During program execution the error has occurred. The Administrator has made aware about this issue. Please feel free to contact us if necessary.</p>
                    <p><strong>Message:</strong> '.($e->getMessage()?'Logged':'Unknown').'</p>
                    <p><strong>File:</strong> '.($e->getFile()?'Logged':'Unknown').'</p>
                    <p><strong>Line:</strong> '.($e->getLine()?'Logged':'Unknown').'</p>
                    <p><strong>Trace:</strong> '.($e->getTraceAsString()?'Logged':'Unknown').'</p>
                    <p><strong>Log:</strong> '.($written!==false?'Written':'FAILED').'</p>
                    <p><strong>Notification:</strong> '.($notified?'Sent':'Pending').'</p>
                </div>
            </div>';
        Output::get()->fatal($html);
        exit();
    }
} 